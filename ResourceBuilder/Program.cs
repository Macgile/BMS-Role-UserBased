﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Resources.Utility;
using Resources.Concrete;
using System.IO;

namespace ResourceBuilder
{
    class Program
    {
        static void Main(string[] args)
        {
            var builder = new Resources.Utility.ResourceBuilder();
            var conString = @"Data Source = (localdb)\Projects; Initial Catalog = MvcInternationalization; Integrated Security = True; Pooling = False";

            conString = @"Data Source =.\SQLEXPRESS; Initial Catalog = BMS; user id = sa; password = Motdepasse1";


            string filePath = builder.Create(new DbResourceProvider(conString), summaryCulture: "fr");
            Console.WriteLine("Created file {0}", filePath);

            var path = @"C:\Users\Administrateur\Documents\Visual Studio 2015\Projects\BMS-Role-UserBased\Resources\Resources.cs";

            try
            {
                File.Copy(filePath, path, true);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                // System.IO.IOException: The file 'file-b.txt' already exists.
            }
        }
    }
}
