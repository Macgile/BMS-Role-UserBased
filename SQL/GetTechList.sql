DECLARE @SuperieurHie INT;
SET @SuperieurHie = 1;

SELECT Technicien.ID, Utilisateur.Nom+' '+Utilisateur.Prenom AS Nom, Utilisateur.RefRole, Technicien.RefRespZone,
       Utilisateur.IsActive, T.ID
       --U.*
FROM Technicien
     INNER JOIN Utilisateur ON Technicien.ID = Utilisateur.RefId
                               AND Utilisateur.RefRole = 3
                               AND Utilisateur.IsActive = 1
     INNER JOIN Technicien AS T ON Technicien.RefRespZone = T.ID
     -- INNER JOIN Utilisateur U ON T.ID = U.RefId AND U.IsActive = 1

WHERE Technicien.RefRespZone = @SuperieurHie
      OR T.RefRespZone = @SuperieurHie
      OR Utilisateur.RefId = @SuperieurHie
      OR @SuperieurHie = 1
ORDER BY Utilisateur.Nom+' '+Utilisateur.Prenom;

-- SELECT * FROM Utilisateur WHERE RefRole = 3 AND IsActive = 1;