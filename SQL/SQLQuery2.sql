/****** Script for SelectTopNRows command from SSMS  ******/

SELECT ID,
       RefCarenceElement,
       RefEspece,
       RefType
FROM   BMS.dbo.CarenceNorme
-- WHERE  RefCarenceElement IN(1, 11, 8)
WHERE  ID IN(21, 23, 28,31)
AND RefEspece = 6
AND RefType = 1;