/****** Script for SelectTopNRows command from SSMS  ******/

SELECT DISTINCT  T.Cle AS Name, T.RefLangue,--T.RefElement, 
			 CASE T.RefLangue 
			 WHEN 1 THEN 'fr'
			 WHEN 2 THEN 'it'
			 WHEN 3 THEN 'es'
			 WHEN 4 THEN 'de'
			 WHEN 5 THEN 'gb'
			 WHEN 6 THEN 'pt'
			 WHEN 8 THEN 'nl'
			 END AS Culture,
			 CASE WHEN (LEN(T.Traduction) <> 0) THEN T.Traduction
			 ELSE (SELECT Traduction FROM BMS.dbo.Traduction WHERE RefElement = T.RefElement AND RefLangue = 1)
			 END AS default_value,
                T.Traduction AS value
FROM BMS.dbo.Traduction T WHERE T.Cle IS NOT NULL AND T.Cle like '%page%'-- = 'Adult' AND T.RefLangue = 1
--ORDER BY T.RefElement, T.RefLangue;

/*
select Culture, Name, Value from dbo.Resources where culture = @culture and name = @name;

*/

-- select Culture, Name, Value from dbo.Resources;

/*
ID	Nom		 IsActive	Nom_T
1	Français		1	524
2	Italien		0	525
3	Espagnol		1	526
4	Allemand		1	527
5	Anglais		0	528
6	Portugais		0	529
7	Argentin		0	526
8	Néerlandais	1	576
*/