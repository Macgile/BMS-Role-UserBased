declare @RefTechBMS int;
set @RefTechBMS = 51;


SELECT Distributeur.ID, Distributeur.Nom 
FROM Technicien AS Technicien_1 INNER JOIN Technicien ON Technicien_1.ID = Technicien.RefRespZone 
INNER JOIN Distributeur ON Technicien.ID = Distributeur.RefTechBMS  AND Distributeur.IsActive = 1
WHERE  (Distributeur.RefTechBMS = @RefTechBMS) 
OR   (Technicien_1.ID = @RefTechBMS) 
OR   (Technicien_1.RefRespZone = @RefTechBMS) ORDER BY Distributeur.Nom