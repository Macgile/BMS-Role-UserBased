SELECT B.RefReleve,
       R.Nom,
       P.RefClient,
       C.Nom+' '+C.Prenom AS Client,
       U.Nom+' '+U.Prenom AS Technicien,
       U.Login,
       U.PassWord
FROM   BMS.dbo.Bilan AS B
       INNER JOIN Releve R ON B.RefReleve = R.ID
       INNER JOIN Parcelle P ON R.RefParcelle = P.ID
       INNER JOIN Client C ON P.RefClient = C.ID
       INNER JOIN Utilisateur U ON C.RefTechBMS = U.RefId
ORDER BY R.Nom DESC;
GO
DECLARE @return_value INT;
EXEC @return_value = dbo.GetCumulClient
     @IdClient = 354,
     @Annee = 2014,
     @Langue = 1;
SELECT 'Return Value' = @return_value;
GO