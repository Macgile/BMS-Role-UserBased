SELECT CarenceNorme.ID
FROM Parcelle
     INNER JOIN Releve ON Parcelle.ID = Releve.RefParcelle
     INNER JOIN CarenceNorme ON Parcelle.RefComboEspece = CarenceNorme.RefEspece
WHERE Releve.ID = 9027
 AND CarenceNorme.RefCarenceElement = 1 -- @RefCarence
 AND CarenceNorme.RefType = 1 -- @CarenceType;
GO

SELECT TOP 1000 ID,
                RefCarenceNorme,
                RefReleve,
                RefComboCarence,
                ValeurAnalyseCarence
FROM BMS.dbo.CarenceReleve where id = 177