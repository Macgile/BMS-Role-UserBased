/*
   samedi 10 septembre 201612:31:53
   Utilisateur : sa
   Serveur : GILLES-PC
   Base de données : BMS
   Application : 
*/

/* Pour éviter les problèmes éventuels de perte de données, passez attentivement ce script en revue avant de l'exécuter en dehors du contexte du Concepteur de bases de données.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE NONCLUSTERED INDEX IX_element_langue ON dbo.Resources
	(
	RefElement,
	RefLangue
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.Resources ADD CONSTRAINT
	FK_Resources_Resources FOREIGN KEY
	(
	Culture,
	Name
	) REFERENCES dbo.Resources
	(
	Culture,
	Name
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Resources SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
