/****** Script for SelectTopNRows command from SSMS  ******/

SELECT CR.ID,
                RefCarenceNorme,
                RefReleve,
                RefComboCarence,
                ValeurAnalyseCarence,
			 CN.*
FROM            BMS.dbo.CarenceReleve CR INNER JOIN [dbo].[CarenceNorme] CN ON CR.RefCarenceNorme = CN.ID

 WHERE RefReleve = 9027
 AND CN.RefType = 1
