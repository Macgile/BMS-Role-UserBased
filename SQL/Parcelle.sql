/****** Script de la commande SelectTopNRows à partir de SSMS  ******/

SELECT TOP 1000 ID,
                RefClient,
                Nom,
                Surface,
                RefComboEspece,
                RefVariete,
                RefComboZoneClimatique,
                RefComboTypeIrrigation,
                RefComboModeProduction,
                IDUserCreat,
                IDUserModif,
                DatCreat,
                DatModif,
                IsActive
FROM BMS.dbo.Parcelle where id = 16397