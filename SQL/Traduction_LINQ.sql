/*
ID	Nom		 IsActive	Nom_T
1	Français		1	524
2	Italien		0	525
3	Espagnol		1	526
4	Allemand		1	527
5	Anglais		0	528
6	Portugais		0	529
7	Argentin		0	526
8	Néerlandais	1	576
*/

SELECT T.*
FROM
(
    SELECT FR.RefElement AS RefElement,
           CASE
               WHEN FR.Cle IS NULL
               THEN N''
               ELSE FR.Cle
           END AS           C1,
           FR.Traduction AS FR,
           IT.Traduction AS IT,
           ES.Traduction AS ES,
           DE.Traduction AS DE,
           GB.Traduction AS GB,
           PT.Traduction AS PT,
           NL.Traduction AS NL
    FROM dbo.Traduction AS FR
         LEFT OUTER JOIN dbo.Traduction AS IT ON IT.RefElement = FR.RefElement AND 2 = IT.RefLangue
         LEFT OUTER JOIN dbo.Traduction AS ES ON ES.RefElement = FR.RefElement AND 3 = ES.RefLangue
         LEFT OUTER JOIN dbo.Traduction AS DE ON DE.RefElement = FR.RefElement AND 4 = DE.RefLangue
         LEFT OUTER JOIN dbo.Traduction AS GB ON GB.RefElement = FR.RefElement AND 5 = GB.RefLangue
         LEFT OUTER JOIN dbo.Traduction AS PT ON PT.RefElement = FR.RefElement AND 6 = PT.RefLangue
         LEFT OUTER JOIN dbo.Traduction AS NL ON NL.RefElement = FR.RefElement AND 8 = NL.RefLangue
    WHERE 1 = FR.RefLangue
) AS T;