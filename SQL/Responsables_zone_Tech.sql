USE [BMS];
GO
DECLARE @SuperieurHie INT;
SET @SuperieurHie = 3;
SELECT Technicien.ID,
       Utilisateur.Nom +' '+ Utilisateur.Prenom AS Nom
FROM   Technicien
       INNER JOIN Utilisateur ON Technicien.ID = Utilisateur.RefId
                                 AND Utilisateur.RefRole = 3
                                 AND Utilisateur.IsActive = 1
       INNER JOIN Technicien AS Technicien_1 ON Technicien.RefRespZone = Technicien_1.ID
WHERE  Technicien.RefRespZone = @SuperieurHie
       OR Technicien_1.RefRespZone = @SuperieurHie
       OR Utilisateur.RefId = @SuperieurHie
       OR @SuperieurHie = 1
ORDER BY Nom 
-- Utilisateur.Nom +' '+ Utilisateur.Prenom;