-- YEARS CLIENT
SELECT DISTINCT
       LEFT(Releve.Nom, 4) AS Nom
	  , Bilan.RefReleve
       , Parcelle.ID as refPar
FROM   Bilan
       INNER JOIN Releve ON Bilan.RefReleve = Releve.ID
       INNER JOIN Parcelle ON Releve.RefParcelle = Parcelle.ID
WHERE  Parcelle.RefClient = 134
       AND Releve.IsActive = 1
       AND Releve.IsInitial = 0
       AND Parcelle.IsActive = 1
       AND Bilan.Qte IS NOT NULL
-- GROUP BY LEFT(Releve.Nom, 4);

-- YEARS DISTRIBUTEUR
SELECT DISTINCT
       LEFT(Releve.Nom, 4) AS Nom
FROM   Bilan
       INNER JOIN Releve ON Bilan.RefReleve = Releve.ID
       INNER JOIN Parcelle ON Releve.RefParcelle = Parcelle.ID
       INNER JOIN Client ON Parcelle.RefClient = Client.ID
       INNER JOIN Distributeur_Client ON Client.ID = Distributeur_Client.
       RefClient
WHERE  Distributeur_Client.RefDistributeur = 182
       AND Client.IsActive = 1
       AND Parcelle.IsActive = 1
       AND Releve.IsInitial = 0
       AND Bilan.Qte IS NOT NULL
GROUP BY Releve.Nom;

-- YEARS PARCELLE
SELECT DISTINCT
       LEFT(Releve.Nom, 4) AS Nom
FROM   Bilan
       INNER JOIN Releve ON Bilan.RefReleve = Releve.ID
       INNER JOIN Parcelle ON Releve.RefParcelle = Parcelle.ID
WHERE  Parcelle.ID = 16377
       AND Releve.IsActive = 1
       AND Releve.IsInitial = 0
       AND Parcelle.IsActive = 1
       AND Bilan.Qte IS NOT NULL
GROUP BY Releve.Nom;

-- YEARS RELEVE

SELECT DISTINCT
       LEFT(Releve.Nom, 4) AS Nom
FROM   Bilan
       INNER JOIN Produit ON Bilan.RefProduit = Produit.ID
       INNER JOIN Releve ON Bilan.RefReleve = Releve.ID
       INNER JOIN Parcelle ON Releve.RefParcelle = Parcelle.ID
WHERE  Bilan.RefReleve = 9091
       AND Bilan.Qte IS NOT NULL;