CREATE TABLE dbo.Resources
(Culture    VARCHAR(10) NOT NULL,
 Name       VARCHAR(100) NOT NULL,
 [Value]    NVARCHAR(4000) NOT NULL,
 RefElement INT NOT NULL,
 RefLangue  INT NOT NULL,
 IsDefaut   BIT not null
                CONSTRAINT pk_culture_name PRIMARY KEY CLUSTERED(Culture ASC, Name ASC, RefElement)
);