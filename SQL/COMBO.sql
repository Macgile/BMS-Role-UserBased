/****** Script de la commande SelectTopNRows à partir de SSMS  ******/

SELECT C.ID,
       C.NomCombo,
       CE.RefElement,
       T.Traduction,
       CE.*
FROM dbo.Combo AS C
     INNER JOIN dbo.Combo_Element AS CE ON CE.IDRefCombo = C.ID
     INNER JOIN Element E ON E.RefElement = CE.RefElement
     INNER JOIN Traduction T ON CE.RefElement = T.RefElement
WHERE T.RefLangue = 1
      AND CE.RefEspece = 6
      -- AND C.ID = 6
      AND C.NomCombo LIKE '%espec%';
GO

/****** Script de la commande SelectTopNRows à partir de SSMS  ******/

SELECT TOP 1000 ID,
                Nom_T,
                IsActive,
                Traduction
FROM BMS.dbo.Espece AS E
     INNER JOIN Traduction AS T ON E.Nom_T = T.RefElement
WHERE T.RefLangue = 1;
GO

/****** Script de la commande SelectTopNRows à partir de SSMS  ******/

SELECT TOP 1000 ID,
                Nom_T,
                IsActive
FROM BMS.dbo.Espece;
GO

/****** Script de la commande SelectTopNRows à partir de SSMS  ******/

SELECT TOP 1000 ID,
                RefClient,
                Nom,
                Surface,
                RefComboEspece,
                RefVariete,
                RefComboZoneClimatique,
                RefComboTypeIrrigation,
                RefComboModeProduction,
                IDUserCreat,
                IDUserModif,
                DatCreat,
                DatModif,
                IsActive
FROM BMS.dbo.Parcelle;
GO