/****** Script de la commande SelectTopNRows à partir de SSMS  ******/

SELECT -- C.ID,
       C.RefCarenceElement,
       -- C.RefEspece,
       C.NormeMin,
       C.NormeMax,
       --C.RefType,
       --CE.Nom_T,
       T.Traduction
FROM BMS.dbo.CarenceNorme C
     INNER JOIN dbo.CarenceElement CE ON CE.ID = C.RefCarenceElement
     INNER JOIN dbo.Traduction T ON CE.Nom_T = T.RefElement
                                    AND T.RefLangue = 1
WHERE C.RefEspece = 6
      AND C.RefType = 1;
GO
SELECT CarenceElement.ID,
       CarenceReleve.RefComboCarence,
       CarenceReleve.ValeurAnalyseCarence,
       CarenceNorme.RefType
FROM CarenceReleve
     INNER JOIN CarenceNorme ON CarenceReleve.RefCarenceNorme = CarenceNorme.ID
     INNER JOIN CarenceElement ON CarenceNorme.RefCarenceElement = CarenceElement.ID
WHERE CarenceReleve.RefReleve = 9027 -- @RefReleve
      AND CarenceNorme.RefType = 1; -- IN(1, 2, 3);-- @RefType; loop 1 to 3
GO
SELECT CN.ID AS ID,
       CN.RefCarenceElement AS RefCarenceElement,
       CN.RefEspece AS RefEspece,
       CN.NormeMax AS NormeMax,
       CN.NormeMin AS NormeMin,
       CN.RefType AS RefType
FROM dbo.CarenceNorme AS CN
WHERE CN.RefType = 1
      AND CN.RefEspece = 6;
GO
SELECT ID,
       RefCarenceElement,
       RefEspece,
       NormeMax,
       NormeMin,
       RefType
FROM BMS.dbo.CarenceNorme;
GO
SELECT ID,
       Nom_T
FROM BMS.dbo.CarenceType;
SELECT CE.RefElement,
       CE.Value,
       dbo.Trans(CE.RefElement, 1) AS Traduction,
       CE.RefEspece
FROM Combo_Element AS CE
     INNER JOIN Combo AS C ON CE.IDRefCombo = C.ID
WHERE C.NomCombo = 'cmbTypeCarence'
      AND CE.RefEspece = 1
ORDER BY CE.Ordre DESC;
GO

/****** Script for SelectTopNRows command from SSMS  ******/

SELECT ID,
       RefCarenceNorme,
       RefReleve,
       RefComboCarence,
       ValeurAnalyseCarence
FROM BMS.dbo.CarenceReleve
WHERE ID = 177;
GO
SELECT CR.ID AS ID,
       CR.RefCarenceNorme AS RefCarenceNorme,
       CR.RefReleve AS RefReleve,
       CR.RefComboCarence AS RefComboCarence,
       CR.ValeurAnalyseCarence AS ValeurAnalyseCarence
FROM dbo.CarenceReleve AS CR
WHERE CR.ID = 177;
--CR.RefReleve = 177; --198; -- 184; --CR.ID = 177;


SELECT CR.RefReleve,
       COUNT(CR.ID)
FROM dbo.CarenceReleve AS CR
GROUP BY CR.RefReleve;



/*

update dbo.CarenceNorme set NormeMax = 205.12, NormeMin = 68.21 where ID = 301


*/