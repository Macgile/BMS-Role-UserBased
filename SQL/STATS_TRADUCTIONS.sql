
SELECT Count(T.RefLangue) AS [Nbr Traduction], T.RefLangue, L.Nom
FROM   BMS.dbo.Traduction T JOIN BMS.dbo.Langue L ON T.RefLangue = L.ID    group by T.RefLangue, L.Nom order by T.RefLangue;

select DISTINCT T.RefElement, 
( select Traduction from  BMS.dbo.Traduction where RefLangue = 1 AND RefElement = T.RefElement) AS FR,
( select Traduction from  BMS.dbo.Traduction where RefLangue = 2 AND RefElement = T.RefElement) AS IT,
( select Traduction from  BMS.dbo.Traduction where RefLangue = 3 AND RefElement = T.RefElement) AS ES,
( select Traduction from  BMS.dbo.Traduction where RefLangue = 4 AND RefElement = T.RefElement) AS DE,
( select Traduction from  BMS.dbo.Traduction where RefLangue = 5 AND RefElement = T.RefElement) AS GB,
( select Traduction from  BMS.dbo.Traduction where RefLangue = 6 AND RefElement = T.RefElement) AS PT,
( select Traduction from  BMS.dbo.Traduction where RefLangue = 7 AND RefElement = T.RefElement) AS AR,
( select Traduction from  BMS.dbo.Traduction where RefLangue = 8 AND RefElement = T.RefElement) AS NL
from BMS.dbo.Traduction T
