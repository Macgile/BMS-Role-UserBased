/* UTILISATEURS RESPONSABLE
AYANT AU MOINS UN UTILISATEUR SOUS SA RESPONSABILITE */
SELECT DISTINCT
       T2.ID AS [ID technicien],
       T2.RefRespZone,
       '' AS [		   ],
       U.ID AS [ID Utilisateur],
       U.Nom+' '+U.Prenom AS Utilisateur,
       U.RefRole,
	  U.isActive,
	  U.RefId,
       COALESCE(
               (
                   SELECT TOP 1 Utilisateur.Nom+' '+Utilisateur.Prenom
                   FROM          Technicien
                                 JOIN Utilisateur ON Utilisateur.RefId = Technicien.ID
                   WHERE        Technicien.ID = T2.RefRespZone
               ), '') AS Responsable
FROM Technicien AS T1
     JOIN Technicien AS T2 ON T1.RefRespZone = T2.ID
     JOIN Utilisateur AS U ON T2.ID = U.RefId --AND U.IsActive = 1
WHERE U.RefRole IN(1, 2, 3)
     AND T1.ID IN
(
    SELECT RefId
    FROM   Utilisateur
    WHERE  RefRole IN(1, 2, 3)
)
ORDER BY T2.RefRespZone,
         U.RefRole;



-- update Utilisateur set Nom = Nom + ' Tech'  WHERE ID = 80