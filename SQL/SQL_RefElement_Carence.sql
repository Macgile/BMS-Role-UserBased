/****** Script for SelectTopNRows command from SSMS  ******/

SELECT CR.ID,
       CR.RefCarenceNorme,
       CR.RefReleve,
       CR.RefComboCarence,
       CR.ValeurAnalyseCarence,
       CN.RefCarenceElement,
	  CN.RefType
FROM   BMS.dbo.CarenceReleve CR
       INNER JOIN CarenceNorme CN ON CR.RefCarenceNorme = CN.ID
	  WHERE CR.RefReleve = 9027
	  AND CN.RefType = 1
	  ORDER BY CR.ID
--WHERE CN.RefType <> 1;
--WHERE CR.RefReleve = 9027; -- recupere les id corrspondant a refcarencenorme
/*
274 releve avec deux types (1 et 2)  Feuilles(1) - Fruits(2) - Rameaux(3)
TOUS LES AUTRES SONT AVEC UN TYPE UNIQUE : 1
*/