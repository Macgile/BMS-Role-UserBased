USE [BMS]
GO
/****** Object:  StoredProcedure [dbo].[GetCumul]    Script Date: 09/11/2016 14:04:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[GetCumul] 
	(
	@IdDistributeur int ,
	@Annee int ,
	@Langue int
	)
AS
SELECT     dbo.Trans(Produit.Nom_T, @Langue) AS Produit, SUM(Bilan.Qte * Parcelle.Surface * Bilan.NbApplication) AS TotQte, Produit.UnitProd, Parcelle.ID AS ParcelleID, 
                      Parcelle.Nom AS ParcelleNom, Client.ID AS ClientID, Client.Nom AS ClientNom, Variete.ID AS VarieteID, dbo.Trans(Variete.Nom_T, @Langue) AS VarieteNom, 
                      Espece.ID AS EspeceID, dbo.Trans(Espece.Nom_T, @Langue) AS EspeceNom
FROM         Bilan INNER JOIN
                      Produit ON Bilan.RefProduit = Produit.ID INNER JOIN
                      Releve ON Bilan.RefReleve = Releve.ID INNER JOIN
                      Parcelle ON Releve.RefParcelle = Parcelle.ID INNER JOIN
                      Client ON Parcelle.RefClient = Client.ID INNER JOIN
                      Distributeur_Client ON Client.ID = Distributeur_Client.RefClient INNER JOIN
                      Variete ON Parcelle.RefVariete = Variete.ID INNER JOIN
                      Espece ON Variete.RefEspece = Espece.ID
WHERE     (LEFT(Releve.Nom, 4) = @Annee) 
			AND (Distributeur_Client.RefDistributeur = @IdDistributeur) 
			AND (Releve.IsInitial = 0) 
			AND (Parcelle.IsActive = 1) 
			AND (Client.IsActive = 1) 
			AND (Releve.IsActive = 1) 
			AND (Releve.IsInitial = 0)
			AND Bilan.Qte IS NOT NULL
			AND Bilan.IsValide = 1
GROUP BY dbo.Trans(Produit.Nom_T, @Langue), Produit.UnitProd, Parcelle.ID, Parcelle.Nom, Client.ID, Client.Nom, Variete.ID, Espece.ID, dbo.Trans(Espece.Nom_T, @Langue), 
                      dbo.Trans(Variete.Nom_T, @Langue)
ORDER BY dbo.Trans(Produit.Nom_T, @Langue)
	RETURN;
GO

ALTER PROCEDURE [dbo].[GetCumulReleve] 
	(
	@Langue int,
	 @IdReleve int
	)
AS
SELECT     dbo.Trans(Produit.Nom_T, @Langue) AS Produit, SUM(Bilan.Qte * Parcelle.Surface * Bilan.NbApplication) AS TotQte, Produit.UnitProd
FROM         Bilan INNER JOIN
                      Produit ON Bilan.RefProduit = Produit.ID INNER JOIN
                      Releve ON Bilan.RefReleve = Releve.ID INNER JOIN
                      Parcelle ON Releve.RefParcelle = Parcelle.ID
WHERE     (Bilan.RefReleve = @IdReleve) 
			AND Bilan.Qte IS NOT NULL
			AND Bilan.IsValide = 1
GROUP BY dbo.Trans(Produit.Nom_T, @Langue), Parcelle.Surface, Produit.UnitProd
ORDER BY dbo.Trans(Produit.Nom_T, @Langue)
	RETURN;
GO

ALTER PROCEDURE [dbo].[GetCumulParcelle]
	(
	@IdParcelle int ,
	@Annee int ,
	@Langue int
	)
AS
SELECT     dbo.Trans(Produit.Nom_T, @Langue) AS Produit, SUM(Bilan.Qte * Parcelle.Surface * Bilan.NbApplication) AS TotQte, Produit.UnitProd
FROM         Bilan INNER JOIN
                      Produit ON Bilan.RefProduit = Produit.ID INNER JOIN
                      Releve ON Bilan.RefReleve = Releve.ID INNER JOIN
                      Parcelle ON Releve.RefParcelle = Parcelle.ID
WHERE     (LEFT(Releve.Nom, 4) = @Annee) 
			AND (Parcelle.ID = @IdParcelle) 
			AND (Releve.IsActive = 1) 
			AND (Releve.IsInitial = 0)
			AND Bilan.Qte IS NOT NULL
			AND Bilan.IsValide = 1
GROUP BY dbo.Trans(Produit.Nom_T, @Langue), Parcelle.RefClient, Releve.Nom, Produit.UnitProd
ORDER BY dbo.Trans(Produit.Nom_T, @Langue)
	RETURN;
GO

ALTER PROCEDURE [dbo].[GetCumulDistributeur](@IdDistributeur INT,
                                         @Annee          INT,
                                         @Langue         INT)
AS
     SELECT dbo.Trans(Produit.Nom_T, @Langue) AS Produit,
            SUM(Bilan.Qte * Parcelle.Surface * Bilan.NbApplication) AS
            TotQte,
            Produit.UnitProd
     FROM   Bilan
            INNER JOIN Produit ON Bilan.RefProduit = Produit.ID
            INNER JOIN Releve ON Bilan.RefReleve = Releve.ID
            INNER JOIN Parcelle ON Releve.RefParcelle = Parcelle.ID
            INNER JOIN Client ON Parcelle.RefClient = Client.ID
            INNER JOIN Distributeur_Client ON Client.ID =
            Distributeur_Client.RefClient
     WHERE  LEFT(Releve.Nom, 4) = @Annee
            AND Distributeur_Client.RefDistributeur = @IdDistributeur
            AND Client.IsActive = 1
            AND Parcelle.IsActive = 1
            AND Releve.IsInitial = 0
		  AND Bilan.IsValide = 1
     GROUP BY dbo.Trans(Produit.Nom_T, @Langue),
              Produit.UnitProd
     ORDER BY dbo.Trans(Produit.Nom_T, @Langue);
     RETURN;
	
GO

ALTER PROCEDURE [dbo].[GetCumulClient] 
	(
	@IdClient int ,
	@Annee int ,
	@Langue int
	)
AS
SELECT     dbo.Trans(Produit.Nom_T, @Langue) AS Produit, SUM(Bilan.Qte * Parcelle.Surface * Bilan.NbApplication) AS TotQte, Produit.UnitProd
FROM         Bilan INNER JOIN
                      Produit ON Bilan.RefProduit = Produit.ID INNER JOIN
                      Releve ON Bilan.RefReleve = Releve.ID INNER JOIN
                      Parcelle ON Releve.RefParcelle = Parcelle.ID
WHERE     (LEFT(Releve.Nom, 4) = @Annee) 
			AND (Parcelle.RefClient = @IdClient) 
			AND (Releve.IsActive = 1) 
			AND (Releve.IsInitial = 0) 
			AND (Parcelle.IsActive = 1)
			AND Bilan.Qte IS NOT NULL
			AND Bilan.IsValide = 1
GROUP BY dbo.Trans(Produit.Nom_T, @Langue), Parcelle.RefClient, Releve.Nom, Produit.UnitProd
ORDER BY dbo.Trans(Produit.Nom_T, @Langue)
RETURN;
GO