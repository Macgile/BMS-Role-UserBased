DECLARE @SuperieurHie AS INT;
SET @SuperieurHie = 1;
--
--
--
--

SELECT Technicien.ID,
       Utilisateur.Nom+' '+Utilisateur.Prenom AS Nom,
       Utilisateur.IsActive,
       Utilisateur.ID,
       Utilisateur.RefRole
	  ,Technicien.RefRespZone
	  --,T.RefRespZone

FROM   Technicien
       INNER JOIN Utilisateur ON Technicien.ID = Utilisateur.RefId
                                 AND Utilisateur.IsActive = 1
                                 AND Utilisateur.RefRole = 3 -- technicien
       INNER JOIN Technicien AS T ON Technicien.RefRespZone = T.ID

WHERE  Technicien.RefRespZone	  = @SuperieurHie
       OR T.RefRespZone		  = @SuperieurHie
       OR Utilisateur.RefId	  = @SuperieurHie
       OR @SuperieurHie		  = 1
ORDER BY Utilisateur.Nom;


-- UPDATE Utilisateur SET IsActive = 1 WHERE  ID = 66;