DECLARE
  @SuperieurHie AS INT;

SET @SuperieurHie=1;

SELECT Technicien.ID,
       Utilisateur.Nom+' '+Utilisateur.Prenom
AS Nom,
       Utilisateur.RefRole
FROM Technicien
INNER JOIN
Utilisateur
ON Technicien.ID=Utilisateur.RefId
    INNER JOIN
    Technicien
AS Technicien_1
    ON Technicien.RefRespZone=Technicien_1.ID
       AND Utilisateur.IsActive=1
WHERE Technicien.RefRespZone=@SuperieurHie
      OR Technicien_1.RefRespZone=@SuperieurHie
      OR Utilisateur.RefId=@SuperieurHie
      OR @SuperieurHie=1
ORDER BY Utilisateur.Nom+' '+Utilisateur.Prenom;