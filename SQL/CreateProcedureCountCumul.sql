USE [BMS]
GO

/****** Object:  StoredProcedure [dbo].[HasCumul]    Script Date: 23/08/2016 17:51:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[HasCumul](@Id   INT,
                             @Type INT,
                             @Num  INT OUTPUT)
AS
     BEGIN
         SET NOCOUNT ON;
         IF @Type = 1
             BEGIN
                 -- SELECT @Num_of_user =COUNT(*) 
                 SELECT @Num = 'Clients';
             END;
         ELSE
         IF @Type = 2
             BEGIN
                 SELECT @Num = 'Distributeur';
             END;
         ELSE
         IF @Type = 3
             BEGIN
                 SELECT @Num = 'Parcelle';
             END;;
         SELECT @Num;
         RETURN;
     END;
GO


