USE [BMS]
GO

DECLARE	@return_value int

EXEC	@return_value = [dbo].[sp_GetClients]
		@Nom = N'',
		@CodePostal = N'',
		@IDTechnicien = 1,
		@IDDistributeur = 0,
		@Role = 3

SELECT	'Return Value' = @return_value

GO
