SELECT DISTINCT T.ID,
       T.RefRespZone,
       U.ID,
       U.RefId,
       U.RefRole,
       U.Nom,
       U.Prenom,
	  U.Login,
	  U.PassWord,
	  U.IsActive,
	  U.RefLangue
FROM dbo.Technicien T
     INNER JOIN dbo.Technicien R ON T.RefRespZone = R.ID AND T.IsActive = 1
     INNER JOIN Utilisateur U ON T.RefRespZone = U.RefId AND U.IsActive = 1
	order by U.RefRole
-- WHERE T.ID <> T.RefRespZone;