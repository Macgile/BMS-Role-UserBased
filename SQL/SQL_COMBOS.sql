--GETCOMBO
SELECT CE.RefElement,
       T.Traduction
FROM   Combo_Element AS CE
       INNER JOIN Combo AS C ON CE.IDRefCombo = C.ID
       INNER JOIN Traduction T ON T.RefElement = CE.RefElement
WHERE  C.NomCombo = 'cmbCarence'
       AND T.RefElement = CE.RefElement
           AND T.RefLangue = 1
-- OR (T.RefElement = CE.RefElement AND T.IsDefaut = 1)
ORDER BY CE.Ordre DESC;
GO

--GETCOMBONUMERIC
SELECT dbo.Combo_Element_Numeric.Valeur,
       dbo.Combo_Element_Numeric.Libelle
FROM   dbo.Combo_Element_Numeric
       INNER JOIN dbo.Combo ON dbo.Combo_Element_Numeric.RefCombo = dbo.
       Combo.ID
WHERE  dbo.Combo.NomCombo = 'cmbTypeTaille';
GO

-- GETCOMBO-ESPECE
SELECT CE.RefElement,
       CE.Value,
       dbo.Trans(CE.RefElement, 1) AS Traduction
FROM   Combo_Element AS CE
       INNER JOIN Combo AS C ON CE.IDRefCombo = C.ID
WHERE  C.NomCombo = 'cmbTypeCarence'
       AND CE.RefEspece = 7
ORDER BY CE.Ordre DESC;
GO

-- GET-GREFFE
SELECT G.ID,
       G.VigGref,
       G.IPC,
       T.Traduction
FROM   Greffe G
       INNER JOIN Traduction T ON G.Nom_T = T.RefElement
WHERE  G.RefEspece = 7
       AND G.IsActive = 1
       AND T.RefLangue = 1
ORDER BY T.Traduction;
GO
-- GET-GREFFE SELECTED GREFFE ITEM
SELECT G.ID,
       G.VigGref,
       G.IPC,
       T.Traduction
FROM   Greffe G
       INNER JOIN Traduction T ON G.Nom_T = T.RefElement
WHERE  G.RefEspece = 7 -- Parcelle.refComboEspece
       AND G.IsActive = 1
       AND G.ID = 2 -- Releve.RefcomboGreffe
       AND T.RefLangue = 1
ORDER BY T.Traduction;
GO

-- ESPECE 
SELECT ID,
       T.Traduction
FROM   BMS.dbo.Espece E
       INNER JOIN Traduction T ON E.Nom_T = T.RefElement
WHERE  T.RefLangue = 1;

GO

DECLARE @SuperieurHie int;
SET    @SuperieurHie = 1;

SELECT T.ID,
       U.Nom+' '+U.Prenom AS Nom,
	  U.IsActive
FROM   Technicien AS T
       INNER JOIN Utilisateur AS U ON (T.ID = U.RefId AND U.IsActive = 1)
       INNER JOIN Technicien AS T1 ON T.RefRespZone = T1.ID
WHERE  U.RefRole = 3 AND T.RefRespZone = @SuperieurHie
       OR U.RefRole = 3  AND T1.RefRespZone = @SuperieurHie
       OR U.RefRole = 3  AND U.RefId = @SuperieurHie
       OR U.RefRole = 3  AND @SuperieurHie = 1
ORDER BY Nom;

/*
SELECT CarenceNorme.RefCarenceElement,
       CarenceNorme.NormeMax,
       CarenceNorme.NormeMin
FROM CarenceNorme
     INNER JOIN CarenceElement ON CarenceNorme.RefCarenceElement = CarenceElement.ID
WHERE CarenceNorme.RefEspece = @RefEspece
      AND CarenceNorme.RefType = @RefType;

*/