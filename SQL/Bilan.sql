DECLARE @Langue AS INT;
DECLARE @IdReleve AS INT;
SET @Langue = 1;
SET @IdReleve = 244;--436;8913

EXEC dbo.GenerateBilanAutoWeb
     @IdReleve = @IdReleve;



/*
    ETAPE
*/

SELECT Etape.ID,
       Etape.ImgEtap,
       Etape.RefEspece,
       dbo.Trans(Etape.Nom_T, @Langue) AS Nom,
       dbo.Trans(Etape.Description_T, 1) AS Description
FROM   Parcelle
       INNER JOIN Releve ON Parcelle.ID = Releve.RefParcelle
       INNER JOIN Etape ON Parcelle.RefComboEspece = Etape.RefEspece
WHERE  Releve.ID = @IdReleve
       OR Releve.ID = @IdReleve
ORDER BY Etape.Ordre;


/*
    PROGRAMME
*/

SELECT ProgrammeNutrition.ID,
       dbo.Trans(ProgrammeNutrition.Nom_T, @Langue) AS Nom,
       dbo.Trans(ProgrammeNutrition.Description_T, @Langue) AS Description
FROM   Releve
       INNER JOIN Parcelle ON Releve.RefParcelle = Parcelle.ID
       INNER JOIN ProgrammeNutrition ON Parcelle.RefComboEspece =
       ProgrammeNutrition.RefEspece
WHERE  Releve.ID = @IdReleve
       OR Releve.ID = @IdReleve;

/*
    BILAN
*/

SELECT Bilan.ID,
       Bilan.RefProgrammeNutrition,
       Bilan.RefEtape,
       Bilan.RefProduit,
       Bilan.Qte,
       Bilan.QteInitial,
       Bilan.IsValide,
       Bilan.IsPerso,
       Releve.Nom AS Releve,
       Parcelle.Nom AS Parcelle,
       dbo.Trans(Espece.Nom_T, @Langue) AS Espece,
       dbo.Trans(Variete.Nom_T, @Langue) AS Variete,
       dbo.Trans(Produit.Nom_T, @Langue) AS Produit,
       Produit.UnitProd,
       Parcelle.Surface,
       CASE
           WHEN Bilan.Qte = Bilan.QteInitial
           THEN 0
           WHEN Bilan.Qte IS NULL
           THEN 2
           WHEN Bilan.QteInitial IS NULL
           THEN 3
           ELSE 1
       END AS PersoCase
FROM   Bilan
       INNER JOIN Releve ON Bilan.RefReleve = Releve.ID
       INNER JOIN Parcelle ON Releve.RefParcelle = Parcelle.ID
       INNER JOIN Variete ON Parcelle.RefVariete = Variete.ID
       INNER JOIN Espece ON Variete.RefEspece = Espece.ID
       INNER JOIN Produit ON Bilan.RefProduit = Produit.ID
WHERE  Bilan.RefReleve = @IdReleve;
--AND RefProgrammeNutrition = 51 AND RefEtape = 172;

/*
    INFOS GARANTIES
*/

SELECT Releve.RefComboTypeUser,
       Utilisateur.Nom,
       Utilisateur.Prenom
FROM   Releve
       INNER JOIN Parcelle ON Releve.RefParcelle = Parcelle.ID
       INNER JOIN Client ON Parcelle.RefClient = Client.ID
       INNER JOIN Technicien ON Client.RefTechBMS = Technicien.ID
       INNER JOIN Utilisateur ON Technicien.ID = Utilisateur.RefId
WHERE  Releve.ID = @IDReleve;