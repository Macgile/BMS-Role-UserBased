/****** Script for SelectTopNRows command from SSMS  ******/

SELECT CR.ID,
       CR.RefCarenceNorme,
       CR.RefReleve,
       CR.RefComboCarence,
       CR.ValeurAnalyseCarence,
	  CN.RefType,
	  CN.RefCarenceElement,
	  CN.RefEspece
FROM   BMS.dbo.CarenceReleve CR
       INNER JOIN dbo.CarenceNorme CN ON CR.RefComboCarence = CN.RefCarenceElement
	  
	  AND CN.RefEspece = 6
WHERE  CR.RefReleve = 9027;

/*
delete BMS.dbo.CarenceReleve WHERE ID IN (523,524,525) AND RefReleve = 9027
*/