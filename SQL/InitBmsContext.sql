DECLARE @IdDistri AS INT;
DECLARE @IDTech AS INT;
SET @IdDistri = 49;
SET @IDTech = 1;
SELECT RefTechBMS AS IdTech,
       Nom AS mNomDistributeur
FROM   Distributeur
WHERE  ID = @IdDistri;
SELECT RefRespZone AS mIdRespZone
FROM   Technicien
WHERE  ID = @IDTech;