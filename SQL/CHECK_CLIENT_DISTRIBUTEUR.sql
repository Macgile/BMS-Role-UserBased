/****** Script for SelectTopNRows command from SSMS  ******/

SELECT RefDistributeur,
       RefClient
FROM BMS.dbo.Distributeur_Client
WHERE RefDistributeur IN
(
    SELECT id
    FROM Distributeur
    WHERE IsActive = 0
);

SELECT RefDistributeur,
       CAST(RefClient AS VARCHAR(100)) + ','
FROM BMS.dbo.Distributeur_Client
WHERE RefClient IN
(
    SELECT id
    FROM Client
    WHERE IsActive = 0
);

