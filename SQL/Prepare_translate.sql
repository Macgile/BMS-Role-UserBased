/****** Script de la commande SelectTopNRows à partir de SSMS  ******/

/*
INSERT INTO table2
(column_name(s))
SELECT column_name(s)
FROM table1;
Culture    VARCHAR(10) NOT NULL,
 Name       VARCHAR(100) NOT NULL,
 [Value]    NVARCHAR(4000) NOT NULL,
 RefElement INT NOT NULL,
 RefLangue  INT NOT NULL
 MID(City,1,4)


*/

USE BMS;
GO
INSERT INTO BMS.dbo.Resources
(Culture,
 Name,
 Value,
 RefElement,
 RefLangue,
 IsDefaut
)
       SELECT LEFT(L.Nom, 10) AS Culture,
              LEFT(T.Traduction, 99) AS Name,
              T.Traduction AS Value,
              T.RefElement,
              T.RefLangue,
              T.IsDefaut
       FROM BMS.dbo.Traduction T
            INNER JOIN dbo.Langue L ON T.RefLangue = L.ID
       WHERE T.RefElement IN(4, 29, 30, 41, 86, 88, 90, 92, 94, 95, 96, 97, 98, 100, 101, 102, 103, 104, 109, 110, 113, 114, 121, 122, 130, 131, 132, 133, 134, 137, 138, 139, 140, 141, 142, 143, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 156, 157, 158, 159, 160, 161, 162, 163, 165, 167, 168, 169, 171, 172, 173, 174, 175, 177, 181, 182, 183, 184, 185, 188, 193, 194, 197, 199, 200, 205, 206, 207, 208, 232, 240, 241, 242, 243, 244, 245, 246, 255, 256, 257, 258, 259, 260, 261, 262, 265, 266, 267, 274, 500, 501, 502, 510, 511, 520, 521, 522, 523, 530, 531, 532, 533, 534, 535, 536, 537, 538, 539, 540, 541, 542, 543, 544, 545, 546, 547, 548, 549, 550, 551, 552, 553, 554, 555, 556, 562, 571, 572, 600, 620, 630, 650, 1011, 1013, 1018, 1019, 1020, 1021, 1024, 1025, 1030, 1031, 1035, 1037, 1039, 1040, 1041, 1042, 1043, 1044, 1045, 1046, 1047, 1048, 1050, 1051, 1052, 1053, 1054, 1058, 1059, 1060, 99015, 99025, 99040, 99060, 99280, 99380, 99430, 99500, 99510, 99520, 99525, 99530, 99540, 99542, 99543, 99544, 99545, 99546, 99547, 99570, 99575, 99580, 99585, 99590, 99600, 99601, 99602, 99603, 99700, 99701, 99710, 99740, 99750, 99760, 99770, 99780, 99790, 99800, 99810, 99820, 99830, 99840, 99850, 99860, 99870, 99880, 99890, 99895)
       -- AND T.RefLangue = 4
	  ORDER BY Name, T.RefElement,
                T.RefLangue;

-- (Allemand, Unbekannt).
