/****** Script for SelectTopNRows command from SSMS  ******/

SELECT TOP 4 ID,
             Nom_T,
             T.Traduction
FROM         BMS.dbo.CarenceElement
             INNER JOIN Traduction T ON T.RefElement = Nom_T
                                        AND T.RefLangue = 1;
GO
SELECT CN.ID,
             CN.RefCarenceElement,
             CN.RefEspece,
             CN.NormeMax,
             CN.NormeMin,
             CN.RefType,
             CE.ID,
             CE.Nom_T,
             T.Traduction
FROM         BMS.dbo.CarenceNorme CN
             INNER JOIN dbo.CarenceElement CE ON CE.ID = CN.
             RefCarenceElement
             INNER JOIN Traduction T ON CE.Nom_T = T.RefElement
                                        AND T.RefLangue = 1
WHERE        CN.RefEspece = 6
AND         CN.ID IN(21, 23, 28,31)
            -- AND CN.RefType = 1;
GO
SELECT CarenceNorme.RefCarenceElement,
       CarenceNorme.NormeMax,
       CarenceNorme.NormeMin
FROM   CarenceNorme
       INNER JOIN CarenceElement ON CarenceNorme.RefCarenceElement =
       CarenceElement.ID
WHERE  CarenceNorme.RefEspece = 6
       AND CarenceNorme.RefType = 1;
GO
SELECT CarenceElement.ID,
       CarenceReleve.RefComboCarence,
       CarenceReleve.ValeurAnalyseCarence
FROM   CarenceReleve
       INNER JOIN CarenceNorme ON CarenceReleve.RefCarenceNorme = CarenceNorme.ID
       INNER JOIN CarenceElement ON CarenceNorme.RefCarenceElement = CarenceElement.ID
WHERE  CarenceReleve.RefReleve = 184
       AND CarenceNorme.RefType = 1;