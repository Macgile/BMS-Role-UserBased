/****** Script for SelectTopNRows command from SSMS  ******/

SELECT DISTINCT C.Nom,
       P.ID AS ID_PARCEL,
       P.Nom , R.*
FROM BMS.dbo.Client C
     INNER JOIN Parcelle P ON C.ID = P.RefClient
     INNER JOIN (SELECT RefParcelle, MAX(DateReleve) AS DateReleve FROM Releve GROUP BY RefParcelle) R ON P.ID = R.RefParcelle
WHERE C.ID = 110;

