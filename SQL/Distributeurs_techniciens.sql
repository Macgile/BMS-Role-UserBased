/****** Script de la commande SelectTopNRows à partir de SSMS  ******/

SELECT D.ID,
       D.Nom,
       D.RefTechBMS,
	  U.Nom, U.Prenom, U.RefId, U.RefRole
	  ,T.ID, T.RefRespZone
FROM BMS.dbo.Distributeur D join Technicien T ON D.RefTechBMS = T.ID
JOIN Utilisateur U ON T.ID = U.RefId 

--WHERE RefTechBMS IN (
--    SELECT ID
--    FROM Technicien -- where RefRole = 3
--);

--WHERE RefTechBMS IN (
--    SELECT RefId
--    FROM Utilisateur -- where RefRole = 3
--);