﻿using System;
using System.Globalization;
using System.Security.Cryptography;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using Newtonsoft.Json;
using SiteBMS.DAL.Security;

namespace SiteBMS
{
    public enum RoleType
    {
        Pdg = 1,
        ResponsableZone,    //2
        Technicien,         //3
        Distributeur,       //4
        Client,             //5
        Demo                //6
    }

    public enum EnumEspece
    {
        Abricot = 1,
        Peche = 5,
        Poire,      //6
        Pomme,      //7
        Prune,      //8
        Raisin,     //9
        Vigne,      //10
        Aucune,     //11
        Cereales,   //12
        Mais        //13
    }

    public enum EspeceType
    {
        Pepin = 1,
        Noyaux,
        Vigne,
        Cereales,
        Mais
    }

    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            var newCulture = (CultureInfo)CultureInfo.CurrentCulture.Clone();
            newCulture.NumberFormat.NumberDecimalSeparator = ".";

            Thread.CurrentThread.CurrentCulture = newCulture;
            Thread.CurrentThread.CurrentUICulture = newCulture;

            //ModelBinders.Binders.Add(typeof(decimal), new DecimalModelBinder());

            // mapper views=>viewModel, ViewModel=>Views
            MapperConfig.Configure();

            AreaRegistration.RegisterAllAreas();
            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            //HtmlEncoder.Default =
            //    new System.Text.Encodings.Web.HtmlEncoder(UnicodeRanges.All, UnicodeRanges.SpacingModifierLetters);

            // initialize default account
            // Database.SetInitializer(new DataContextInitializer());
        }

        protected void Application_PostAuthenticateRequest(object sender, EventArgs e)
        {
            var authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
            if (authCookie == null) return;

            try
            {
                var authTicket = FormsAuthentication.Decrypt(authCookie.Value);

                if (authTicket == null)
                    return; // Response.Redirect("Account");

                var serializeModel = JsonConvert.DeserializeObject<CustomPrincipalSerializeModel>(authTicket.UserData);

                var newUser = new CustomPrincipal(authTicket.Name)
                {
                    UserId = serializeModel.UserId,
                    LanguageId = serializeModel.LanguageId,
                    PayId = serializeModel.PayId,
                    FirstName = serializeModel.FirstName,
                    LastName = serializeModel.LastName,
                    Roles = serializeModel.Roles,
                    RefRole = serializeModel.RefRole,
                    RefId = serializeModel.RefId,
                    RoleName = serializeModel.RoleName,
                    RoleId = serializeModel.RoleId,
                    EnumRole = (RoleType)Enum.ToObject(typeof(RoleType), serializeModel.RefRole)
                };

                switch (newUser.EnumRole)
                {
                    // pdg et responsable non sont pas des technicien
                    case RoleType.Pdg:
                    case RoleType.ResponsableZone: break;

                    // IdTech est le refid du compte utilisateur de role technicien
                    case RoleType.Technicien:
                        newUser.IdTech = newUser.RefId;
                        break;

                    // IdDistributeur est le refid du compte utilisateur de role distributeur
                    case RoleType.Distributeur:
                        newUser.IdDistributeur = newUser.RefId;
                        break;

                    // IdClient est le refid du compte utilisateur de role Client
                    case RoleType.Client:
                        newUser.IdClient = newUser.RefId;
                        break;
                }

                HttpContext.Current.User = newUser;
            }
            catch (CryptographicException)
            {
                FormsAuthentication.SignOut();
            }
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            var newCulture = (CultureInfo)CultureInfo.CurrentCulture.Clone();
            newCulture.NumberFormat.NumberDecimalSeparator = ".";

            Thread.CurrentThread.CurrentCulture = newCulture;
            Thread.CurrentThread.CurrentUICulture = newCulture;
        }
    }

    public class DecimalModelBinder : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext,
            ModelBindingContext bindingContext)
        {
            ValueProviderResult valueResult = bindingContext.ValueProvider
                .GetValue(bindingContext.ModelName);
            ModelState modelState = new ModelState { Value = valueResult };
            object actualValue = null;
            try
            {
                actualValue = Convert.ToDecimal(valueResult.AttemptedValue,
                    CultureInfo.CurrentCulture);
            }
            catch (FormatException e)
            {
                modelState.Errors.Add(e);
            }

            bindingContext.ModelState.Add(bindingContext.ModelName, modelState);
            return actualValue;
        }
    }
}