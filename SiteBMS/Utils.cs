﻿using SiteBMS.DAL;
using SiteBMS.DAL.Security;
using System.Diagnostics;
using System.Linq;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;


namespace SiteBMS
{
    public static class Utils // sealed to ensure the utility class won't be inherited

    {
        // Ensure you cannot instantiate an object of this type

        //Add static methods which means you can reference them
        //anywhere in the project simply by using Utils.
        //This one keeps the name of the connection string in just 2 places -
        //once in the Web.Config, and once in the Utils class.
        public static string UcFirst(string s)
        {
            // Check for empty string.
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }
            // Return char and concat substring.
            return char.ToUpper(s[0]) + s.Substring(1);
        }




    }


    public static class Resources_
    {
        //protected readonly DataContext db = new DataContext();
        public static string Translate(int key)
        {
            string sentence = "";
             using(var db = new DataContext())
            {
                Debug.WriteLine(key);
                sentence = "Translated sentence: " + key;
                sentence = (from t in db.Traductions
                            where t.RefElement == key && t.RefLangue == 1
                            select t.Traduction1).FirstOrDefault();

                return sentence;
            }
        }
    }

}