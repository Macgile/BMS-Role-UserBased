﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Web.Mvc;
using SiteBMS.DAL;
using SiteBMS.DAL.Security;
using SiteBMS.Models;

namespace SiteBMS
{
    public class TypeList
    {
        public int Value { get; set; }
        public string Text { get; set; }
    }

    public static class Repositories
    {
        private static readonly int[] exceptEspeces = new[] { 11, 12, 13 };

        public static void ShowStateErrors(ModelStateDictionary modelState)
        {
            // Repositories.ShowStateErrors(ModelState);
            var errors = (from e in modelState
                          where e.Value.Errors.Count > 0
                          select e).ToList();

            foreach (var error in errors)
            {
                Debug.WriteLine("{0} : {1}", error.Key, error.Value.Errors.First().ErrorMessage);
            }
        }

        public static List<KeyValuePair<string, ModelState>> GetStateErrors(ModelStateDictionary modelState)
        {
            //var errors = Repositories.GetStateErrors(ModelState);
            var errors = (from e in modelState
                          where e.Value.Errors.Count > 0
                          select e).ToList();
            return errors;
        }

        // clause "IN" for linQ
        public static bool In<T>(this T source, params T[] list)
        {
            return list.Contains(source);
            /*

             // USING
             var states = _objdatasources.StateList().Where(s => s.In(countrycodes));

            // OR
             var states = tooManyStates.Where(s => s.In("x", "y", "z"));

            // alternative
            var selected = from u in users
                           where new[] { "Admin", "User", "Limited" }.Contains(u.User_Rights)
                           select u
             */
        }

        public static string GetEspece(DataContext db, int especeId, int languageId)
        {
            var espece = (from e in db.Especes
                          join t in db.Traductions on e.Nom_T equals t.RefElement
                          where t.RefLangue == 1 && e.ID == especeId
                          select t.Traduction1).FirstOrDefault();

            if (espece != null)
                return espece;

            return string.Empty;
        }

        /*
         ********************
         *
         * GET LIST ITEMS
         *
         * ******************
         */

        public static IEnumerable GetPaysList(DataContext db, int languageId)
        {
            var pays = (from l in db.Langues
                        join t in db.Traductions on l.Nom_T equals t.RefElement
                        where l.IsActive && t.RefLangue == languageId
                        select new
                        {
                            Value = t.RefElement,
                            Text = t.Traduction1
                        }).ToList();

            return pays;
        }

        public static IEnumerable GetLanguesList(DataContext db, int languageId)
        {
            var pays = (from l in db.Langues
                        join t in db.Traductions on l.Nom_T equals t.RefElement
                        where l.IsActive && t.RefLangue == languageId
                        select new
                        {
                            Value = t.RefElement,
                            Text = t.Traduction1
                        }).ToList();

            return pays;
        }

        // GetEspeces LIST
        public static List<TypeList> GetEspecesList(DataContext db, int languageId)
        {
            var especes = (from e in db.Especes
                           join t in db.Traductions on e.Nom_T equals t.RefElement
                           where t.RefLangue == languageId && !exceptEspeces.Contains(e.ID)
                           orderby e.ID
                           select new TypeList
                           {
                               Value = e.ID,
                               Text = t.Traduction1
                           }).ToList();

            //  var items = new SelectList(especes, "Value", "Text", selectedIndex);
            return especes;
        }

        // GetCombo LIST
        public static IEnumerable GetComboList(DataContext db, string comboName, int languageId)
        {
            var combo = (from c in db.Comboes
                         join ce in db.Combo_Element on c.ID equals ce.IDRefCombo
                         join tr in db.Traductions on ce.RefElement equals tr.RefElement
                         where c.NomCombo == comboName && tr.RefLangue == languageId
                         select new
                         {
                             Value = ce.RefElement,
                             Text = tr.Traduction1
                         }).ToList();

            //  var items = new SelectList(combo, "Value", "Text", selectedIndex);

            return combo;
        }

        // GetComboNumeric LIST
        public static IEnumerable GetComboNumericList(DataContext db, string comboName)
        {
            var comboNum = (from cn in db.Combo_Element_Numeric
                            join c in db.Comboes on cn.RefCombo equals c.ID
                            where c.NomCombo == comboName
                            select new
                            {
                                Value = cn.Valeur,
                                Text = cn.Libelle
                            }).ToList();

            //  var items = new SelectList(comboNum, "Value", "Text", selectedIndex);

            return comboNum;
        }

        // GetComboEspece LIST
        public static IEnumerable GetComboEspecesList(DataContext db, string comboName, int especeId, int languageId)
        {
            var especes = (from ce in db.Combo_Element
                           join c in db.Comboes on ce.IDRefCombo equals c.ID
                           join t in db.Traductions on ce.RefElement equals t.RefElement
                           where c.NomCombo == comboName
                           && ce.RefEspece == especeId
                           && t.RefLangue == languageId
                           select new
                           {
                               Value = ce.Value,
                               Text = t.Traduction1
                           }).ToList();

            //  var items = new SelectList(especes, "Value", "Text", selectedIndex);

            return especes;
        }

        // GetVariete LIST
        public static IEnumerable GetVarietesList(DataContext db, int especeId, int languageId)
        {
            var varietes = (from v in db.Varietes
                            join t in db.Traductions on v.Nom_T equals t.RefElement
                            where t.RefLangue == languageId && v.RefEspece == especeId
                            select new
                            {
                                Value = v.ID,
                                Text = t.Traduction1
                            }).ToList();

            //  var items = new SelectList(varietes, "Value", "Text", selectedIndex);

            return varietes;
        }

        // Fumure K-P-N LIST
        public static IEnumerable GetFumureKpnList(DataContext db)
        {
            var fumure = (from n in Enumerable.Range(1, 200)
                          select new
                          {
                              Value = n,
                              Text = n
                          }).ToList();

            // var items = new SelectList(fumure, "Value", "Text", selectedIndex);

            return fumure;
        }

        //LargeurRang LIST
        public static IEnumerable GetLargeurRangList(DataContext db)
        {
            var largeurRang = (from n in Enumerable.Range(1, 300)
                               select new
                               {
                                   Value = n,
                                   Text = n
                               }).ToList();

            // var items = new SelectList(fumure, "Value", "Text", selectedIndex);

            return largeurRang;
        }

        //HauteurCanopee LIST
        public static IEnumerable GetHauteurCanopeeList(DataContext db)
        {
            var hauteurCanopee = (from n in Enumerable.Range(1, 300)
                                  select new
                                  {
                                      Value = n,
                                      Text = n
                                  }).ToList();
            // var items = new SelectList(fumure, "Value", "Text", selectedIndex);
            return hauteurCanopee;
        }

        /*
         *
        ****************************
        * GET SELECTLISTITEM
        ****************************
        *
        */

        // GetEspeces
        public static IEnumerable<SelectListItem> GetEspeces(DataContext db, int languageId)
        {
            var query = (from e in db.Especes
                         join t in db.Traductions on e.Nom_T equals t.RefElement
                         where t.RefLangue == languageId && !exceptEspeces.Contains(e.ID)
                         select new SelectListItem
                         {
                             Value = e.ID.ToString(),
                             Text = t.Traduction1.ToString()
                         }).ToList();
            return query;
        }

        // GetCombo
        public static IEnumerable<SelectListItem> GetCombo(DataContext db, string comboName, int languageId, int selected = 0)
        {
            var combo = (from c in db.Comboes
                         join ce in db.Combo_Element on c.ID equals ce.IDRefCombo
                         join tr in db.Traductions on ce.RefElement equals tr.RefElement
                         where (c.NomCombo == comboName && tr.RefLangue == languageId && tr.Traduction1.Length > 0)
                         /*|| (c.NomCombo == comboName && tr.IsDefaut == true)*/
                         select new SelectListItem
                         {
                             Value = ce.RefElement.ToString(),
                             Text = tr.Traduction1,
                             Selected = (selected == ce.RefElement)
                         }).Distinct();
            return combo.ToList();
        }

        // GetComboNumeric
        public static IEnumerable<SelectListItem> GetComboNumeric(DataContext db, string comboName)
        {
            var comboNumt = new SelectList((from cn in db.Combo_Element_Numeric
                                            join c in db.Comboes on cn.RefCombo equals c.ID
                                            where c.NomCombo == comboName
                                            select new { ID = cn.Valeur, Libelle = cn.Libelle }
                                                ), "ID", "Libelle", 0);

            return comboNumt;
        }

        // GetComboEspece
        public static IEnumerable<SelectListItem> GetComboEspece(DataContext db, string comboName, int especeId, int languageId)
        {
            var cmbEspece = (from ce in db.Combo_Element
                             join c in db.Comboes on ce.IDRefCombo equals c.ID
                             join t in db.Traductions on ce.RefElement equals t.RefElement
                             where c.NomCombo == comboName
                             && ce.RefEspece == especeId
                             && t.RefLangue == languageId
                             select new SelectListItem
                             {
                                 Value = ce.RefElement.ToString(),
                                 Text = t.Traduction1.ToString()
                             }).ToList();
            return cmbEspece;
        }

        // proc_GetGreffe
        public static IEnumerable<SelectListItem> GetGreffe(DataContext db, int especeId, int languageId)
        {
            // porte Greffe
            var greffe = (from g in db.Greffes
                          join t in db.Traductions on g.Nom_T equals t.RefElement
                          where (t.RefLangue == languageId && g.RefEspece == especeId && t.Traduction1.Length > 0)
                          || (g.RefEspece == especeId && t.IsDefaut == true)
                          orderby t.Traduction1
                          select new SelectListItem
                          {
                              Value = g.ID.ToString(),
                              Text = t.Traduction1
                          }).Distinct();

            return greffe.ToList();
        }

        // Fumure K-P-N
        public static IEnumerable<SelectListItem> GetFumureKpn(DataContext db)
        {
            var fumure = (from n in Enumerable.Range(1, 200)
                          select new SelectListItem
                          {
                              Value = n.ToString(),
                              Text = n.ToString()
                          }).ToList();

            return fumure;
        }

        // Pourcentage Calcaire
        public static IEnumerable<SelectListItem> GetCalcaire()
        {
            var calcaire = (from n in Enumerable.Range(1, 70)
                            select new SelectListItem
                            {
                                Value = n.ToString(),
                                Text = n.ToString() + " %"
                            }).ToList();

            return calcaire;
        }

        // Degres VIN
        public static IEnumerable<SelectListItem> GetDegres()
        {
            var degres = (from n in Enumerable.Range(10, 35)
                          select new SelectListItem
                          {
                              Value = n.ToString(),
                              Text = n.ToString() + "°"
                          }).ToList();

            return degres;
        }

        //LargeurRang LIST
        public static IEnumerable<SelectListItem> GetLargeurRang()
        {
            var largeurRang = (from n in Enumerable.Range(1, 300)
                               select new SelectListItem
                               {
                                   Value = n.ToString(),
                                   Text = n.ToString()
                               }).ToList();

            // var items = new SelectList(fumure, "Value", "Text", selectedIndex);

            return largeurRang;
        }

        public static IEnumerable<SelectListItem> GetHauteurCanopee(DataContext db)
        {
            var hauteurCanopee = (from n in Enumerable.Range(1, 300)
                                  select new SelectListItem
                                  {
                                      Value = n.ToString(),
                                      Text = n.ToString()
                                  }).ToList();
            // var items = new SelectList(fumure, "Value", "Text", selectedIndex);
            return hauteurCanopee;
        }

        /// <summary>
        /// ID = id parelle
        /// </summary>
        /// <param name="db"></param>
        /// <param name="id"></param>
        /// <returns></returns>

        public static IEnumerable<SelectListItem> GetRecolteYears(DataContext db, int id)
        {
            var currentYear = DateTime.Now.Year;
            var listYears = new List<SelectListItem>();

            // How many years in the current plot and where it begins
            foreach (int y in Enumerable.Range(currentYear, 5))
            {
                var year = y.ToString();
                var count = db.Releves.Count(r => r.RefParcelle == id && r.IsInitial == false &&  r.Nom.StartsWith(year) && r.IsActive == true);

                listYears.Add(new SelectListItem
                {
                    Value = year,
                    Text = year,
                    Selected = (y == currentYear)
                });
            }

            return listYears;
        }

        public static IEnumerable<SelectListItem> GetRecolteNum(DataContext db, int id, int year)
        {
            var listNum = new List<SelectListItem>();

            var countCurrentYear = db.Releves.Count(r => r.RefParcelle == id && r.IsInitial == false && r.IsActive == true && r.Nom.StartsWith(year.ToString()));

            //find how many Num in current Year and where it begins
            foreach (int x in Enumerable.Range((countCurrentYear + 1), (10 - countCurrentYear)))
            {
                var num = x.ToString();

                listNum.Add(new SelectListItem
                {
                    Value = num,
                    Text = num,
                    Selected = (x == countCurrentYear + 1)
                });
            }

            return listNum;
        }

        public static IEnumerable<SelectListItem> GetRecolte(DataContext db, int id)
        {
            var currentYear = DateTime.Now.Year;

            var listRecolte = new List<SelectListItem>();

            foreach (int y in Enumerable.Range(currentYear, 5))
            {
                var year = y.ToString();
                var count = db.Releves.Count(r => r.RefParcelle == id && r.IsInitial == false && r.Nom.StartsWith(year));

                //  SqlMethods.Like(r.Nom, "%" + year.ToString() + "%")

                if (count > 0)
                {
                    // the number of recolt for current year is 2 maximum
                    if (count > 1) continue;
                    listRecolte.Add(new SelectListItem
                    {
                        Value = year.ToString() + "-2",
                        Text = year.ToString() + "-2"
                    });
                }
                else
                {
                    listRecolte.Add(new SelectListItem
                    {
                        Value = year.ToString() + "-1",
                        Text = year.ToString() + "-1"
                    });
                    listRecolte.Add(new SelectListItem
                    {
                        Value = year.ToString() + "-2",
                        Text = year.ToString() + "-2"
                    });
                }
            }
            return listRecolte;
        }

        // GetVariete
        public static IEnumerable<SelectListItem> GetVariete(DataContext db, int especeId, int languageId)
        {
            var variete = (from v in db.Varietes
                           join t in db.Traductions on v.Nom_T equals t.RefElement
                           where (t.RefLangue == languageId && v.RefEspece == especeId && t.Traduction1.Length > 0)
                           || (v.RefEspece == especeId && t.IsDefaut == true)
                           select new SelectListItem
                           {
                               Value = v.ID.ToString(),
                               Text = t.Traduction1.ToString()
                           }).Distinct();
            return variete.ToList();
        }

        public static IEnumerable<SelectListItem> GetTechList(DataContext db, int id, int selected = 0)
        {
            var techList = (from t in (db.Database.SqlQuery<ClientList>("dbo.GetTechList @SuperieurHie = {0}", id).ToList())
                            select new SelectListItem
                            {
                                Text = t.Nom + " " + t.Prenom,
                                Value = t.ID.ToString(),
                                Selected = (selected == t.ID)
                            });

            return techList;
        }

        // GetTranslation
        public static string GetTranslation(DataContext db, int refElement, int languageId)
        {
            var translation =
                db.Traductions.Where(t => t.RefLangue == languageId && t.RefElement == refElement)
                    .Select(x => x.Traduction1)
                    .FirstOrDefault();
            return translation;
        }

        // GetUtilisateur
        public static string GetUtilisateur(DataContext db, int? userId)
        {
            if (userId != null)
                return (from u in db.Utilisateurs
                        where u.ID == userId
                        select u.Nom + " " + u.Prenom).FirstOrDefault();
            return string.Empty;
        }

        // get superieur hierarchique or Pdg if nothing
        public static int GetSuperieur(DataContext db, CustomPrincipal User, int id = 0)
        {
            var idTech = 0;
            if (User.IsAuthorized(RoleType.Distributeur))
            {
                idTech = (from t in db.Techniciens
                          where t.ID == id
                          select t.RefRespZone).ToList().FirstOrDefault();
            }
            else
            {
                // pdg, responsable, technicien
                idTech = User.RefId;
            }

            // if id tech = 0 set superieur to 1 (pdg)
            idTech = (idTech > 0) ? idTech : (int)RoleType.Pdg;

            return idTech;
        }

        public static int ConvertLangueToPays(DataContext db, int languageId)
        {
            /*
            SELECT Traduction.*,
                   Langue.*
            FROM Traduction
                 INNER JOIN Langue ON Traduction.RefElement = Langue.RefElement
                                      AND Traduction.RefLangue = Langue.ID
            WHERE Traduction.RefLangue = 4;
            */

            var country = (from t in db.Traductions
                           join l in db.Langues on t.RefElement equals l.RefElement
                           where t.RefLangue == languageId && l.ID == languageId
                           select t.RefElement);

            // BAD QUERY
            //var country = (from c in db.Comboes
            //          join ce in db.Combo_Element on c.ID equals ce.IDRefCombo
            //          join t in db.Traductions on ce.RefElement equals t.RefElement
            //          where c.ID == 13 && t.RefLangue == languageId
            //          select ce.RefElement);

            var element = country.FirstOrDefault();
            // traduction not exist in database, 82 : france
            var id = (element == 0) ? 82 : element;
            return id;
        }
    }
}