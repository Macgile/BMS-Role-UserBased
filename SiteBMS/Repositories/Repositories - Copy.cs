﻿using SiteBMS.DAL;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Mvc;

namespace SiteBMS
{
    public static class Repositories
    {

        // clause "IN" for linQ
        public static bool In<T>(this T source, params T[] list)
        {
            return list.Contains(source);
            /*

             // USING
             var states = _objdatasources.StateList().Where(s => s.In(countrycodes));

            // OR
             var states = tooManyStates.Where(s => s.In("x", "y", "z"));

            // alternative
            var selected = from u in users
                           where new[] { "Admin", "User", "Limited" }.Contains(u.User_Rights)
                           select u
             */
        }

        public static string GetEspece(DataContext db, int especeID, int languageID)
        {
            //ViewBag.Espece = (from e in db.Especes
            //                  join t in db.Traductions on e.Nom_T equals t.RefElement
            //                  where t.RefLangue == 1 && e.ID == viewModel.Parcelle.RefComboEspece
            //                  select t.Traduction1).FirstOrDefault();

            var espece = (from e in db.Especes
                          join t in db.Traductions on e.Nom_T equals t.RefElement
                          where t.RefLangue == 1 && e.ID == especeID
                          select t.Traduction1).FirstOrDefault();

            if (espece != null)
                return espece;

            return string.Empty;
        }

        // GetEspeces
        public static IEnumerable<SelectListItem> GetEspeces(DataContext db, int? selectedID, int languageID)
        {
            if (selectedID == null)
                selectedID = 0;

            var query = (from e in db.Especes
                         join t in db.Traductions on e.Nom_T equals t.RefElement
                         where t.RefLangue == languageID
                         select new SelectListItem
                         {
                             Value = e.ID.ToString(),
                             Text = t.Traduction1.ToString(),
                             Selected = (e.ID == selectedID) ? true : false
                         }).ToList();
            return query;
        }

        // GetCombo
        public static IEnumerable<SelectListItem> GetCombo(DataContext db, string comboName, string selectedID, int languageID)
        {
            var combo = (from c in db.Comboes
                         join ce in db.Combo_Element on c.ID equals ce.IDRefCombo
                         join tr in db.Traductions on ce.RefElement equals tr.RefElement
                         where c.NomCombo == comboName && tr.RefLangue == languageID
                         select new SelectListItem
                         {
                             Value = ce.RefElement.ToString(),
                             Text = tr.Traduction1,
                             Selected = (ce.RefElement.ToString() == selectedID) ? true : false
                         }).ToList();
            return combo;
        }

        // GetComboNumeric
        public static IEnumerable<SelectListItem> GetComboNumeric(DataContext db, string comboName, string selectedID)
        {
            //new SelectList(db.Greffes, "ID", "ID", releve.RefComboGreffe);
            /*
            var comboNum = (from cn in db.Combo_Element_Numeric
                            join c in db.Comboes on cn.RefCombo equals c.ID
                            where c.NomCombo == comboName
                            select new SelectListItem
                            {
                                Value = cn.Valeur.ToString(),
                                Text = cn.Libelle.ToString(),
                                Selected = (cn.Valeur.ToString() == selectedID) ? true : false
                            }).ToList();
            */
            var comboNumt = new SelectList((from cn in db.Combo_Element_Numeric
                                            join c in db.Comboes on cn.RefCombo equals c.ID
                                            where c.NomCombo == comboName
                                            select new { ID = cn.Valeur, Libelle = cn.Libelle }
                                                ), "ID", "Libelle", selectedID);

            return comboNumt;
            //return comboNum;
        }

        // GetComboEspece
        public static IEnumerable<SelectListItem> GetComboEspece(DataContext db, string comboName, int? selectedID, int especeID, int languageID)
        {
            if (selectedID == null)
                selectedID = 0;

            var cmbEspece = (from ce in db.Combo_Element
                             join c in db.Comboes on ce.IDRefCombo equals c.ID
                             join t in db.Traductions on ce.RefElement equals t.RefElement
                             where c.NomCombo == comboName
                             && ce.RefEspece == especeID
                             && t.RefLangue == languageID
                             select new SelectListItem
                             {
                                 Value = ce.RefElement.ToString(),
                                 Text = t.Traduction1.ToString(),
                                 Selected = (ce.RefElement == selectedID) ? true : false
                             }).ToList();

            /*
            SELECT CE.RefElement,
                   --CE.Value,
                   --CE.RefEspece,
                   T.Traduction
            FROM   Combo_Element AS CE
                   INNER JOIN Combo AS C ON CE.IDRefCombo = C.ID
                   INNER JOIN Traduction T ON CE.RefElement = T.RefElement
            WHERE  C.NomCombo = 'cmbTypeTaille'
                   AND CE.RefEspece = 7
                   AND T.RefLangue = 1
            ORDER BY CE.Ordre DESC;
            */

            return cmbEspece;
        }

        // proc_GetGreffe
        public static IEnumerable<SelectListItem> GetGreffe(DataContext db, int selectedID, int especeID, int languageID)
        {
            // porte Greffe
            var greffe = (from g in db.Greffes
                          join t in db.Traductions on g.Nom_T equals t.RefElement
                          where t.RefLangue == languageID
                          && g.RefEspece == especeID
                          orderby t.Traduction1
                          select new SelectListItem
                          {
                              Value = g.ID.ToString(),
                              Text = t.Traduction1,
                              Selected = (g.ID == selectedID) ? true : false
                          }).ToList();

            // OrderBy(t => t.Text)

            return greffe;
        }

        /*
         // GET-GREFFE SELECTED GREFFE ITEM
                SELECT G.ID,
                       G.VigGref,
                       G.IPC,
	                  T.Traduction
                FROM   Greffe G INNER JOIN Traduction T ON G.Nom_T = T.RefElement
                WHERE  G.RefEspece = 7 -- Parcelle.refComboEspece
                       AND G.IsActive = 1
	                  AND G.ID = 2 -- Releve.RefcomboGreffe
	                  AND T.RefLangue = 1
                ORDER BY T.Traduction;
        */

        // Fumure K-P-N
        public static IEnumerable<SelectListItem> getFumureKPN(DataContext db, int? selectedID)
        {
            if (selectedID == null)
                selectedID = 0;

            var fumure = (from n in Enumerable.Range(1, 200)
                          select new SelectListItem
                          {
                              Value = n.ToString(),
                              Text = n.ToString(),
                              Selected = (n == selectedID) ? true : false
                          }).ToList();

            return fumure;
        }

        // Fumure K-P-N
        public static IEnumerable<SelectListItem> GetVariete(DataContext db, int especeId, string selectedID, int languageID)
        {
            var variete = (from v in db.Varietes
                           join t in db.Traductions on v.Nom_T equals t.RefElement
                           where t.RefLangue == languageID && v.RefEspece == especeId
                           select new SelectListItem
                           {
                               Value = v.ID.ToString(),
                               Text = t.Traduction1.ToString(),
                               Selected = (v.ID.ToString() == selectedID) ? true : false
                           }).ToList();
            return variete;
        }

        public static string GetUtilisateur(DataContext db, int? userID)
        {
            if (userID != null)
                return (from u in db.Utilisateurs
                        where u.ID == userID
                        select u.Nom + " " + u.Prenom).FirstOrDefault();
            return string.Empty;
        }
    }

    /*
    DECLARE @SuperieurHie int;
    SET    @SuperieurHie = 1;

    SELECT T.ID,
           U.Nom+' '+U.Prenom AS Nom,
          U.IsActive
    FROM   Technicien AS T
           INNER JOIN Utilisateur AS U ON (T.ID = U.RefId AND U.IsActive = 1)
           INNER JOIN Technicien AS T1 ON T.RefRespZone = T1.ID
    WHERE  U.RefRole = 3 AND T.RefRespZone = @SuperieurHie
           OR U.RefRole = 3  AND T1.RefRespZone = @SuperieurHie
           OR U.RefRole = 3  AND U.RefId = @SuperieurHie
           OR U.RefRole = 3  AND @SuperieurHie = 1
    ORDER BY Nom;
    */
}