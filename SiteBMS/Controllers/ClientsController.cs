﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using PagedList;
using SiteBMS.Models;
using SiteBMS.ViewModels;

namespace SiteBMS.Controllers
{
    [Authorize]
    public class ClientsController : BaseController
    {
        // GET: Clients
        public ActionResult Index(string sortOrder, string currentFilter, int? page, string searchString, int? refLangue, int? refDistributeur)
        {
            DateTime GeneralStartTime = DateTime.Now;
            ViewBag.SortOrder = string.IsNullOrEmpty(sortOrder) ? "SortDesc" : ""; // name_desc
            ViewBag.SortingClass = "SortDesc";
            ViewBag.PaysSortParm = sortOrder == "Pays" ? "PaysDesc" : "Pays";
            ViewBag.CodePostalSortParm = sortOrder == "CodePostal" ? "CodePostalDesc" : "CodePostal";
            ViewBag.DateCreated = sortOrder == "DateCreated" ? "DateCreatedDesc" : "DateCreated";

            // dropdownlist filter
            ViewBag.RefLangue = refLangue;
            ViewBag.RefDistributeur = refDistributeur;

            // DROITS - ROLES
            // si l'utilisateur est pdg/responsable/technicien, id_technicien est le RefId sinon 0
            var idTechnicien = User.IsAuthorized(new[] { RoleType.Pdg, RoleType.ResponsableZone, RoleType.Technicien })
                ? User.RefId
                : 0;

            // si l'utilisateur est un distributeur, id_distributeur est le refid du compte utilisateur
            var idDistributeur = User.IsAuthorized(RoleType.Distributeur) ? User.IdDistributeur : 0;

            // filtre par distributeur
            if (refDistributeur != null && refDistributeur > 0)
            {
                idDistributeur = (int)refDistributeur;
            }

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;
            ViewBag.CurrentSort = sortOrder;

            // rercheche de tous les clients
            var getClient =
                db.Database.SqlQuery<ClientList>(
                    "dbo.sp_GetClients @Nom, @CodePostal, @IDTechnicien, @IDDistributeur, @Role",
                    new SqlParameter("@Nom", ""), /* champ de recherche: nom pas utilisé ici */
                    new SqlParameter("@CodePostal", ""), /* champ de recherche code postal : nom pas utilisé ici */
                    new SqlParameter("@IDTechnicien", idTechnicien), /* reference du technicien */
                    new SqlParameter("@IDDistributeur", idDistributeur), /* champ de recherche distributeur */
                    new SqlParameter("@Role", User.RefRole)).ToList();

            // get all customer in getClient List
            var clients = (from c in db.Clients.Where(c=>c.IsActive == true).ToList()
                           let firstOrDefault = c.Parcelles.FirstOrDefault()
                           join cl in getClient on c.ID equals cl.ID
                           // Array of Especes not available in this application, see BaseController Class
                           where firstOrDefault != null && !especes.Contains(firstOrDefault.RefComboEspece)
                           && firstOrDefault.IsActive == true
                           || c.Parcelles.Count == 0
                           orderby c.Nom
                           select c);//.ToList();

            if (refLangue != null)
            {
                clients = clients.Where(c => c.RefComboPays == refLangue).ToList();
            }

            if (!string.IsNullOrEmpty(searchString))
            {
                clients = clients.Where(s => s.Nom.StartsWith(searchString, StringComparison.InvariantCultureIgnoreCase)).ToList();
            }

            // LIST OF EXISTING COUNTRIES
            // select contry in refLangue or nothing
            var pays = Repositories.GetCombo(db, "cmbPays", User.LanguageId, refLangue ?? 0).OrderBy(p => p.Text);
            ViewBag.DDLPays = pays;

            // update country name in clients list, The property 'Pays' isn't Mapped by EF Indentities
            clients.ToList()
                .ForEach(
                    c => c.Pays = pays.Where(t => t.Value == c.RefComboPays.ToString())
                        .Select(t => t.Text).FirstOrDefault());

            // Sort by sortOrder parameter if set
            switch (sortOrder)
            {
                case "SortDesc":
                    clients = clients.OrderByDescending(s => s.Nom).ToList();
                    break;

                case "PaysDesc":
                    clients = clients.OrderByDescending(s => s.Pays).ToList();
                    break;

                case "Pays":
                    clients = clients.OrderBy(s => s.Pays).ToList();
                    break;

                case "CodePostalDesc":
                    clients = clients.OrderByDescending(s => s.CodePostal).ToList();
                    break;

                case "CodePostal":
                    clients = clients.OrderBy(s => s.CodePostal).ToList();
                    break;

                case "DateCreatedDesc":
                    clients = clients.OrderByDescending(s => s.DatCreat).ToList();
                    break;

                case "DateCreated":
                    clients = clients.OrderBy(s => s.DatCreat).ToList();
                    break;
            }

            var clientVm = MapperConfig.mapper.Map<List<Client>, List<ClientViewModel>>(clients.ToList());

            DateTime startTime = DateTime.Now;

            var listId = (from c in clients.AsEnumerable() select c.ID).ToList();

            //(from c in clients.ToList() select c.ID).Contains(B.Releve.Parcelle.RefClient)
            // get client has cumul for this year
            var listClient = (from B in db.Bilans.ToList()
                              where
                              listId.Contains(B.Releve.Parcelle.RefClient)
                                && B.Qte > 0 &&
                                B.Releve.IsActive == true &&
                                B.Releve.IsInitial == false &&
                                B.Releve.Parcelle.IsActive == true &&
                                B.Releve.Parcelle.Client.IsActive == true &&
                                B.Releve.Nom.StartsWith(currentYear.ToString())
                              group B.Releve.Parcelle by new
                              {
                                  B.Releve.Parcelle.RefClient
                              } into g
                              where g.Count() > 0
                              select g.Key.RefClient).ToList();

            // Debug.WriteLine(listClient.Count);

            // The customer has cumul for the current year ?
            // update hasCumul in clients list, The property 'hasCumul' isn't Mapped by EF Indentities
            clientVm.ToList().ForEach(c => { c.hasCumul = listClient.Contains(c.ID); });

            DateTime endTime = DateTime.Now;

            TimeSpan span = endTime.Subtract(startTime);
            //Debug.WriteLine("Time Difference (seconds): " + span.Seconds);
            //Debug.WriteLine("Time Difference (minutes): " + span.Minutes);

            ViewBag.Seconds = "Time Difference (seconds): " + span.Seconds;
            ViewBag.Minutes = "Time Difference (minutes): " + span.Minutes;

            //Debug.WriteLine("Time Difference (hours): " + span.Hours);
            //Debug.WriteLine("Time Difference (days): " + span.Days);

            clientVm.ToList()
                .ForEach(c =>
                {
                    c.UserCount = db.Utilisateurs.Count(u => u.RefId == c.ID && u.IsActive && u.RefRole == (int)RoleType.Client);
                });

            var distributeurs = clients
                .SelectMany(client => client.Distributeurs)
                .Select(d => new { Value = d.ID, Text = d.Nom.ToUpper() }).Distinct().OrderBy(d => d.Text).ToList();

            ViewBag.DDLDistributeur = new SelectList(distributeurs, "Value", "Text", refDistributeur ?? 0);

            var pageNumber = (page ?? 1);

            DateTime GeneralEndTime = DateTime.Now;

            TimeSpan gspan = GeneralEndTime.Subtract(GeneralStartTime);
            ViewBag.GSeconds = "Time Difference (seconds): " + gspan.Seconds;
            ViewBag.GMinutes = "Time Difference (minutes): " + gspan.Minutes;

            return View(clientVm.ToPagedList(pageNumber, pageSize));
        }

        // GET: Clients/Details/5
        public ActionResult Details(int? id, int? page)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var client = db.Clients.Find(id);

            if (client == null)
            {
                return HttpNotFound();
            }
            var clientVm = MapperConfig.mapper.Map<Client, ClientViewModel>(client);

            clientVm.ListDistributeurs = client.Distributeurs; //db.Distributeurs.ToList();
            InitializeViewBag(clientVm);
            clientVm.UtilisateursVm = GetUtilisateursVm(id);
            ViewBag.FullName = clientVm.FullName;
            ViewBag.Page = (page ?? 1);
            return View(clientVm);
        }

        // GET: Clients/Create
        public ActionResult Create()
        {
            // A client can not create a client
            var clientVm = new ClientViewModel();

            // uniquement les distributeur dont il est le technicien
            // ou le resonsable du technicien
            // tous si pdg
            GetUserDistributeur(clientVm);
            //clientVm.RefTechBMS = 0;
            InitializeViewBag(clientVm);
            return View(clientVm);
        }

        // POST: Clients/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ClientViewModel clientVm)

        {
            if (ModelState.IsValid)
            {
                var client = MapperConfig.mapper.Map<ClientViewModel, Client>(clientVm);

                client.Nom = Utils.UcFirst(client.Nom);
                client.Prenom = Utils.UcFirst(client.Prenom);

                client.DatModif = DateTime.Now;
                client.DatCreat = DateTime.Now;
                client.IDUserCreat = User.UserId;
                client.IDUserModif = User.UserId;

                var distributeur = db.Distributeurs.Find(clientVm.Distributeur_ID);
                if (distributeur != null)
                {
                    client.Distributeurs.Add(distributeur);
                    client.RefTechBMS = distributeur.RefTechBMS;
                }

                db.Clients.Add(client);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            //clientVm.IsActive = true;
            //clientVm.Distributeurs = db.Distributeurs.ToList();
            GetUserDistributeur(clientVm);
            InitializeViewBag(clientVm);
            return View(clientVm);
        }

        // GET: Clients/Edit/5
        public ActionResult Edit(int? id, int? page)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = db.Clients.Find(id);

            if (client == null)
            {
                return HttpNotFound();
            }

            var clientVm = MapperConfig.mapper.Map<Client, ClientViewModel>(client);

            clientVm.ListDistributeurs = db.Distributeurs.ToList()
                .Where(d => !client.Distributeurs.Contains(d))
                .OrderBy(d => d.Nom)
                .ToList();

            // if client has distributeur or 0 if not
            clientVm.Distributeur_ID = (client.Distributeurs.Count > 0) ? client.Distributeurs.FirstOrDefault().ID : 0;

            InitializeViewBag(clientVm);

            clientVm.UtilisateursVm = GetUtilisateursVm(id);
            ViewBag.Page = page;
            ViewBag.FullName = clientVm.FullName;

            return View(clientVm);
        }

        // POST: Clients/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ClientViewModel clientVm, int? page)
        {
            if (ModelState.IsValid)
            {
                var client = MapperConfig.mapper.Map<ClientViewModel, Client>(clientVm);
                db.Entry(client).State = EntityState.Modified;
                client.Nom = Utils.UcFirst(client.Nom);
                client.Prenom = Utils.UcFirst(client.Prenom);
                client.DatModif = DateTime.Now;
                client.IDUserModif = User.UserId;

                var distributeur = db.Distributeurs.Find(clientVm.Distributeur_ID);
                if (distributeur != null)
                {
                    // first remove all linked distributor
                    var clientDistributeur = (from c in db.Clients.Include("Distributeurs")
                                              where c.ID == client.ID
                                              select c).FirstOrDefault();
                    var deleteDistributeur = clientDistributeur.Distributeurs.ToList();
                    deleteDistributeur.ForEach(d => clientDistributeur.Distributeurs.Remove(d));

                    client.Distributeurs.Add(distributeur);
                    client.RefTechBMS = distributeur.RefTechBMS;
                }

                db.SaveChanges();
                return RedirectToAction("Index", new { page = (page ?? 1) });
            }

            Repositories.ShowStateErrors(ModelState);

            clientVm.Distributeurs = db.Clients.Find(clientVm.ID).Distributeurs;
            clientVm.ListDistributeurs = db.Distributeurs.ToList()
                .Where(x => !clientVm.Distributeurs.Contains(x))
                .ToList();

            InitializeViewBag(clientVm);

            clientVm.UtilisateursVm = GetUtilisateursVm(clientVm.ID);
            ViewBag.Page = page;
            ViewBag.FullName = clientVm.FullName;

            return View(clientVm);
        }

        private void InitializeViewBag(ClientViewModel client)
        {
            ViewBag.DDLCommercialisation = Repositories.GetCombo(db, "cmbTypeCommercialisation", User.LanguageId);
            ViewBag.DDLPays = Repositories.GetCombo(db, "cmbPays", User.LanguageId).OrderBy(d => d.Text);

            // set reference id for selected value
            var SuperieurHie = Repositories.GetSuperieur(db, User, client?.RefTechBMS ?? 0);
            ViewBag.DDLTechList = Repositories.GetTechList(db, SuperieurHie, client?.RefTechBMS ?? 0);

            var techName = (from t in db.Techniciens
                            join u in db.Utilisateurs on t.ID equals u.RefId
                            where t.ID == client.RefTechBMS
                            select new { Name = u.Prenom + " " + u.Nom }).ToList();

            ViewBag.TechBMS = techName.Count > 0 ? techName.FirstOrDefault().Name : "Inconnu";

            var distributeurs = db.Distributeurs.Select(d => new { Value = d.ID.ToString(), Text = d.Nom }).ToList();
            var selectedDistibutor = client.Distributeurs.Count > 0 ? client.Distributeurs.FirstOrDefault().ID : 0;

            ViewBag.DDLDistributeurs = new SelectList(distributeurs, "Value", "Text", selectedDistibutor);

            //var selectedValue = User.IsAuthorized(new[] { RoleType.Pdg, RoleType.ResponsableZone }) ? User.RefId : User.IdTech;
            //ViewBag.DDLTechList = Repositories.GetTechList(db, User.RefId, selectedValue);
        }

        private void GetUserDistributeur(ClientViewModel clientVm)
        {
            // uniquement les distributeur dont il est le technicien
            // ou resonsable du technicien du distributeur
            // ou tous si pdg

            // selectionner les distributeurs dont il est le technicien
            // OU
            // selectionner les distributeurs de tous les techniciens dont il est le responsable
            // OU
            // selectionner les distributeurs de tous les techniciens et responsable dont il est le pdg

            var distributeurs = new List<Distributeur>();

            if (User.IsAuthorized(new[] { RoleType.Pdg, RoleType.ResponsableZone, RoleType.Technicien }))
            {
                if (User.IsRole(RoleType.Pdg))
                {
                    distributeurs = db.Distributeurs.Where(d => d.IsActive).Select(d => d).ToList();
                }
                else
                {
                    distributeurs = (from d in db.Distributeurs.Where(d => d.IsActive)
                                     join tech in db.Techniciens on d.RefTechBMS equals tech.ID
                                     join responsable in db.Techniciens on tech.RefRespZone equals responsable.ID
                                     where d.RefTechBMS == User.RefId || tech.ID == User.RefId || tech.RefRespZone == User.RefId
                                     orderby d.Nom
                                     select d).ToList(); ;
                }
            }
            else if (User.IsRole(RoleType.Distributeur))
            {
                distributeurs = db.Distributeurs.Where(d => d.ID == User.IdDistributeur).Select(d => d).ToList(); ;
            }

            clientVm.Distributeurs = distributeurs;
        }

        // Utilisateur(s) du "Bilan Web" associé à ce distributeur
        private List<UtilisateurViewModel> GetUtilisateursVm(int? idClient)
        {
            var list = (from u in db.Utilisateurs
                        join l in db.Langues on u.RefLangue equals l.ID
                        join t in db.Traductions on l.Nom_T equals t.RefElement
                        where l.IsActive && u.RefId == idClient && u.RefRole == (int)RoleType.Client && u.IsActive
                        && t.RefLangue == User.LanguageId
                        select new
                        {
                            utilisateur = u,
                            LangueName = t.Traduction1
                        });
            list.ToList().ForEach(u => u.utilisateur.LangueName = u.LangueName);
            var utilisateurs = list.Select(u => u.utilisateur).ToList();
            var utilisateursVm = MapperConfig.mapper.Map<List<Utilisateur>, List<UtilisateurViewModel>>(utilisateurs);
            return utilisateursVm;
        }

        // POST: Add Distributor
        [HttpPost]
        public JsonResult AddDistributor(int? id, int? selectedId, string name)
        {
            if (id != null && selectedId != null)
            {
                var distributeur = db.Distributeurs.Find(selectedId);
                db.Clients.Find(id).Distributeurs.Add(distributeur);
                db.SaveChanges();
                return Json("Response from Create : " + name);
            }
            return Json("Response Error");
        }

        // Archive client from Details view
        [HttpPost]
        public JsonResult Archiver(int? id)
        {
            if (id != null)
            {
                var client = db.Clients.Find(id);
                client.IsActive = false;
                db.Entry(client).State = EntityState.Modified;

                if (db.SaveChanges() > 0)
                {
                    // desactiver utilisateur et clients
                    db.Database.ExecuteSqlCommand(@"UPDATE dbo.Utilisateur SET IsActive = 0 WHERE RefId = @IdClient AND RefRole = @RefRole",
                        new SqlParameter("@IdClient", id),
                        new SqlParameter("@RefRole", (int)RoleType.Client));

                    // supprimer les associations clients/distributeur
                    db.Database.ExecuteSqlCommand(@"DELETE FROM dbo.Distributeur_Client WHERE RefClient = @IdClient",
                        new SqlParameter("@IdClient", id));
                    return Json("Success");
                }
            }
            return Json("Response Error");
        }

        // GET: Clients/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = db.Clients.Find(id);
            if (client == null)
            {
                return HttpNotFound();
            }
            return View(client);
        }

        // POST: Clients/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var client = db.Clients.Find(id);
            db.Clients.Remove(client);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}