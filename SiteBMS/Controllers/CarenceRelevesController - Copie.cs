﻿using SiteBMS.DAL;
using SiteBMS.Models;
using SiteBMS.ViewModels;
using System;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace SiteBMS.Controllers
{
    public class CarenceRelevesController : BaseController
    {
        private DataContext db = new DataContext();

        private int languageID = 1;

        // GET: CarenceReleves
        public ActionResult Index()
        {
            var carenceReleves = db.CarenceReleves
                .Include(c => c.CarenceNorme)
                .Include(c => c.Releve);
            return View(carenceReleves.ToList());
        }

        // GET: CarenceReleves List for  releve
        public ActionResult List(int? id, int? page)
        {
            var carenceReleves = (from cr in db.CarenceReleves
                                  join cn in db.CarenceNormes on cr.RefCarenceNorme equals cn.ID
                                  //join r in db.Releves on cr.re
                                  where cr.RefReleve == id
                                  group new { cr, cn } by new { cn.RefType } into g
                                  let first = g.FirstOrDefault()
                                  let norme = first.cn
                                  let carence = first.cr
                                  let parcelle = first.cr.Releve.RefParcelle
                                  let releve = first.cr.Releve
                                  //let count = g.Count()
                                  select new CarenceTypeViewModel
                                  {
                                      Name = (norme.RefType == 1) ? "Feuilles" : (norme.RefType == 2) ? "Fruits" : "Rameaux",
                                      RefType = norme.RefType,
                                      RefReleve = carence.RefReleve,
                                      CarenceCount = g.Count(),
                                      RefParcelle = parcelle,
                                      Releve = releve
                                  });
            ViewBag.Page = (page ?? 1);
            return View(carenceReleves.ToList());
        }

        // GET: CarenceReleves/Details/5
        public ActionResult Details(int? id, int? page)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CarenceReleve carenceReleve = db.CarenceReleves.Find(id);

            if (carenceReleve == null)
            {
                return HttpNotFound();
            }

            ViewBag.Page = (page ?? 1);
            return View(carenceReleve);
        }

        // GET: CarenceReleves/Create
        public ActionResult Create(int? id, int? page)
        {
            ViewBag.RefCarenceNorme = new SelectList(db.CarenceNormes, "ID", "ID");
            ViewBag.RefReleve = new SelectList(db.Releves, "ID", "Nom");

            ViewBag.Page = (page ?? 1);
            return View();
        }

        // POST: CarenceReleves/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,RefCarenceNorme,RefReleve,RefComboCarence,ValeurAnalyseCarence")] CarenceReleve carenceReleve, int? page)
        {
            if (ModelState.IsValid)
            {
                db.CarenceReleves.Add(carenceReleve);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.RefCarenceNorme = new SelectList(db.CarenceNormes, "ID", "ID", carenceReleve.RefCarenceNorme);
            ViewBag.RefReleve = new SelectList(db.Releves, "ID", "Nom", carenceReleve.RefReleve);
            return View(carenceReleve);
        }

        // GET: CarenceReleves/Edit/5 (id == id releve, typeID == typeCarence)
        public ActionResult Edit(int? id, int? typeID, int? page)
        {
            // http://localhost:3839/CarenceReleves/Edit/9027

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            // Feuilles(1) - Fruits(2) - Rameaux(3)
            // 1 : by default
            int typeCarence = (typeID ?? 1);

            // List of all carence of type : typeCarence
            var carenceList = (from cr in db.CarenceReleves
                               join cn in db.CarenceNormes on cr.RefCarenceNorme equals cn.ID
                               where cn.RefType == typeCarence
                               && cn.RefEspece == cr.Releve.Parcelle.RefComboEspece
                               && cr.RefReleve == id
                               select cr).ToList<CarenceReleve>();

            if (carenceList == null)
            {
                return HttpNotFound();
            }

            // first in the list
            var carenceReleve = carenceList.FirstOrDefault();

            var carenceValues = new CarenceValues();
            carenceValues.TypeCarence = typeCarence;

            // added to simplify
            var carenceValuesVM = new CarenceValuesViewModel();
            carenceValuesVM.CarenceReleve = carenceReleve;

            foreach (var c in carenceList)
            {
                carenceValues.PropertySet(carenceValues, "tbx_" + c.CarenceNorme.RefCarenceElement, c.ValeurAnalyseCarence.ToString());
                carenceValues.PropertySet(carenceValues, "ddl_" + c.CarenceNorme.RefCarenceElement, c.RefComboCarence);

                // added to simplify
                carenceValuesVM.PropertySet(carenceValuesVM, "tbx_" + c.CarenceNorme.RefCarenceElement, c.ValeurAnalyseCarence.ToString());
                carenceValuesVM.PropertySet(carenceValuesVM, "ddl_" + c.CarenceNorme.RefCarenceElement, c.RefComboCarence);
            }

            // mapping Model=>ViewModel
            var carenceVM = MapperConfig.mapper.Map<CarenceReleve, CarenceViewModel>(carenceReleve);

            // pas de mapping ici si utilisation de CarenceValuesViewModel
            // 

            carenceVM.CarenceValue = carenceValues;
            carenceVM.EspeceID = carenceReleve.Releve.Parcelle.RefComboEspece;

            carenceValuesVM.EspeceID = carenceReleve.Releve.Parcelle.RefComboEspece;



            ViewBag.Page = (page ?? 1);

            InitViewBag(carenceVM);

           // return View(carenceVM);

            return View(carenceValuesVM);

        }

        // POST: CarenceReleves/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CarenceViewModel carenceReleveVM, int? page)
        {
            page = (page ?? 1);

            // get all value in CarenceValue instance
            var vals = carenceReleveVM.CarenceValue.GetValues(carenceReleveVM.CarenceValue);

            carenceReleveVM.Releve = db.Releves.Find(carenceReleveVM.RefReleve);
            carenceReleveVM.CarenceNorme = db.CarenceNormes.Find(carenceReleveVM.RefCarenceNorme);

            // create Carence instances in List
            var values = (from v in vals
                          join re in db.CarenceNormes on v.RefNorme equals re.RefCarenceElement
                          where re.RefType == carenceReleveVM.CarenceValue.TypeCarence
                          && re.RefEspece == carenceReleveVM.EspeceID
                          select new Carence
                          {
                              ValeursAnalyse = v.ValeursAnalyse,
                              TypeChoix = v.TypeChoix,
                              RefNorme = re.ID
                          });

            var carenceReleve = MapperConfig.mapper.Map<CarenceViewModel, CarenceReleve>(carenceReleveVM);

            if (!ModelState.IsValid)
            {
                // must provide type carence 1/2/3
                // list of all carence for RefType carence (1,2 or 3)
                var carenceToDelete = db.CarenceReleves
                    .Where(cr => cr.RefReleve == carenceReleveVM.RefReleve
                    && cr.CarenceNorme.RefType == carenceReleveVM.CarenceValue.TypeCarence)
                    .Select(cr => cr).ToList<CarenceReleve>();

                // delete each carence in database
                foreach (var carence in carenceToDelete)
                {
                    // Debug.WriteLine(carence.ID);
                    // db.CarenceReleves.Remove(carence);
                    // db.SaveChanges();
                }

                // insert new carences in database
                foreach (var newValue in values)
                {
                    // "ID, RefCarenceNorme,    RefReleve,  RefComboCarence,    ValeurAnalyseCarence"
                    //CarenceReleve carence = new CarenceReleve() {
                    // CreateMap<Carence, CarenceReleve>()
                    //};
                    var carence = MapperConfig.mapper.Map<Carence, CarenceReleve>(newValue);
                    carence.RefReleve = carenceReleveVM.RefReleve;

                    //carence.ValeurAnalyseCarence = newValue.ValeursAnalyse;
                    // carence.RefComboCarence = newValue.TypeChoix;

                    //carence.RefCarenceNorme = newValue.RefNorme;

                    // Debug.WriteLine(carence.ValeurAnalyseCarence);

                    //db.CarenceReleves.Add(carence);
                    // db.SaveChanges();
                }
                return RedirectToAction("Edit", new { id = carenceReleve.RefReleve, typeID = carenceReleveVM.CarenceValue.TypeCarence, page = page });
            }
            else
            {
                Repositories.ShowStateErrors(ModelState);
                var errors = ModelState.Values.SelectMany(v => v.Errors);
            }

            InitViewBag(carenceReleveVM);
            ViewBag.Page = page;

            return View(carenceReleveVM);
        }

        // Initialize ViewBag properties
        private void InitViewBag(CarenceViewModel carenceReleve)
        {
            // espece id for ajax call
            ViewBag.EspeceID = carenceReleve.CarenceNorme.Espece.ID;

            var carenceNorme = db.CarenceNormes
                .Where(c => c.RefType == carenceReleve.CarenceNorme.CarenceType.ID
                && c.RefEspece == carenceReleve.CarenceNorme.Espece.ID)
                .Select(c => new
                {
                    ID = c.RefCarenceElement,
                    Min = c.NormeMin.ToString(),
                    Max = c.NormeMax.ToString()
                })
                .ToList();

            // fil json in script section
            ViewBag.RefCarenceNorme = carenceNorme;

            ViewBag.DDLTypeCarence = new SelectList(Repositories.GetComboEspecesList(db,
                "cmbTypeCarence", carenceReleve.Releve.Parcelle.RefComboEspece, languageID), "Value", "Text", carenceReleve.CarenceNorme.RefType);

            // 4 is default visuelle carence : aucune
            ViewBag.DDLVisuelCarence = new SelectList(Repositories.GetComboList(db,
                "cmbCarence", languageID), "Value", "Text", 4);

            ViewBag.RemarqueCarence = carenceReleve.Releve.RemarqueCarence;

            var typeC = (from tc in db.CarenceTypes
                         join t in db.Traductions on tc.Nom_T equals t.RefElement
                         where t.RefLangue == languageID
                         select new { tc.ID, t.Traduction1 }).ToList();

            ViewBag.TypeCarence = typeC;
        }

        // GET: CarenceReleves/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CarenceReleve carenceReleve = db.CarenceReleves.Find(id);
            if (carenceReleve == null)
            {
                return HttpNotFound();
            }
            return View(carenceReleve);
        }

        // POST: CarenceReleves/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CarenceReleve carenceReleve = db.CarenceReleves.Find(id);
            db.CarenceReleves.Remove(carenceReleve);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        /*
        // https://chsakell.com/2013/06/08/retrieve-json-data-from-mvc-controllers-in-asp-net-mvc/
        public JsonResult GetProductDataJson(string selectedCategory = "All")
        {
            var data = GetData(selectedCategory).Select(p => new {
                ProductId = p.ProductId,
                Name = p.Name,
                Description = p.Description,
                Price = p.Price,
                Category = Enum.GetName(typeof(Category), p.Category)
            });
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        */

        // POST : ChangeTypeCarence
        [HttpPost]
        public JsonResult ChangeTypeCarence(string especeID, string selectedId)
        {
            /*
             * return min and max values for type carence
             * Norme minimale :	0,00	0,00	0,00	0,00	0,00
             * Norme maximale :	0,00	0,00	0,00	0,00	0,00
             */

            if (especeID != string.Empty && selectedId != string.Empty)
            {
                var idEspece = Convert.ToInt32(especeID);
                var idType = Convert.ToInt32(selectedId);

                var carenceNorme = db.CarenceNormes
                    .Where(c => c.RefType == idType
                    && c.RefEspece == idEspece)
                    .Select(c => new { ID = c.RefCarenceElement, Min = c.NormeMin.ToString(), Max = c.NormeMax.ToString() })
                    .ToList();

                //return Json("Response from Create : " + name);

                //var idEspece = Convert.ToInt32(especeID);
                //var variete = Repositories.GetVariete(db, idEspece, languageID);
                return Json(carenceNorme, JsonRequestBehavior.AllowGet);
            }
            return Json("Response Error");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}