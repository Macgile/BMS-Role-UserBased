﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Mvc;
using SiteBMS.ViewModels;

namespace SiteBMS.Controllers
{
    internal class Years
    {
        public string Value { get; set; }
        public string Text { get; set; }
        //public int OrderYear { get; set; }
    }

    public class CumulController : BaseController
    {
        // GET: Cumul /Cumul/Index/354?cumulType=1&year=2014
        [HttpGet]
        public ActionResult Index(int? id, int? cumulType, int? year)
        {
            // 1: client, 2: distributeur, 3: parcelle, 4: releve
            var cumulVm = new List<CumulViewModel>();
            var currentYear = DateTime.Now.Year;
            List<Years> yearsList = new List<Years>();

            ViewBag.ID = (id ?? 0);
            ViewBag.CumulType = (cumulType ?? 0);

            switch (cumulType)
            {
                case 1: // Client 354 : Cumul/Index/354?cumulType=1

                    /*
                    SELECT SUBSTRING(R.Nom, 0, 5) AS Nom
                    FROM Bilan B
                         INNER JOIN Releve R ON B.RefReleve = R.ID
                         INNER JOIN Parcelle P ON R.RefParcelle = P.ID
                    WHERE P.RefClient = 354
                          AND R.IsActive = 1
                          AND R.IsInitial = 0
                          AND P.IsActive = 1
                          AND B.Qte IS NOT NULL AND B.IsValide = 1
                         GROUP BY R.Nom;
                    */

                    //var list = (from b in db.Bilans
                    //            join r in db.Releves on b.RefReleve equals r.ID
                    //            join p in db.Parcelles on r.RefParcelle equals p.ID
                    //            where p.RefClient == id //&& r.IsActive == true
                    //            && r.IsInitial == false && p.IsActive == true
                    //            && b.Qte != null && b.IsValide == true
                    //            let nom = r.Nom.Substring(0, 4)
                    //            select new Years
                    //            {
                    //                Value = nom,
                    //                Text = nom
                    //            }).Distinct().OrderByDescending(y => y.Text);

                    var list = (from b in db.Bilans
                                join p in db.Parcelles.Where(p => p.IsActive == true && p.Client.IsActive == true) on new { b.Releve.RefParcelle, RefClient = (int)id, IsActive = true }
                                  equals new { RefParcelle = p.ID, RefClient = p.RefClient, IsActive = p.IsActive }
                                where b.Releve.IsActive == true && b.Releve.IsInitial == false
                                //orderby b.Releve.Nom
                                group b.Releve by new
                                {
                                    Nom = b.Releve.Nom.Substring(0, 4)
                                } into g
                                select new Years
                                {
                                    Value = g.Key.Nom.Substring(0, 4),
                                    Text = g.Key.Nom.Substring(0, 4)
                                }).OrderBy(o => o.Value);

                    yearsList = list.ToList();

                    // Debug.WriteLine("");

                    // if the parameter "year" is null and yearsList.count > 0, the year is always the last max(year)
                    if (yearsList.Count > 0)
                    {
                        year = (year ?? int.Parse(yearsList.Last().Value));

                        cumulVm =
                            db.Database.SqlQuery<CumulViewModel>(
                                "dbo.GetCumulClient @IdClient, @Annee, @Langue",
                                new SqlParameter("@IdClient", id),
                                new SqlParameter("@Annee", year),
                                new SqlParameter("@Langue", User.LanguageId)).ToList();
                    }

                    var client = db.Clients.Find(id).FullName;

                    ViewBag.Title = client; //  "Clients";

                    break;

                case 2: // Distributeur 182 : Cumul/Index/182?cumulType=2

                    // tous les releves des clients associé a ce distributeur et dont l'année du releve est de l'années en cours
                    // r.IsActive == true
                    // && p.Client.Distributeurs.Contains(id)

                    var listDistri = (from b in db.Bilans
                                      join r in db.Releves on b.RefReleve equals r.ID
                                      join p in db.Parcelles.Where(p => p.IsActive == true
                                      && p.Client.IsActive == true) on r.RefParcelle equals p.ID
                                      join c in db.Clients.Where(c => c.Distributeurs.Any(d => d.ID == id && c.IsActive == true)) on p.RefClient equals c.ID
                                      where r.IsInitial == false && p.IsActive == true
                                      && b.Qte > 0 && b.IsValide == true
                                      let nom = r.Nom.Substring(0, 4)
                                      select new Years
                                      {
                                          Value = nom,
                                          Text = nom
                                      }).Distinct().OrderBy(y => y.Text);

                    yearsList = listDistri.ToList();

                    // if the parameter "year" is null and yearsList.count > 0, the year is always yearsList.first ()
                    if (yearsList.Count > 0)
                    {
                        year = (year ?? int.Parse(yearsList.Last().Value));

                        cumulVm =
                            db.Database.SqlQuery<CumulViewModel>(
                                "dbo.GetCumulDistributeur @IdDistributeur, @Annee, @Langue",
                                new SqlParameter("@IdDistributeur", id),
                                new SqlParameter("@Annee", year),
                                new SqlParameter("@Langue", User.LanguageId)).ToList();
                    }

                    var distributeur = db.Distributeurs.Find(id).Nom;

                    ViewBag.Title = distributeur; // "Distributeurs";
                    break;

                case 3: // Parcelle 16377 : Cumul/Index/16377?cumulType=3

                    var listParcelle = (from b in db.Bilans
                                        join r in db.Releves on b.RefReleve equals r.ID
                                        join p in db.Parcelles.Where(p => p.IsActive == true && p.Client.IsActive == true) on r.RefParcelle equals p.ID
                                        where p.ID == id && r.IsActive == true
                                        && r.IsInitial == false && p.IsActive == true
                                        && b.Qte != null && b.IsValide == true
                                        let nom = r.Nom.Substring(0, 4)
                                        select new Years
                                        {
                                            Value = nom,
                                            Text = nom
                                        }).Distinct().OrderBy(y => y.Text);

                    yearsList = listParcelle.ToList();

                    // if the parameter "year" is null and yearsList.count > 0, the year is always yearsList.first ()
                    if (yearsList.Count > 0)
                    {
                        year = (year ?? int.Parse(yearsList.Last().Value));

                        cumulVm =
                        db.Database.SqlQuery<CumulViewModel>(
                            "dbo.GetCumulParcelle @IdParcelle, @Annee, @Langue",
                            new SqlParameter("@IdParcelle", id),
                            new SqlParameter("@Annee", year),
                            new SqlParameter("@Langue", User.LanguageId)).ToList();
                    }

                    var parcelle = db.Parcelles.Find(id).Nom;

                    ViewBag.Title = parcelle; // "Parcelles";
                    break;

                case 4: // Releve 9097 : Cumul/Index/9097?cumulType=4
                    cumulVm =
                        db.Database.SqlQuery<CumulViewModel>(
                            "dbo.GetCumulReleve @Langue, @IdReleve",
                            new SqlParameter("@Langue", User.LanguageId),
                            new SqlParameter("@IdReleve", id)).ToList();

                    var releve = db.Releves.Find(id).Nom;

                    ViewBag.Title = releve; //  "Releves";

                    break;

                default:
                    ViewBag.Title = "";
                    break;
            }

            ViewBag.Id = id;
            ViewBag.CumulType = cumulType;
            ViewBag.Year = year;
            ViewBag.Years = new SelectList(yearsList, "Value", "Text", year);// (year ?? currentYear));

            //return View(cumulVm);
            return PartialView("_Index", cumulVm);
        }
    }
}