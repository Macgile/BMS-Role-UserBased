﻿using System;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Web.Mvc;
using PagedList;
using SiteBMS.Models;
using SiteBMS.ViewModels;

namespace SiteBMS.Controllers
{
    public class TranslateController : BaseController
    {
        // GET: TranslateViewModels
        public ActionResult Index(int? page, string searchString, string currentFilter, string sortOrder, string emptyWord)
        {
            ViewBag.SortOrder = string.IsNullOrEmpty(sortOrder) ? "SortDesc" : "";
            ViewBag.EmptyWord = emptyWord;


            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;


            var translation1 = (from t in db.Traductions
                                from it in db.Traductions.Where(it => it.RefElement == t.RefElement && it.RefLangue == 2).DefaultIfEmpty()
                                from es in db.Traductions.Where(es => es.RefElement == t.RefElement && es.RefLangue == 3).DefaultIfEmpty()
                                from de in db.Traductions.Where(de => de.RefElement == t.RefElement && de.RefLangue == 4).DefaultIfEmpty()
                                from gb in db.Traductions.Where(gb => gb.RefElement == t.RefElement && gb.RefLangue == 5).DefaultIfEmpty()
                                from pt in db.Traductions.Where(pt => pt.RefElement == t.RefElement && pt.RefLangue == 6).DefaultIfEmpty()
                                from nl in db.Traductions.Where(nl => nl.RefElement == t.RefElement && nl.RefLangue == 8).DefaultIfEmpty()
                                where t.RefLangue == 1 && t.RefElement > 0
                                select new
                                {
                                    Cle = t.Cle ?? "",
                                    Id = t.RefElement,
                                    FR = t.Traduction1,
                                    IT = it.Traduction1,
                                    ES = es.Traduction1,
                                    DE = de.Traduction1,
                                    GB = gb.Traduction1,
                                    PT = pt.Traduction1,
                                    NL = nl.Traduction1
                                });

            // Count the percentage progress of each language
            ViewBag.TermesCount = translation1.Count();
            ViewBag.FRCount = translation1.Count(c => c.FR.Length > 0) * 100 / ViewBag.TermesCount;
            ViewBag.ITCount = translation1.Count(c => c.IT.Length > 0) * 100 / ViewBag.TermesCount;
            ViewBag.ESCount = translation1.Count(c => c.ES.Length > 0) * 100 / ViewBag.TermesCount;
            ViewBag.DECount = translation1.Count(c => c.DE.Length > 0) * 100 / ViewBag.TermesCount;
            ViewBag.GBCount = translation1.Count(c => c.GB.Length > 0) * 100 / ViewBag.TermesCount;
            ViewBag.PTCount = translation1.Count(c => c.PT.Length > 0) * 100 / ViewBag.TermesCount;
            ViewBag.NLCount = translation1.Count(c => c.NL.Length > 0) * 100 / ViewBag.TermesCount;

            // filter by empty word
            switch (emptyWord)
            {
                case "Cle": translation1 = translation1.Where(t => t.Cle.Length == 0).Select(c => c); break;
                case "FR": translation1 = translation1.Where(t => t.FR.Length == 0).Select(c => c); break;
                case "IT": translation1 = translation1.Where(t => t.IT.Length == 0).Select(c => c); break;
                case "ES": translation1 = translation1.Where(t => t.ES.Length == 0).Select(c => c); break;
                case "DE": translation1 = translation1.Where(t => t.DE.Length == 0).Select(c => c); break;
                case "GB": translation1 = translation1.Where(t => t.GB.Length == 0).Select(c => c); break;
                case "PT": translation1 = translation1.Where(t => t.PT.Length == 0).Select(c => c); break;
                case "NL": translation1 = translation1.Where(t => t.NL.Length == 0).Select(c => c); break;
            }

            var translation = translation1.AsEnumerable().Select(x => new TranslateViewModel
            {
                Cle = x.Cle,
                Id = x.Id,
                FR = x.FR,
                IT = x.IT,
                ES = x.ES,
                DE = x.DE,
                GB = x.GB,
                PT = x.PT,
                NL = x.NL
            });

            // search in cle and traduction field
            if (!string.IsNullOrEmpty(searchString))
            {
                translation = translation.Where(
               t => t.Cle.StartsWith(searchString, StringComparison.InvariantCultureIgnoreCase)
            || t.FR.StartsWith(searchString, StringComparison.InvariantCultureIgnoreCase)
            || t.IT.StartsWith(searchString, StringComparison.InvariantCultureIgnoreCase)
            || t.ES.StartsWith(searchString, StringComparison.InvariantCultureIgnoreCase)
            || t.DE.StartsWith(searchString, StringComparison.InvariantCultureIgnoreCase)
            || t.GB.StartsWith(searchString, StringComparison.InvariantCultureIgnoreCase)
            || t.PT.StartsWith(searchString, StringComparison.InvariantCultureIgnoreCase)
            || t.NL.StartsWith(searchString, StringComparison.InvariantCultureIgnoreCase));
            }

            // filter oder by
            switch (sortOrder)
            {
                case "SortDesc":
                    translation = translation.OrderByDescending(s => s.Cle);
                    break;
            }

            var pageNumber = (page ?? 1);
            return View(translation.ToPagedList(pageNumber, 10));
        }

        // GET: TranslateViewModels/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TranslateViewModels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Cle,FR,IT,ES,DE,GB,PT,NL")] TranslateViewModel translateViewModel)
        {
            if (ModelState.IsValid)
            {
                //db.TranslateViewModels.Add(translateViewModel);

                int maxId = db.Traductions.Max(u => u.RefElement) + 1;

                var fr = new Traduction()
                {
                    RefElement = maxId,
                    Cle = translateViewModel.Cle,
                    RefLangue = 1,
                    Culture = "fr",
                    Traduction1 = translateViewModel.FR,
                    IsDefaut = true
                };

                db.Traductions.Add(fr);
                if (db.SaveChanges() > 0)
                {
                    var langues = new int[] { 2, 3, 4, 5, 6, 8 };
                    foreach (var langue in langues)
                    {
                        var item = new Traduction()
                        {
                            RefElement = maxId,
                            Cle = translateViewModel.Cle,
                            Traduction1 = translateViewModel.FR?.Trim() ?? "",
                            IsDefaut = false,
                            RefLangue = langue
                        };

                        switch (langue)
                        {
                            case 2: // IT
                                item.Culture = "it";
                                item.Traduction1 = translateViewModel.IT?.Trim() ?? "";
                                break;

                            case 3: // ES
                                item.Culture = "es";
                                item.Traduction1 = translateViewModel.ES?.Trim() ?? "";
                                break;

                            case 4: // DE
                                item.Culture = "de";
                                item.Traduction1 = translateViewModel.DE?.Trim() ?? "";
                                break;

                            case 5: // GB
                                item.Culture = "gb";
                                item.Traduction1 = translateViewModel.GB?.Trim() ?? "";
                                break;

                            case 6: // PT
                                item.Culture = "pt";
                                item.Traduction1 = translateViewModel.PT?.Trim() ?? "";
                                break;

                            case 8: // NL
                                item.Culture = "nl";
                                item.Traduction1 = translateViewModel.NL?.Trim() ?? "";
                                break;
                        }

                        db.Traductions.Add(item);
                        db.SaveChanges();
                    }
                }

                return RedirectToAction("Index");
            }

            return View(translateViewModel);
        }

        // POST: Add Distributor
        // http://vault.lozanotek.com/archive/2010/04/16/posting_json_data_to_mvc_controllers.aspx
        [HttpPost]
        public ActionResult SaveTranslate(TranslateViewModel translate)
        {
            // ajouter auto les langues manquante
            if (translate != null && translate.Id > 0)
            {
                var traduction = db.Traductions.Where(t => t.RefElement == translate.Id).ToList();

                foreach (var item in traduction)
                {
                    Debug.WriteLine(item.RefLangue);

                    // save key if not null
                    if (!string.IsNullOrEmpty(translate.Cle))
                        item.Cle = translate.Cle.Trim();

                    switch (item.RefLangue)
                    {
                        case 1: item.Traduction1 = translate.FR?.Trim() ?? ""; break;
                        case 2: item.Traduction1 = translate.IT?.Trim() ?? ""; break;
                        case 3: item.Traduction1 = translate.ES?.Trim() ?? ""; break;
                        case 4: item.Traduction1 = translate.DE?.Trim() ?? ""; break;
                        case 5: item.Traduction1 = translate.GB?.Trim() ?? ""; break;
                        case 6: item.Traduction1 = translate.PT?.Trim() ?? ""; break;
                        case 8: item.Traduction1 = translate.NL?.Trim() ?? ""; break;
                    }
                    db.Entry(item).State = EntityState.Modified;

                    db.SaveChanges();
                }
                return Json(new { id = translate.Id });
            }
            return Json("Response Error");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}