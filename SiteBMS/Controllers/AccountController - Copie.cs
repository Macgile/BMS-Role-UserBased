﻿using Newtonsoft.Json;
using SiteBMS.DAL.Security;
using SiteBMS.Models;
using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace SiteBMS.Controllers
{
    public class AccountController : BaseController
    {
        //private DataContext Context = new DataContext();

        //
        // GET: /Account/
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(LoginViewModel model, string returnUrl = "")
        {
            if (!ModelState.IsValid) return View(model);

            var user = db.Users.FirstOrDefault(u => u.Username == model.Username && u.Password == model.Password);

            //var util = db.Utilisateurs.FirstOrDefault(u => u.Login == model.Username && u.PassWord == model.Password);
            //var utilisateur = db.Utilisateur.FirstOrDefault(u => u.Login == "tech" && u.PassWord == "pm");

            if (user != null)
            {
                var roles = user.Roles.FirstOrDefault();

                var serializeModel = new CustomPrincipalSerializeModel
                {
                    UserId = user.UserId,
                    LanguageId = user.LanguageID,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    roles = roles?.RoleName ?? "client",
                    RoleId = roles?.RoleId ?? 0
                };

                var userData = JsonConvert.SerializeObject(serializeModel);
                var authTicket = new FormsAuthenticationTicket(
                    1, // version
                    user.Email,
                    DateTime.Now,
                    DateTime.Now.AddMinutes(30),
                    model.RememberMe,
                    userData);

                var encTicket = FormsAuthentication.Encrypt(authTicket);
                var faCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encTicket);
                Response.Cookies.Add(faCookie);

                // redirect to home->index
                return RedirectToAction("Index", "Clients");

                // TODO : RedirectToAction by profil user
                /*
                    if (roles.Contains("Admin"))
                    {
                        return RedirectToAction("Index", "Admin");
                    }
                    else if (roles.Contains("User"))
                    {
                        return RedirectToAction("Index", "User");
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    */
            }

            ModelState.AddModelError("", "Nom d'utilisateur et/ou mot de passe incorrect");

            return View(model);
        }

        [AllowAnonymous]
        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            // replace -> Login by Index
            //return RedirectToAction("Login", "Account", null);
            return RedirectToAction("Index", "Account", null);
        }
    }
}