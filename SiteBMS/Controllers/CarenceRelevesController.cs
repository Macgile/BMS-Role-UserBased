﻿using SiteBMS.Models;
using SiteBMS.ViewModels;
using System;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using System.Collections.Generic;

namespace SiteBMS.Controllers
{
    public class CarenceRelevesController : BaseController
    {
        // GET: CarenceReleves
        public ActionResult Index()
        {
            var carenceReleves = db.CarenceReleves
                .Include(c => c.CarenceNorme)
                .Include(c => c.Releve);
            return View(carenceReleves.ToList());
        }

        // GET: CarenceReleves List for  releve
        public ActionResult List(int? id, int? page)
        {
            const string sqlQuery =
                @"SELECT C.TYPEID AS RefType, C.Traduction AS Name, B.CarenceCount, B.RefReleve, B.RefParcelle
                                FROM(SELECT CT.ID AS TYPEID, T.Traduction FROM BMS.dbo.CarenceType AS CT
                                    INNER JOIN Traduction AS T ON T.RefElement = CT.Nom_T AND T.RefLangue = @LangueID)
                                AS C LEFT JOIN (SELECT CN.RefType, CR.RefReleve, R.RefParcelle, COUNT(CN.RefType) AS CarenceCount
                                FROM dbo.CarenceReleve AS CR
                                    INNER JOIN dbo.CarenceNorme AS CN ON CR.RefCarenceNorme = CN.ID
                                    INNER JOIN dbo.Releve AS R ON CR.RefReleve = R.ID
                        WHERE CR.RefReleve = @ReleveID
                        GROUP BY CN.RefType, CR.RefReleve, R.RefParcelle) AS B ON C.TYPEID = B.RefType";

            var parameters = new object[]
            {
                new SqlParameter("@ReleveID", id),
                new SqlParameter("@LangueID", User.LanguageId)
            };

            var carenceVm = db.Database.SqlQuery<CarenceTypeViewModel>(sqlQuery, parameters).ToList();
            ViewBag.Releve = db.Releves.Find(id);
            ViewBag.Page = (page ?? 1);

            return View(carenceVm);
        }

        // GET: CarenceReleves/Details/5
        public ActionResult Details(int? id, int? typeId, int? page)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            // Feuilles(1) - Fruits(2) - Rameaux(3)
            // 1 : default
            var typeCarence = (typeId ?? 1);

            // List of all carence of type : typeCarence
            // List<CarenceReleve>
            var carenceList = (from cr in db.CarenceReleves
                               join cn in db.CarenceNormes on cr.RefCarenceNorme equals cn.ID
                               where cn.RefType == typeCarence
                               && cn.RefEspece == cr.Releve.Parcelle.RefComboEspece
                               && cr.RefReleve == id
                               select cr).ToList();

            if (carenceList.Equals(null))
            {
                return HttpNotFound();
            }

            // first in the list
            var carenceReleve = carenceList.FirstOrDefault();

            // ViewModel (no based to model)
            var carenceValuesVm = new CarenceValuesViewModel
            {
                CarenceReleve = carenceReleve,
                TypeCarence = typeCarence,
                Releve = carenceReleve?.Releve,
                RefReleve = carenceReleve?.RefReleve ?? 0,
                EspeceID = carenceReleve?.Releve.Parcelle.RefComboEspece ?? 0
            };

            // fill viewmodel with values
            foreach (var c in carenceList)
            {
                carenceValuesVm.PropertySet(carenceValuesVm, "tbx_" + c.CarenceNorme.RefCarenceElement, c.ValeurAnalyseCarence.ToString());
                carenceValuesVm.PropertySet(carenceValuesVm, "ddl_" + c.CarenceNorme.RefCarenceElement, c.RefComboCarence);
            }

            carenceValuesVm.EspeceID = carenceReleve.Releve.Parcelle.RefComboEspece;

            ViewBag.Page = (page ?? 1);
            InitViewBag(carenceValuesVm.CarenceReleve);

            return View(carenceValuesVm);
        }

        // GET: CarenceReleves/Create id = refReleve, typeId = CarenceType
        public ActionResult Create(int? id, int? typeId, int? page)
        {
            // Feuilles(1) - Fruits(2) - Rameaux(3)
            // 1 : by default
            var typeCarence = (typeId ?? 1);

            var refReleve = (id ?? default(int));

            var releve = db.Releves.Find(refReleve);

            // carenceNorme
            // TODO : only one carenceNorme
            var carenceNorme = (from cn in db.CarenceNormes
                               where cn.RefType == typeCarence
                               && cn.RefEspece == releve.Parcelle.RefComboEspece
                               select cn).FirstOrDefault();

            // new carenceReleve
            var carenceReleve = new CarenceReleve
            {
                RefReleve = refReleve,
                Releve = releve,
                CarenceNorme = carenceNorme,
            };

            // ViewModel (no based to model)
            var carenceValuesVm = new CarenceValuesViewModel
            {
                CarenceReleve = carenceReleve,
                TypeCarence = typeCarence,
                RefReleve = carenceReleve.RefReleve,
                EspeceID = releve.Parcelle.RefComboEspece,
                Releve = releve
            };

            ViewBag.Page = (page ?? 1);

            InitViewBag(carenceValuesVm.CarenceReleve);

            ViewBag.Page = (page ?? 1);
            return View(carenceValuesVm);
        }

        // POST: CarenceReleves/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CarenceValuesViewModel carenceReleveVm, int? page)
        {
            var vals = carenceReleveVm.GetValues(carenceReleveVm);
            carenceReleveVm.CarenceReleve = db.CarenceReleves.Find(carenceReleveVm.RefReleve);

            if (ModelState.IsValid)
            {
                SaveReleveCarence(carenceReleveVm, vals);
                return RedirectToAction("List", new { id = carenceReleveVm.RefReleve, page });
            }

            InitViewBag(carenceReleveVm.CarenceReleve);
            ViewBag.Page = page;
            return View(carenceReleveVm);
        }

        // GET: CarenceReleves/Edit/5 (id == id releve, typeID == typeCarence)
        public ActionResult Edit(int? id, int? typeId, int? page)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            // Feuilles(1) - Fruits(2) - Rameaux(3)
            // 1 : default
            var typeCarence = (typeId ?? 1);

            // List of all carence of type : typeCarence
            // List<CarenceReleve>
            var carenceList = (from cr in db.CarenceReleves
                               join cn in db.CarenceNormes on cr.RefCarenceNorme equals cn.ID
                               where cn.RefType == typeCarence
                               && cn.RefEspece == cr.Releve.Parcelle.RefComboEspece
                               && cr.RefReleve == id
                               select cr).ToList();

            if (carenceList.Equals(null))
            {
                return HttpNotFound();
            }

            // first in the list
            var carenceReleve = carenceList.FirstOrDefault();

            // ViewModel (no based to model)
            var carenceValuesVm = new CarenceValuesViewModel
            {
                CarenceReleve = carenceReleve,
                TypeCarence = typeCarence,
                Releve = carenceReleve?.Releve,
                RefReleve = carenceReleve?.RefReleve ?? 0,
                EspeceID = carenceReleve?.Releve.Parcelle.RefComboEspece ?? 0
            };

            // fill viewmodel with values
            foreach (var c in carenceList)
            {
                carenceValuesVm.PropertySet(carenceValuesVm, "tbx_" + c.CarenceNorme.RefCarenceElement, c.ValeurAnalyseCarence.ToString());
                carenceValuesVm.PropertySet(carenceValuesVm, "ddl_" + c.CarenceNorme.RefCarenceElement, c.RefComboCarence);
            }

            if (carenceReleve != null) carenceValuesVm.EspeceID = carenceReleve.Releve.Parcelle.RefComboEspece;
            //carenceValuesVm.TypeCarence = typeCarence;

            ViewBag.Page = (page ?? 1);

            InitViewBag(carenceValuesVm.CarenceReleve);
            return View(carenceValuesVm);
        }

        // POST: CarenceReleves/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CarenceValuesViewModel carenceReleveVm, int? page)
        {
            page = (page ?? 1);

            // get all value in CarenceValuesViewModel instance
            var vals = carenceReleveVm.GetValues(carenceReleveVm);
            carenceReleveVm.CarenceReleve = db.CarenceReleves.Find(carenceReleveVm.CarenceReleve.ID);

            if (ModelState.IsValid)
            {
                SaveReleveCarence(carenceReleveVm, vals);
                return RedirectToAction("List", new { id = carenceReleveVm.RefReleve, page });
            }
            else
            {
                Repositories.ShowStateErrors(ModelState);
                var errors = ModelState.Values.SelectMany(v => v.Errors);
            }

            InitViewBag(carenceReleveVm.CarenceReleve);
            ViewBag.Page = page;

            return View(carenceReleveVm);
        }

        // Save carenceReleve in Database
        private void SaveReleveCarence(CarenceValuesViewModel carenceReleveVm, IEnumerable<Carence> vals)
        {
            // fonctionne pour add mais pas pour edition car dans edit il y a pas de refreleve
            carenceReleveVm.Releve = db.Releves.Find(carenceReleveVm.RefReleve);

            // create all Carence instances in List
            var values = (from v in vals
                          join re in db.CarenceNormes on v.RefNorme equals re.RefCarenceElement
                          where re.RefType == carenceReleveVm.TypeCarence
                                && re.RefEspece == carenceReleveVm.EspeceID
                          select new Carence
                          {
                              ValeursAnalyse = v.ValeursAnalyse,
                              TypeChoix = v.TypeChoix,
                              RefNorme = re.ID,

                          });

            // must provide type carence 1/2/3
            // list of all carence for RefType carence (1,2 or 3)
            var carenceToDelete = db.CarenceReleves
                .Where(cr => cr.RefReleve == carenceReleveVm.RefReleve
                             && cr.CarenceNorme.RefType == carenceReleveVm.TypeCarence)
                .Select(cr => cr).ToList();

            // delete each carence in database
            foreach (var carence in carenceToDelete)
            {
                //System.Diagnostics.Debug.WriteLine(carence.ID);
                db.CarenceReleves.Remove(carence);
                db.SaveChanges();
            }

            // create all instance of CarenceReleve
            // and insert in database
            foreach (var carence in values.Select(value => MapperConfig.mapper.Map<Carence, CarenceReleve>(value)))
            {
                carence.RefReleve = carenceReleveVm.RefReleve;
                carence.Releve = carenceReleveVm.Releve;
                // System.Diagnostics.Debug.WriteLine(carence.ValeurAnalyseCarence);
                // manque CarenceNorme qui est a null
                db.CarenceReleves.Add(carence);
                db.SaveChanges();
            }
        }

        // Initialize ViewBag properties
        private void InitViewBag(CarenceReleve carenceReleve)
        {
            // espece id for ajax call
            ViewBag.EspeceID = carenceReleve.CarenceNorme.Espece.ID;

            var carenceNorme = db.CarenceNormes
                .Where(c => c.RefType == carenceReleve.CarenceNorme.CarenceType.ID
                            && c.RefEspece == carenceReleve.CarenceNorme.Espece.ID)
                .Select(c => new
                {
                    ID = c.RefCarenceElement,
                    Min = c.NormeMin.ToString(),
                    Max = c.NormeMax.ToString()
                })
                .ToList();

            // fil json in script section
            ViewBag.RefCarenceNorme = carenceNorme;

            // type de carence (feuilles/fruits/rameau) choisi
            var typeCarence = (from ct in db.CarenceTypes
                           join t in db.Traductions on ct.Nom_T equals t.RefElement
                           where t.RefLangue == User.LanguageId && ct.ID == carenceReleve.CarenceNorme.CarenceType.ID
                           select t.Traduction1).Distinct();

            ViewBag.TypeCarence = typeCarence.FirstOrDefault();

            // 4 is default visual carence : aucune
            ViewBag.DDLVisuelCarence = new SelectList(Repositories.GetComboList(db,
                "cmbCarence", User.LanguageId), "Value", "Text", 4);

            ViewBag.RemarqueCarence = carenceReleve.Releve.RemarqueCarence;
        }

        // POST : ChangeTypeCarence
        [HttpPost]
        public JsonResult ChangeTypeCarence(string especeId, string selectedId)
        {
            /*
             * return min and max values for type carence
             * Norme minimale :	0,00	0,00	0,00	0,00	0,00
             * Norme maximale :	0,00	0,00	0,00	0,00	0,00
             */

            if (especeId != string.Empty && selectedId != string.Empty)
            {
                var idEspece = Convert.ToInt32(especeId);
                var idType = Convert.ToInt32(selectedId);

                var carenceNorme = db.CarenceNormes
                    .Where(c => c.RefType == idType
                                && c.RefEspece == idEspece)
                    .Select(
                        c => new { ID = c.RefCarenceElement, Min = c.NormeMin.ToString(), Max = c.NormeMax.ToString() })
                    .ToList();

                //return Json("Response from Create : " + name);

                //var idEspece = Convert.ToInt32(especeID);
                //var variete = Repositories.GetVariete(db, idEspece, User.LanguageId);
                return Json(carenceNorme, JsonRequestBehavior.AllowGet);
            }
            return Json("Response Error");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}