﻿using SiteBMS.Models;
using SiteBMS.ViewModels;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace SiteBMS.Controllers
{
    public class BilanReportController : BaseController
    {
        internal class GetProduit
        {
            public int ID { get; set; }
            public string Traduction { get; set; }
        }

        // GET: BilanReport id = releve id
        [HandleError(View = "Error")]
        public ActionResult Index(int? id, int? page)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            // call stored procedure
            var affectedRows = db.Database.ExecuteSqlCommand("dbo.GenerateBilanAutoWeb @IdReleve = {0}", id);

            if (affectedRows == 0)
            {
                return HttpNotFound();
            }

            var releve = db.Releves.Find(id);

            var produits = db.Database.SqlQuery<GetProduit>("dbo.GetProduit @RefLangue = {0}, @RefEspece = {1} ",
                User.LanguageId.ToString(), releve.Parcelle.RefComboEspece.ToString())
                 .ToList()
                 .Select(item => new SelectListItem
                 {
                     Value = item.ID.ToString(),
                     Text = item.Traduction
                 });

            ViewBag.DDLProduits = produits;
            ViewBag.Releve = releve;

            ViewBag.TitleBilan = Utils.UcFirst(releve.Parcelle.Nom);

            ViewBag.Page = (page ?? 1);
            ViewBag.StadePhenologique = (from t in db.Traductions
                                         where t.RefElement == 232 && t.RefLangue == User.LanguageId
                                         select t.Traduction1).FirstOrDefault();

            var etapeBilan = (from p in db.Parcelles
                              join r in db.Releves on p.ID equals r.RefParcelle
                              join e in db.Etapes on p.RefComboEspece equals e.RefEspece
                              from tn in db.Traductions.Where(t => t.RefElement == e.Nom_T && t.RefLangue == User.LanguageId).DefaultIfEmpty()
                              where r.ID == id
                              select new EtapeBilan
                              {
                                  ID = e.ID,
                                  ImgEtap = e.ImgEtap,
                                  RefEspece = e.RefEspece,
                                  Nom = tn.Traduction1,
                                  Description = "" //td.Traduction1
                              }).OrderBy(e => e.ID);

            var programeBilan = (from r in db.Releves
                                 join p in db.Parcelles on r.RefParcelle equals p.ID
                                 join pn in db.ProgrammeNutritions on p.RefComboEspece equals pn.RefEspece
                                 from tn in db.Traductions
                                 from td in db.Traductions
                                 where r.ID == id && td.RefLangue == User.LanguageId && tn.RefLangue == User.LanguageId
                                       && tn.RefElement == pn.Nom_T && td.RefElement == pn.Description_T
                                 select new ProgramBilan
                                 {
                                     ID = pn.ID,
                                     Nom = tn.Traduction1,
                                     Description = td.Traduction1
                                 });

            var bilan = (from b in db.Bilans
                         join r in db.Releves on b.RefReleve equals r.ID
                         join p in db.Parcelles on r.RefParcelle equals p.ID
                         join v in db.Varietes on p.RefVariete equals v.ID
                         join e in db.Especes on v.RefEspece equals e.ID
                         join pr in db.Produits on b.RefProduit equals pr.ID
                         from te in db.Traductions
                         from tv in db.Traductions
                         from tp in db.Traductions
                         where
                             b.RefReleve == id 
                             && te.RefLangue == 1 /*User.LanguageId */
                             && tv.RefLangue == 1 /*User.LanguageId */
                             && tp.RefLangue == 1 /*User.LanguageId */
                             && te.RefElement == e.Nom_T && tv.RefElement == v.Nom_T && tp.RefElement == pr.Nom_T
                         select new BilanValues
                         {
                             ID = b.ID,
                             RefProgrammeNutrition = b.RefProgrammeNutrition,
                             RefEtape = b.RefEtape,
                             RefProduit = b.RefProduit,
                             Qte = b.Qte,
                             QteInitial = b.QteInitial,
                             IsValide = b.IsValide,
                             IsPerso = b.IsPerso,
                             Releve = r.Nom,
                             Parcelle = p.Nom,
                             Espece = te.Traduction1,
                             Variete = tv.Traduction1,
                             Produit = tp.Traduction1,
                             UnitProd = pr.UnitProd,
                             Surface = p.Surface,
                             PersoCase = (b.Qte == b.QteInitial) ? 0 : (b.Qte == null) ? 2 : (b.QteInitial == null) ? 3 : 1
                         });

            var bilanReportVm = new BilanReportViewModel()
            {
                Etape = etapeBilan.ToList(),
                Programme = programeBilan.ToList(),
                Values = bilan.ToList()
            };

            return View(bilanReportVm);
        }

        // POST: add Product
        [HttpPost]
        public JsonResult AddProduct(int idProduct, decimal quantite, int idReleve, int idProgram, int idEtape)
        {
            var item = new Bilan()
            {
                RefReleve = idReleve,
                RefProgrammeNutrition = idProgram,
                RefEtape = idEtape,
                RefProduit = idProduct,
                Qte = quantite,
                QteInitial = quantite,
                NbApplication = 1,
                IsValide = true,
                IsPerso = true,
                Origine = "Web",
                PersoBy = null
            };

            db.Bilans.Add(item);
            var resultBilan = db.SaveChanges();

            if (resultBilan == 1)
            {
                // SET releve as IsPerso
                var releve = db.Releves.Find(idReleve);
                db.Entry(releve).State = EntityState.Modified;
                releve.IsPerso = true;
                db.SaveChanges();
            }
            return Json(resultBilan);
        }

        // POST: Edit Product
        [HttpPost]
        public JsonResult UpdateProduct(int id, int idProduct, decimal? quantite)
        {
            if (id == 0 || idProduct == 0) return Json("error");

            var bilan = db.Bilans.Find(id);
            db.Entry(bilan).State = EntityState.Modified;

            bilan.RefProduit = idProduct;
            bilan.Qte = quantite;
            bilan.QteInitial = quantite;
            bilan.IsPerso = true;

            var releve = db.Releves.Find(bilan.RefReleve);
            db.Entry(releve).State = EntityState.Modified;
            releve.IsPerso = true;

            var result = db.SaveChanges();
            return Json(result);
        }

        // POST: delete Product
        [HttpPost]
        public JsonResult DeleteProduct(int id)
        {
            if (id == 0) return Json("error");

            var bilan = db.Bilans.Find(id);
            db.Entry(bilan).State = EntityState.Modified;
            bilan.IsValide = false;

            //db.Bilans.Remove(bilan);

            var releve = db.Releves.Find(bilan.RefReleve);
            db.Entry(releve).State = EntityState.Modified;
            releve.IsPerso = true;

            var result = db.SaveChanges();
            return Json(result);
        }
    }
}