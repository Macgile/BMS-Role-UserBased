﻿using SiteBMS.DAL.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SiteBMS.DAL;
using System.Diagnostics;
using SiteBMS.Helpers;
using System.Threading;

namespace SiteBMS.Controllers
{
    public class BaseController : Controller
    {
        protected readonly DataContext db = new DataContext();

        protected new CustomPrincipal User => HttpContext.User as CustomPrincipal;
        // TODO : Array Espece
        protected readonly int[] especes = { 11,12,13 }; // aucun, mais, cereal
        protected const int pageSize = 8;
        protected const int userLanguage = 82; // french by default
        // current year for cumul
        protected readonly int currentYear = DateTime.Now.Year;

        protected override IAsyncResult BeginExecuteCore(AsyncCallback callback, object state)
        {
            string cultureName = null;

            // Attempt to read the culture cookie from Request
            HttpCookie cultureCookie = Request.Cookies["_culture"];
            if (cultureCookie != null)
                cultureName = cultureCookie.Value;
            else
                cultureName = Request.UserLanguages != null && Request.UserLanguages.Length > 0 ?
                        Request.UserLanguages[0] :  // obtain it from HTTP header AcceptLanguages
                        null;
            // Validate culture name
            cultureName = CultureHelper.GetImplementedCulture(cultureName); // This is safe

            // Modify current thread's cultures           
            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(cultureName);
            Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;

            return base.BeginExecuteCore(callback, state);
        }


    }
}