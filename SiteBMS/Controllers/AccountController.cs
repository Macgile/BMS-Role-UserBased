﻿using Newtonsoft.Json;
using SiteBMS.DAL.Security;
using SiteBMS.Models;
using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace SiteBMS.Controllers
{
    [AllowAnonymous]
    public class AccountController : BaseController
    {
        // GET: /Account/ First Connexion
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(LoginViewModel model, string returnUrl = "")
        {
            if (ModelState.IsValid)
            {
                var utilisateur =
                    db.Utilisateurs.FirstOrDefault(u => u.Login == model.Username && u.PassWord == model.Password && u.IsActive);

                if (utilisateur != null)
                {
                    var roles = utilisateur.RoleUtilisateurs;

                    var pays = Repositories.ConvertLangueToPays(db, utilisateur?.RefLangue ?? 1);

                    // the new CustomPrincipal is implemented in global.asax
                    var serializeModel = new CustomPrincipalSerializeModel
                    {
                        UserId = utilisateur.ID,
                        LanguageId = utilisateur?.RefLangue ?? 1,
                        PayId = pays,
                        FirstName = utilisateur.Nom,
                        LastName = utilisateur.Prenom,
                        Roles = roles.Name,
                        RefRole = utilisateur?.RefRole ?? 0,
                        RefId = utilisateur?.RefId ?? 0,
                        RoleName = utilisateur?.RoleUtilisateurs.Name ?? "client",
                        RoleId = utilisateur?.RoleUtilisateurs.Id ?? 0
                    };

                    var userData = JsonConvert.SerializeObject(serializeModel);
                    var authTicket = new FormsAuthenticationTicket(
                        1,
                        //user.Email,
                        utilisateur.Email,
                        DateTime.Now,
                        DateTime.Now.AddMinutes(30),
                        model.RememberMe,
                        userData);

                    var encTicket = FormsAuthentication.Encrypt(authTicket);
                    var faCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encTicket);
                    Response.Cookies.Add(faCookie);

                    // redirect to home->index
                    // not allowed for customer
                    if (utilisateur.RefRole == (int)RoleType.Client)
                    {
                        return RedirectToAction("Details", "Clients", new { id = utilisateur.RefId });
                    }
                    else if (utilisateur.RefRole == (int)RoleType.Distributeur)
                    {
                        return RedirectToAction("Details", "Distributeurs", new { id = utilisateur.RefId });
                    }
                    else
                    {
                        return RedirectToAction("Index", "Clients");
                    }

                    /*
                    if (roles.Contains("Admin"))
                    {
                        return RedirectToAction("Index", "Admin");
                    }
                    else if (roles.Contains("User"))
                    {
                        return RedirectToAction("Index", "User");
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    */
                }

                ModelState.AddModelError("", "Incorrect username and/or password");
            }

            return View(model);
        }

        //public ActionResult Disclaimer()
        //{
        //    return View();
        //}

        [AllowAnonymous]
        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            // replace -> Login by Index
            //return RedirectToAction("Login", "Account", null);
            return RedirectToAction("Index", "Account", null);
        }
    }
}