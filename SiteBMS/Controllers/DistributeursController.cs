﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using PagedList;
using SiteBMS.Models;
using SiteBMS.ViewModels;

namespace SiteBMS.Controllers
{
    public class DistributeursController : BaseController
    {
        // only if user have a role > distributeur(4)
        // GET: Distributeurs
        public ActionResult Index(string sortOrder, string currentFilter, int? page, string searchString, int? refLangue)
        {
            ViewBag.SortByName = string.IsNullOrEmpty(sortOrder) ? "SortDesc" : ""; 
            ViewBag.SortByPays = sortOrder == "Pays" ? "PaysDesc" : "Pays";
            ViewBag.SortByZip = sortOrder == "CodePostal" ? "CodePostalDesc" : "CodePostal";
            ViewBag.SortByDate = sortOrder == "SortByDate" ? "SortByDateDesc" : "SortByDate";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;
            ViewBag.CurrentSort = sortOrder;
            ViewBag.RefLangue = refLangue;

            // correct, verifier avec sql
            var distributeurs = (from th in db.Techniciens
                                 join r in db.Techniciens on th.RefRespZone equals r.ID
                                 join d in (db.Distributeurs.Include("Clients").Where(di => di.IsActive).Select(di => di)) on th.ID equals d.RefTechBMS
                                 where th.ID == User.RefId || r.ID == User.RefId || r.RefRespZone == User.RefId
                                 orderby d.Nom
                                 select d).ToList();

            // FAUX uniquement les distributeur de l'utilisateur et ou de ses subalterne
            // distributeurs = db.Distributeurs.Where(di => di.IsActive).Select(di => di).ToList();

            if (refLangue != null)
            {
                // Debug.WriteLine(refLangue);
                distributeurs = distributeurs.Where(s => s.RefComboPays == refLangue).ToList();
            }

            // search result
            if (!string.IsNullOrEmpty(searchString))
            {
                //distributeurs = distributeurs.Where(s => s.Nom.ToLower().Contains(searchString.ToLower())).ToList();
                distributeurs = distributeurs.Where(s => s.Nom.StartsWith(searchString, StringComparison.InvariantCultureIgnoreCase)).ToList();
            }

            // countries list
            var pays = Repositories.GetCombo(db, "cmbPays", User.LanguageId);
            ViewBag.DDLPays = pays;

            distributeurs.ToList()
                .ForEach(
                    c => c.Pays = pays.Where(t => t.Value == c.RefComboPays.ToString())
                        .Select(t => t.Text).FirstOrDefault());

            switch (sortOrder)
            {
                //case "Sort":
                //    distributeurs = distributeurs.OrderBy(s => s.Nom).ToList();
                //    break;

                case "SortDesc":
                    distributeurs = distributeurs.OrderByDescending(s => s.Nom).ToList();
                    break;

                case "Pays":
                    distributeurs = distributeurs.OrderBy(s => s.Pays).ToList();
                    break;

                case "PaysDesc":
                    distributeurs = distributeurs.OrderByDescending(s => s.Pays).ToList();
                    break;

                case "CodePostal":
                    distributeurs = distributeurs.OrderBy(s => s.CodePostal).ToList();
                    break;

                case "CodePostalDesc":
                    distributeurs = distributeurs.OrderByDescending(s => s.CodePostal).ToList();
                    break;

                case "SortByDate":
                    distributeurs = distributeurs.OrderBy(s => s.IDDatCreat).ToList();
                    break;

                case "SortByDateDesc":
                    distributeurs = distributeurs.OrderByDescending(s => s.IDDatCreat).ToList();
                    break;
            }

            var distributeurVm =
                MapperConfig.mapper.Map<List<Distributeur>, List<DistributeurViewModel>>(distributeurs);

            var idDistributeur = distributeurs.Select(d => d.ID).ToList();


            /*
            var listd = (from b in db.Bilans
             join dc in db.Distributeur_Clients on b.Releve.Parcelle.RefClient equals dc.RefClient
             where
               (new int[] { 179, 91, 63, 90, 144, 132, 84, 112 }).Contains(dc.RefDistributeur) &&
               b.Qte > 0 &&
               b.Releve.IsActive == true &&
               b.Releve.IsInitial == false &&
               b.Releve.Parcelle.IsActive == true &&
               b.Releve.DateReleve.Year > 2005
             group dc by new
             {
                 dc.RefDistributeur
             } into g
             select new
             {
                 g.Key.RefDistributeur
             });
             */


            // find distributeur has cummul
            // tous les releve de l'année en cours des clients de ces controleurs (ceux de la liste).
            var distribCumul = (from d in db.Distributeurs
                                where (from b in db.Bilans
                                join c in db.Clients on b.Releve.Parcelle.Client.ID equals c.ID
                                where b.Releve.Parcelle.Client.IsActive == true
                                && b.Releve.Parcelle.IsActive == true
                                && b.Releve.IsInitial == false
                                && b.Releve.IsActive == true
                                && b.Qte > 0
                                && b.Releve.Nom.StartsWith(currentYear.ToString())
                                       select new { c.Distributeurs.FirstOrDefault().ID }).Contains(new { ID = d.ID })
                                group d by new { d.ID } into g
                                orderby g.Key.ID
                                select g.Key.ID).ToList();



            // update hasCumul in distributeurs list, The property 'hasCumul' isn't Mapped by EF Indentities
            distributeurVm.ToList()
                .ForEach(
                    d => d.hasCumul = distribCumul.Contains(d.ID));

            distributeurVm.ToList()
                .ForEach(c => { c.UserCount = db.Utilisateurs.Count(u => u.RefId == c.ID && u.RefRole == (int)RoleType.Distributeur); });

            var pageNumber = (page ?? 1);
            return View(distributeurVm.ToPagedList(pageNumber, pageSize));
        }

        // GET: Distributeurs/Details/5
        public ActionResult Details(int? id, int? page)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var distributeur = db.Distributeurs.Find(id);

            if (distributeur == null)
            {
                return HttpNotFound();
            }

            var distributeurVm = MapperConfig.mapper.Map<Distributeur, DistributeurViewModel>(distributeur);
            InitViewBag(distributeur);
            distributeurVm.UtilisateursVm = GetUtilisateursVm(id);

            var techName = (from t in db.Techniciens
                            join u in db.Utilisateurs on t.ID equals u.RefId
                            where t.ID == distributeur.RefTechBMS
                            select new { Name = u.Prenom + " " + u.Nom }).ToList();

            ViewBag.TechBMS = techName.Count > 0 ? techName.FirstOrDefault().Name : "Inconnu";
            ViewBag.ClientsCount = distributeur.Clients.Where(c => c.IsActive == true).Count();

            ViewBag.Page = (page ?? 1);

            return View(distributeurVm);
        }

        // GET: Distributeurs/Create
        public ActionResult Create(int? page)
        {
            var distributeur = new Distributeur
            {
                IDDatCreat = DateTime.Now,
                IDDatModif = DateTime.Now,
                IDUserCreat = User.UserId,
                IDUserModif = User.UserId
                // RefTechBMS = User.RefId
            };

            var distributeurVm = MapperConfig.mapper.Map<Distributeur, DistributeurViewModel>(distributeur);
            InitViewBag(distributeur);
            ViewBag.Page = (page ?? 1);

            return View(distributeurVm);

            //return View();
        }

        // POST: Distributeurs/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Distributeur distributeur, int? page)
        {
            if (ModelState.IsValid)
            {
                db.Distributeurs.Add(distributeur);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            InitViewBag();
            var distributeurVm = MapperConfig.mapper.Map<Distributeur, DistributeurViewModel>(distributeur);

            ViewBag.Page = (page ?? 1);
            return View(distributeurVm);
        }

        // GET: Distributeurs/Edit/5
        public ActionResult Edit(int? id, int? page)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var distributeur = db.Distributeurs.Find(id);

            if (distributeur == null)
            {
                return HttpNotFound();
            }

            ViewBag.Page = (page ?? 1);

            var distributeurVm = MapperConfig.mapper.Map<Distributeur, DistributeurViewModel>(distributeur);
            distributeurVm.Utilisateur = new Utilisateur();
            distributeurVm.UtilisateursVm = GetUtilisateursVm(id);
            InitViewBag(distributeur);
            distributeurVm.Old_Technicien = distributeur.RefTechBMS;

            return View(distributeurVm);
        }

        // POST: Distributeurs/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(DistributeurViewModel distributeurVm, int? page)
        {
            var distributeur = MapperConfig.mapper.Map<DistributeurViewModel, Distributeur>(distributeurVm);

            if (ModelState.IsValid)
            {
                distributeur.IDDatModif = DateTime.Now;
                distributeur.IDUserModif = User.UserId;
                db.Entry(distributeur).State = EntityState.Modified;
                db.SaveChanges();

                // update technicien BMS for all clients linked with this distributor
                if (distributeurVm.Old_Technicien != distributeur.RefTechBMS)
                {
                    db.Database.ExecuteSqlCommand(@"UPDATE dbo.Client SET RefTechBMS = @RefTechBMS WHERE RefTechBMS = @Old_Technicien",
                        new SqlParameter("@RefTechBMS", distributeur.RefTechBMS),
                        new SqlParameter("@Old_Technicien", distributeurVm.Old_Technicien));
                }

                if (User.IsAuthorized(new[] { RoleType.Pdg, RoleType.ResponsableZone, RoleType.Technicien }))
                {
                    return RedirectToAction("Index", new { page = (page ?? 1) });
                }
                else
                {
                    // redirect to home if  not authorised
                    RedirectToAction("Index", "Home");
                }
            }

            ViewBag.Page = (page ?? 1);
            distributeurVm.UtilisateursVm = GetUtilisateursVm(distributeur.ID);
            InitViewBag(distributeur);

            return View(distributeurVm);
        }

        // Utilisateur(s) du "Bilan Web" associé à ce distributeur
        private List<UtilisateurViewModel> GetUtilisateursVm(int? idDistributeur)
        {
            var list = (from u in db.Utilisateurs
                        join l in db.Langues on u.RefLangue equals l.ID
                        where l.IsActive && u.RefId == idDistributeur && u.RefRole == (int)RoleType.Distributeur && u.IsActive
                        select new
                        {
                            utilisateur = u,
                            LangueName = l.Nom
                        });
            list.ToList().ForEach(u => u.utilisateur.LangueName = u.LangueName);
            var utilisateurs = list.Select(u => u.utilisateur).ToList();
            var utilisateursVm = MapperConfig.mapper.Map<List<Utilisateur>, List<UtilisateurViewModel>>(utilisateurs);
            return utilisateursVm;
        }

        // Archive client from Details view
        [HttpPost]
        public JsonResult Archiver(int? id)
        {
            if (id != null)
            {
                var distributeur = db.Distributeurs.Find(id);
                distributeur.IsActive = false;
                db.Entry(distributeur).State = EntityState.Modified;

                if (db.SaveChanges() > 0)
                {
                    // desactiver utilisateur et clients
                    db.Database.ExecuteSqlCommand(@"UPDATE dbo.Utilisateur SET IsActive = 0 WHERE RefId = @IdDistributeur AND RefRole = @RefRole",
                        new SqlParameter("@IdDistributeur", id),
                        new SqlParameter("@RefRole", (int)RoleType.Distributeur));

                    // supprimer les associations clients/distributeur
                    db.Database.ExecuteSqlCommand(@"DELETE FROM dbo.Distributeur_Client WHERE RefDistributeur = @IdDistributeur",
                        new SqlParameter("@IdDistributeur", id));
                    return Json("success");
                }
            }
            return Json("Response Error");
        }

        // Initialize ViewBag properties
        private void InitViewBag(Distributeur distributeur = null)
        {
            ViewBag.CreateBy = "";
            ViewBag.ModifiedBy = "";
            //var IdTech = 0;

            if (distributeur != null && distributeur.ID != 0)
            {
                var users = (from d in db.Distributeurs
                             join c in db.Utilisateurs on d.IDUserCreat equals c.ID
                             join m in db.Utilisateurs on d.IDUserModif equals m.ID
                             where d.ID == distributeur.ID
                             select new
                             {
                                 CreateBy = c.Prenom + " " + c.Nom,
                                 ModifiedBy = m.Prenom + " " + m.Nom
                             }).ToList();
                if (users.Count > 0)
                {
                    ViewBag.CreateBy = users.First().CreateBy;
                    ViewBag.ModifiedBy = users.First().ModifiedBy;
                }

                // Number of client for this distributor !
                // var numClients = (from c in db.Distributeurs)
            }

            ViewBag.Pays = Repositories.GetCombo(db, "cmbPays", User.LanguageId);

            //var SuperieurHie = (IdTech.Count > 0) ? IdTech.First() : 0;

            var SuperieurHie = Repositories.GetSuperieur(db, User, distributeur?.RefTechBMS ?? 0);

            // set reference id for selected value
            //var SuperieurHie = User.IsAuthorized(RoleType.Distributeur) ? (IdTech > 0 ? IdTech : 0) : User.RefId;
            // get list technicien under responsable zone of RefTechBMS
            ViewBag.DDLTechList = Repositories.GetTechList(db, SuperieurHie, distributeur?.RefTechBMS ?? 0);
        }

        // Get List of technicien under ResponsableZone
        private int GetSuperieur(int id)
        {
            var IdTech = 0;
            if (User.IsAuthorized(RoleType.Distributeur))
            {
                IdTech = (from t in db.Techniciens
                          where t.ID == id
                          select t.RefRespZone).ToList().FirstOrDefault();
            }
            else
            {
                // pdg, responsable, technicien
                IdTech = User.RefId;
            }

            // if id tech = 0 set superieur to 1 (pdg)
            IdTech = (IdTech > 0) ? IdTech : (int)RoleType.Pdg;

            return IdTech;
        }

        // GET: Distributeurs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var distributeur = db.Distributeurs.Find(id);
            if (distributeur == null)
            {
                return HttpNotFound();
            }
            return View(distributeur);
        }

        // POST: Distributeurs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var distributeur = db.Distributeurs.Find(id);
            db.Distributeurs.Remove(distributeur);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}