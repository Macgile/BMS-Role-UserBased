﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using PagedList;
using SiteBMS.Models;
using SiteBMS.ViewModels;

namespace SiteBMS.Controllers
{
    public class RelevesController : BaseController
    {
        // groupement par type d'especes
        private static int[] Noyau = new[] { 1, 5, 8 };

        private static int[] Pepin = new[] { 6, 7 };
        private static int[] Vigne = new[] { 9, 10 };
        private static int[] Cereal = new[] { 12, 13 };
        private static int[] Aucune = new[] { 11 };

        // GET: Releves
        public ActionResult Index()
        {
            var releves = db.Releves.Include(r => r.Greffe).Include(r => r.Parcelle).ToList();
            var viewModel = MapperConfig.mapper.Map<List<Releve>, List<ReleveViewModel>>(releves);
            return View((IEnumerable<Releve>)viewModel);
        }

        // GET: Releves/List/5
        public ActionResult List(int? id, int? page)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var releves = db.Releves
                //.Include(r => r.Greffe)
                //.Include(r => r.Parcelle)
                //.Include(r => r.CarenceReleves)
                .Where(r => r.RefParcelle == id && r.IsInitial == false).ToList();

            if (releves.Count == 0)
            {
                var refClient = db.Parcelles.Find(id).RefClient;
                return RedirectToAction("List", "Parcelles", new { id = refClient, page = page });
            }

            // get available cumul for releves
            var releveCumul = (from b in db.Bilans
                               join r in db.Releves on b.RefReleve equals r.ID
                               where r.IsActive == true
                               && r.IsInitial == false
                               && b.Qte > 0
                               //&& b.IsValide == true
                               && r.RefParcelle == id
                               && r.Parcelle.IsActive == true
                               select b.Releve.ID).Distinct();

            var releveVm = MapperConfig.mapper.Map<List<Releve>, List<ReleveViewModel>>(releves);

            // update releve to update hasCumul
            releveVm.ToList()
                .ForEach(
                    r => r.hasCumul = releveCumul.ToList().Contains(r.ID));

            // first releve for infos
            ViewBag.Releve = releveVm.FirstOrDefault();

            ViewBag.DDLCarenceType = new SelectList((from ct in db.CarenceTypes
                                                     join t in db.Traductions on ct.Nom_T equals t.RefElement
                                                     where t.RefLangue == User.LanguageId
                                                     select new SelectListItem
                                                     {
                                                         Value = ct.ID.ToString(),
                                                         Text = t.Traduction1
                                                     }).ToList(), "Value", "Text");

            var pageNumber = (page ?? 1);
            return View(releveVm.ToPagedList(pageNumber, pageSize));
        }

        // GET: Releves/Details/5
        public ActionResult Details(int? id, int? page)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var releve = db.Releves.Find(id);
            if (releve == null)
            {
                return HttpNotFound();
            }

            // mapping to ViewModel
            var viewModel = MapperConfig.mapper.Map<Releve, ReleveViewModel>(releve);

            ViewBag.Page = (page ?? 1);

            // initialize all ViewBag DropDownList for it
            InitViewBag(releve);

            return View(viewModel);
        }

        // GET: Releves/Create
        [Route("Create/{id}/{page?}/{origin?}")]
        public ActionResult Create(int? id, int? page, int? origin)
        {
            var idParcelle = (id ?? 0);

            // TODO : Creer les DDL avec parametres !!!
            // get last releve
            var oldReleve = db.Releves.Where(r => r.RefParcelle == idParcelle && r.IsInitial == false).OrderByDescending(r => r.DatCreat);

            var releve = new Releve();

            // if laste releve, copy currentvalue in new releve and detach
            if (oldReleve.Count() > 0)
            {
                db.Releves.Add(releve);
                db.Entry(releve).CurrentValues.SetValues(oldReleve.First());
                db.Entry(releve).State = EntityState.Detached;
                releve.ID = 0;
            }

            // set new values

            releve.RefParcelle = idParcelle;
            releve.Parcelle = db.Parcelles.Find(idParcelle);
            //releve.IsActive = true;

            switch (User.RoleName)
            {
                case "Distributeur": releve.RefComboTypeUser = 191; break;
                case "Client": releve.RefComboTypeUser = 190; break;
                //case "Technicien": releve.RefComboTypeUser = 192; break; // supprimmer de la base
                // 109 PAR DEFAULT POUR LE PDG, RESPONSABLE, TECHNICIEN
                default: releve.RefComboTypeUser = 109; break;
            }

            releve.DatCreat = DateTime.Now;
            releve.DatModif = DateTime.Now;
            releve.IDUserCreat = User.UserId;
            releve.IDUserModif = User.UserId;
            releve.DateReleve = DateTime.Now;
            ViewBag.Page = (page ?? 1);

            InitViewBag(releve);

            // origin controller : 1 == parcelles, 2 == releves
            origin = (origin ?? 1);

            // for cancel button
            ViewBag.Controller = origin; //  (origin == 1) ? "Parcelles" : "Releves";

            var viewModel = MapperConfig.mapper.Map<Releve, ReleveViewModel>(releve);

            return View(viewModel);
        }

        // POST: Releves/Create
        [HttpPost]
        [Route("Create/{id}/{page?}/{origin?}")]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ReleveViewModel releveVM, int? page, int? origin)
        {
            // TODO : Sauvegarder le relevé isInitial
            releveVM.Parcelle = db.Parcelles.Find(releveVM.RefParcelle);
            var releve = MapperConfig.mapper.Map<ReleveViewModel, Releve>(releveVM);

            page = (page ?? 1);

            // origin controller : 1 == parcelles, 2 == releves
            origin = (origin ?? 2); // releves by default

            if (ModelState.IsValid)
            {
                db.Releves.Add(releve);
                db.SaveChanges();

                // redirect to  List releves Controler : id = refParcelle
                if (origin == 2)
                {
                    return RedirectToAction("List", new { id = releve.RefParcelle, page = page });
                }
                // redirect to  List Parcelles Controler and id = refClient
                return RedirectToAction("List", "Parcelles", new { id = releve.Parcelle.RefClient, page = page });
            }
            else
            {
                Repositories.ShowStateErrors(ModelState);
            }

            // for redirect
            ViewBag.Controller = origin;
            ViewBag.Page = page;
            InitViewBag(releve);

            return View(releveVM);
        }

        // GET: Releves/Edit/5
        public ActionResult Edit(int? id, int? page, int? origin)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var releve = db.Releves.Find(id);

            if (releve == null)
            {
                return HttpNotFound();
            }

            // mapping to ViewModel
            var viewModel = MapperConfig.mapper.Map<Releve, ReleveViewModel>(releve);

            ViewBag.Page = (page ?? 1);
            ViewBag.Controller = (origin ?? 1);

            // initialize all ViewBag DropDownList for it
            InitViewBag(releve);
            return View(viewModel);
        }

        // POST: Releves/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult OLDEdit(ReleveViewModel viewModel, int? page, int? origin)
        {
            var releve = MapperConfig.mapper.Map<ReleveViewModel, Releve>(viewModel);
            releve.Parcelle = db.Parcelles.Find(releve.RefParcelle);

            //releve.Greffe = db.Greffes.Where(g=>g.RefEspece == releve.RefComboGreffe)

            if (ModelState.IsValid)
            {
                releve.IsPerso = false; // set Perso to false
                db.Entry(releve).State = EntityState.Modified;
                db.SaveChanges();

                // redirection list des relevés pour la parcelle
                return RedirectToAction("List", new { id = releve.RefParcelle, page = (page ?? 1) });
            }
            else
            {
                Repositories.ShowStateErrors(ModelState);
            }

            ViewBag.Page = (page ?? 1);
            ViewBag.Controller = (origin ?? 1);

            InitViewBag(releve);
            viewModel = MapperConfig.mapper.Map<Releve, ReleveViewModel>(releve);
            return View(viewModel);
        }

        // POST: Releves/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Releve releve, int? page, int? origin)
        {
            releve.Parcelle = db.Parcelles.Find(releve.RefParcelle);

            if (ModelState.IsValid)
            {
                releve.IsPerso = false; // set Perso to false
                db.Entry(releve).State = EntityState.Modified;
                db.SaveChanges();

                // redirection list des relevés pour la parcelle
                return RedirectToAction("List", new { id = releve.RefParcelle, page = (page ?? 1) });
            }
            else
            {
                Repositories.ShowStateErrors(ModelState);
            }

            ViewBag.Page = (page ?? 1);
            ViewBag.Controller = (origin ?? 1);

            InitViewBag(releve);
            var viewModel = MapperConfig.mapper.Map<Releve, ReleveViewModel>(releve);
            return View(viewModel);
        }

        [HttpPost]
        public JsonResult ChangeRecolteYear(int id, int year)
        {
            if (id > 0 && year > 0)
            {
                var listNum = new List<int>();

                var countCurrentYear = db.Releves.Count(r => r.RefParcelle == id && r.IsInitial == false 
                && r.IsActive == true && r.Nom.StartsWith(year.ToString()));

                //find how many Num in current Year and where it begins
                foreach (int num in Enumerable.Range((countCurrentYear + 1), (10 - countCurrentYear)))
                {
                    listNum.Add(num);
                }
                return Json(listNum, JsonRequestBehavior.AllowGet);
            }
            return Json("Response Error");
        }

        // Initialize ViewBag properties
        private void InitViewBag(Releve releve)
        {
            // ViewBag properties values

            // disable client validation (jqueryval)
            //HtmlHelper.ClientValidationEnabled = false;

            ViewBag.coefN = 0.0;
            ViewBag.coefP = 0.0;
            ViewBag.coefK = 0.0;

            // COMMON LISTS WHITOUT PARAMETERS
            try
            {
                // get coeficients for calculate fumure preview
                // create javascript variable for each coeficients
                var coeficients = db.CoefFumures.Where(c => c.RefEspece == releve.Parcelle.RefComboEspece).ToList();

                if (coeficients.Count > 1)
                {
                    ViewBag.coefK = coeficients[0].Coef;
                    ViewBag.coefN = coeficients[1].Coef;
                    ViewBag.coefP = coeficients[2].Coef;
                }

                // listes des récolte (relevés) disponible
                ViewBag.DDLRecolte = Repositories.GetRecolte(db, releve.RefParcelle);
                ViewBag.DDLRecolteYear = Repositories.GetRecolteYears(db, releve.RefParcelle);
                ViewBag.DDLRecolteNum = Repositories.GetRecolteNum(db, releve.RefParcelle, DateTime.Now.Year);

                // GetCombo => no parameters
                ViewBag.DDLTypeUser = Repositories.GetCombo(db, "cmbTypeUser", User.LanguageId); // user role selected by default
                ViewBag.DDLRefComboVigueurVisuelle = Repositories.GetCombo(db, "cmbVigueurVisuelle", User.LanguageId);
                ViewBag.DDLAge = Repositories.GetCombo(db, "cmbAge", User.LanguageId);
                ViewBag.DDLStadeConservation = Repositories.GetCombo(db, "cmbStadeConservation", User.LanguageId);

                // cmbDysfonctionnement    : ddlMillerandage: RefComboMillerandage, ddlDessechement: RefComboRafle
                // cmbDysfonctionnementFem : ddlCoulure: RefComboCoulure, ddlChlorose: RefComboChlorose, ddlPhyto: RefComboSanitaire

                var disfonctionnement = Repositories.GetCombo(db, "cmbDysfonctionnement", User.LanguageId);
                var disfonctionnementFm = Repositories.GetCombo(db, "cmbDysfonctionnementFem", User.LanguageId);

                // disfonctionnement
                ViewBag.DDLMillerandage = disfonctionnement;
                ViewBag.DDLDessechement = disfonctionnement;

                // disfonctionnementFm
                ViewBag.DDLCoulure = disfonctionnementFm;
                ViewBag.DDLChlorose = disfonctionnementFm;
                ViewBag.DDLPhyto = disfonctionnementFm;

                //GetComboNumeric => no parameters
                ViewBag.DDLObjectifRendement = Repositories.GetComboNumeric(db, "cmbTonnagePomPoir");
                ViewBag.DDLObjectifRendementN_1 = Repositories.GetComboNumeric(db, "cmbTonnagePomPoir");
                ViewBag.DDLObjectifCalibre = Repositories.GetComboNumeric(db, "cmbCalibrePomPoir");
                ViewBag.DDLObjectifCalibreN_1 = Repositories.GetComboNumeric(db, "cmbCalibrePomPoir");
                ViewBag.DDLMatiereOrganique = Repositories.GetComboNumeric(db, "cmbMO");

                // only noyau
                if (Noyau.Contains(releve.Parcelle.RefComboEspece))
                {
                    ViewBag.DDLNbRang = Repositories.GetLargeurRang();
                    ViewBag.DDLHauteurCanope = Repositories.GetHauteurCanopee(db);
                    ViewBag.DDLBacteriose = Repositories.GetComboEspece(db, "cmbBacteriose", releve.Parcelle.RefComboEspece,
                    User.LanguageId);
                    ViewBag.DDLSymptome = Repositories.GetComboEspece(db, "cmbTypeConservation", releve.Parcelle.RefComboEspece,
                    User.LanguageId);

                    // tauxNouaison = nbr fruits / nbr fleur * 100
                    var HauteurCanope = releve.HauteurCanope ?? 0;
                    var NbRang = releve.NbRang ?? 0;
                    var tauxNouaison = (HauteurCanope > 0 && NbRang > 0) ? Math.Round((HauteurCanope / NbRang) * 100, 2) : 0;
                    ViewBag.InitialTauxNouaison = tauxNouaison.ToString();
                }
                else
                {
                    ViewBag.DDLNbRang = Repositories.GetComboNumeric(db, "cmbFoliaire");
                    ViewBag.DDLHauteurCanope = Repositories.GetComboNumeric(db, "cmbFoliaire");
                }

                // not pepin
                if (!Pepin.Contains(releve.Parcelle.RefComboEspece))
                {
                    ViewBag.DDLCalcaire = Repositories.GetCalcaire();
                }

                if (Vigne.Contains(releve.Parcelle.RefComboEspece))
                {
                    // cmbTypeVin
                    ViewBag.DDLAppelation = Repositories.GetComboEspece(db, "cmbTypeVin", releve.Parcelle.RefComboEspece,
                    User.LanguageId);
                    ViewBag.DDLDegres = Repositories.GetDegres();
                }

                ViewBag.DDLPluviometrie = Repositories.GetComboNumeric(db, "cmbPluvio");

                // fumure => no parameters
                ViewBag.DDLFumureK = Repositories.GetFumureKpn(db);
                ViewBag.DDLFumureP = Repositories.GetFumureKpn(db);
                ViewBag.DDLFumureN = Repositories.GetFumureKpn(db);

                // COMMON LISTS WITH PARAMETERS

                //TODO : Isoler ces viewBag dans les actions

                // GetUtilisateur => releve parameters
                ViewBag.CreateBy = Repositories.GetUtilisateur(db, releve.IDUserCreat);
                ViewBag.ModifiedBy = Repositories.GetUtilisateur(db, releve.IDUserModif);

                // Parcelle => releve.Parcelle parameters
                ViewBag.DDLComboGreffe = Repositories.GetGreffe(db, releve.Parcelle.RefComboEspece, User.LanguageId);

                ViewBag.ZoneClimatique = Repositories.GetCombo(db, "cmbZoneClimatique", User.LanguageId)
                    .FirstOrDefault(x => x.Value == releve.Parcelle.RefComboZoneClimatique.ToString()).Text;

                ViewBag.Variete = Repositories.GetVariete(db, releve.Parcelle.RefComboEspece, User.LanguageId)
                    .FirstOrDefault(x => x.Value == releve.Parcelle.RefVariete.ToString()).Text;

                ViewBag.DDLTaille = Repositories.GetComboEspece(db, "cmbTypeTaille", releve.Parcelle.RefComboEspece,
                    User.LanguageId);
                ViewBag.DDLConservation = Repositories.GetComboEspece(db, "cmbTypeConservation",
                    releve.Parcelle.RefComboEspece, User.LanguageId);

                ViewBag.Espece = Repositories.GetEspece(db, releve.Parcelle.RefComboEspece, User.LanguageId);

                var users = (from c in db.Utilisateurs
                             from m in db.Utilisateurs
                             where c.ID == releve.IDUserCreat && m.ID == releve.IDUserModif
                             select new
                             {
                                 CreateBy = c.Prenom + " " + c.Nom,
                                 ModifiedBy = m.Prenom + " " + m.Nom
                             }).ToList().FirstOrDefault();

                if (users != null)
                {
                    ViewBag.CreateBy = users.CreateBy;
                    ViewBag.ModifiedBy = users.ModifiedBy;
                }
                else
                {
                    ViewBag.CreateBy = "";
                    ViewBag.ModifiedBy = "";
                }
            }
            catch (Exception ex)
            {
                // throw;
                Debug.WriteLine(ex.Message);
            }
        }

        // GET: Releves/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var releve = db.Releves.Find(id);
            if (releve == null)
            {
                return HttpNotFound();
            }
            return View(releve);
        }

        // POST: Releves/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var releve = db.Releves.Find(id);
            db.Releves.Remove(releve);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}