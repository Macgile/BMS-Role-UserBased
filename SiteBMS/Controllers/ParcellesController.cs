﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using PagedList;
using SiteBMS.Models;
using SiteBMS.ViewModels;

namespace SiteBMS.Controllers
{
    public class ParcellesController : BaseController
    {
        // GET: Parcelles
        public ActionResult Index()
        {
            return View(db.Parcelles.ToList());
        }

        // GET: Parcelles id : id client || ID Utilisateur
        public ActionResult List(int? id, int? page)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var parcelles = db.Parcelles.Where(p => !especes.Contains(p.RefComboEspece) && p.RefClient == id && p.IsActive == true).ToList();

            //var par = (from p in db.Parcelles
            //           join e in (from esp in db.Especes
            //                      join t in db.Traductions on esp.Nom_T equals t.RefElement
            //                      where t.RefLangue == User.LanguageId
            //                      select new { Traduction1 = t.Traduction1, RefComboEspece = esp.ID }) on p.RefComboEspece equals e.RefComboEspece into join_e
            //           from e in join_e.DefaultIfEmpty()
            //           where !especes.Contains(p.RefComboEspece) && p.RefClient == id
            //           select p);

            //var parcelles = par.ToList();

            // .Where(p => p.RefClient == id).ToList();

            // id client ou id utilisateur

            //var parcelles = db.Parcelles.Where(p => !especes.Contains(p.RefComboEspece))
            //    .Where(
            //    p=> p.RefClient == id || p.RefClient == db.Utilisateurs
            //    .Where(u => u.ID == id && u.RefRole == (int)RoleType.Client).Select(u => u.RefId).FirstOrDefault()
            //    ).ToList();

            // mimic a IN SQL CLAUSE
            //var parcelles = db.Parcelles
            //    .Where(p => p.RefClient == id
            //  // TODO : Array Espece
            //  && !especes.Contains(p.RefComboEspece)).ToList();

            var parcelleVm = MapperConfig.mapper.Map<List<Parcelle>, List<ParcelleViewModel>>(parcelles);

            foreach (var parcelle in parcelleVm)
            {
                var espece = (from e in db.Especes
                              join t in db.Traductions on e.Nom_T equals t.RefElement
                              where e.ID == parcelle.RefComboEspece
                              select t.Traduction1).FirstOrDefault();

                parcelle.NomEspece = (espece ?? "Unknow");
            }

            // selectionne toutes les parcelles du client qui on un releve et un bilan
            var parcelleCumul = (from Bilan in db.Bilans
                                 where (from P in db.Parcelles
                                            // toutes les ID parcelles contenu dans bilan->releve->parcelle>ID
                                        select new { P.ID }).Contains(new { ID = Bilan.Releve.Parcelle.ID })
                                        && Bilan.Releve.IsActive == true
                                        && Bilan.Releve.IsInitial == false
                                        && Bilan.Releve.Parcelle.IsActive == true
                                        && Bilan.Releve.Parcelle.Client.IsActive == true
                                       // && Bilan.Releve.Nom.StartsWith(currentYear.ToString())
                                        && Bilan.Qte >0
                                        //&& Bilan.IsValide == true
                                 group Bilan.Releve.Parcelle by new { Bilan.Releve.Parcelle.ID } into g
                                 orderby g.Key.ID
                                 select g.Key.ID);

            //SELECT *            FROM   Parcelle P
            //       JOIN
            //(
            //    SELECT DISTINCT
            //           R.RefParcelle
            //    FROM   Bilan B
            //           JOIN Releve R ON B.RefReleve = R.ID
            //) AS BI ON P.ID = BI.RefParcelle
            //WHERE P.RefClient = 162;
            //join br in (from b in db.Bilans
            //            join r in db.Releves on b.RefReleve equals r.ID
            //            select r.RefParcelle).ToList<int>() on p.ID equals br.
            //                        );

            //var test = (from p in db.Parcelles
            //            where (from b in db.Bilans.Where(b => b.IsValide == true && b.Qte != null)
            //                   join r in db.Releves.Where(r => r.IsActive == true && r.IsInitial == true)
            //                   on b.RefReleve equals r.ID select new { r.RefParcelle }).Contains(new { ID = p.ID }));

            //var test = (from b in db.Bilans.Where(b => b.IsValide == true && b.Qte != null)
            //            join r in db.Releves.Where(r => r.IsActive == true && r.IsInitial == true) on b.RefReleve equals r.ID
            //            join p in db.Parcelles.Where(p => p.RefClient == id) on r.RefParcelle equals p.ID
            //            select p.ID);

            parcelleVm.ToList()
                .ForEach(
                    p => p.hasCumul = parcelleCumul.Contains(p.ID));

            var client = db.Clients.Find(id);
            ViewBag.Client = client;
            ViewBag.Page = (page ?? 1);

            var pageCount = (Math.Ceiling(((decimal)parcelles.Count) / pageSize) < (page ?? 1)) ? 1 : (page ?? 1);
            var pageNumber = pageCount;

            return View(parcelleVm.ToPagedList(pageNumber, pageSize));
        }

        // GET: Parcelles/Details/5
        public ActionResult Details(int? id, int? page)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var parcelle = db.Parcelles.Find(id);

            if (parcelle == null)
            {
                return HttpNotFound();
            }

            ViewBag.Page = (page ?? 1);

            InitViewBag(parcelle);

            var parcelleVm = MapperConfig.mapper.Map<Parcelle, ParcelleViewModel>(parcelle);

            return View(parcelleVm);
        }

        // GET: Parcelles/Create : id == id client
        public ActionResult Create(int? id, int? page)
        {
            var idClient = id ?? 0;
            var parcelle = new Parcelle();
            var origin = "Parcelles"; // parcelle:1, clients:1

            if (Request.UrlReferrer != null)
            {
                var fullUrl = Request.UrlReferrer;
                var segments = fullUrl.AbsolutePath.Split('/');
                origin = segments.Contains("Parcelles") ? "Parcelles" : "Clients";
            }

            var client = db.Clients.Find(idClient);
            parcelle.Client = client;

            ViewBag.ClientFullName = client.FullName;

            // initialize ViewBag for it
            InitViewBag(parcelle);

            ViewBag.ClientID = idClient;
            parcelle.DatCreat = DateTime.Now;
            parcelle.DatModif = DateTime.Now;
            parcelle.IDUserCreat = User.UserId;
            parcelle.IDUserModif = User.UserId;
            parcelle.RefClient = idClient;
            ViewBag.Page = (page ?? 1);
            ViewBag.Origin = origin;

            var parcelleVm = MapperConfig.mapper.Map<Parcelle, ParcelleViewModel>(parcelle);

            return View(parcelleVm);
        }

        // POST: Parcelles/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Parcelle parcelle, int? page)
        {
            page = (page ?? 1);

            if (ModelState.IsValid)
            {
                db.Parcelles.Add(parcelle);
                db.SaveChanges();
                return RedirectToAction("List", new { id = parcelle.RefClient, page });
            }

            ViewBag.Page = page;
            InitViewBag(parcelle);

            var parcelleVm = MapperConfig.mapper.Map<Parcelle, ParcelleViewModel>(parcelle);
            return View(parcelleVm);
        }

        // GET: Parcelles/Edit/5
        public ActionResult Edit(int? id, int? page)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var parcelle = db.Parcelles.Find(id);

            if (parcelle == null)
            {
                return HttpNotFound();
            }

            ViewBag.Page = (page ?? 1);
            InitViewBag(parcelle);

            var parcelleVm = MapperConfig.mapper.Map<Parcelle, ParcelleViewModel>(parcelle);

            return View(parcelleVm);
        }

        // POST: Parcelles/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Parcelle parcelle, int? page)
        {
            page = (page ?? 1);

            if (ModelState.IsValid)
            {
                db.Entry(parcelle).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("List", new { id = parcelle.RefClient, page });
            }

            InitViewBag(parcelle);
            ViewBag.Page = page;
            var parcelleVm = MapperConfig.mapper.Map<Parcelle, ParcelleViewModel>(parcelle);
            return View(parcelleVm);
        }

        // Initialize ViewBag properties
        private void InitViewBag(Parcelle parcelle)
        {
            var refComboEspece = 0;
            //var RefVariete = string.Empty;
            //var RefComboZoneClimatique = string.Empty;
            //var RefComboTypeIrrigation = string.Empty;
            //var RefComboModeProduction = string.Empty;

            if (parcelle != null)
            {
                refComboEspece = parcelle.RefComboEspece;
                //RefVariete = parcelle.RefVariete.ToString();
                //RefComboZoneClimatique = parcelle.RefComboZoneClimatique.ToString();
                //RefComboTypeIrrigation = parcelle.RefComboTypeIrrigation.ToString();
                //RefComboModeProduction = parcelle.RefComboModeProduction.ToString();
            }

            ViewBag.DDLEspece = Repositories.GetEspeces(db, User.LanguageId);
            ViewBag.DDLVariete = Repositories.GetVariete(db, refComboEspece, User.LanguageId);
            ViewBag.DDLZoneClimatique = Repositories.GetCombo(db, "cmbZoneClimatique", User.LanguageId);
            ViewBag.DDLIrrigation = Repositories.GetCombo(db, "cmbTypeIrrigation", User.LanguageId);
            ViewBag.DDLModeProduction = Repositories.GetCombo(db, "cmbTypeModeProduction", User.LanguageId);
        }

        // GET VARIETE LIST FROM AJAX (See Create Parcelle)
        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult LoadVarietes(string especeId)
        {
            var idEspece = Convert.ToInt32(especeId);
            var variete = Repositories.GetVariete(db, idEspece, User.LanguageId);
            return Json(variete, JsonRequestBehavior.AllowGet);
        }

        // GET: Parcelles/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var parcelle = db.Parcelles.Find(id);
            if (parcelle == null)
            {
                return HttpNotFound();
            }
            return View(parcelle);
        }

        // POST: Parcelles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var parcelle = db.Parcelles.Find(id);
            db.Parcelles.Remove(parcelle);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}