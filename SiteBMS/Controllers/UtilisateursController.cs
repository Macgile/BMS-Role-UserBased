﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Newtonsoft.Json;
using PagedList;
using SiteBMS.DAL.Security;
using SiteBMS.Models;
using SiteBMS.ViewModels;

namespace SiteBMS.Controllers
{
    public class UtilisateursController : BaseController
    {
        private readonly int[] roles = new[] { (int)RoleType.Pdg, (int)RoleType.ResponsableZone, (int)RoleType.Technicien };

        // GET: Utilisateurs
        public ActionResult Index(string sortOrder, string currentFilter, int? page, string searchString, int? refLangue)
        {
            ViewBag.NameSortParm = string.IsNullOrEmpty(sortOrder) ? "SortDesc" : ""; // name_desc
            ViewBag.SortingClass = "SortDesc";
            ViewBag.PaysSortParm = sortOrder == "Pays" ? "PaysDesc" : "Pays";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;
            ViewBag.RefLangue = refLangue;
            ViewBag.CurrentSort = sortOrder;

            var utilisateurs = (from t in db.Techniciens
                                join r in db.Techniciens on t.RefRespZone equals r.ID
                                join u in db.Utilisateurs.Where(ro => roles.Contains(ro.RefRole)) on t.ID equals u.RefId
                                where t.RefRespZone == User.RefId
                                || r.RefRespZone == User.RefId
                                || u.RefId == User.RefId
                                || User.RefId == (int)RoleType.Pdg
                                orderby u.Nom
                                select u).ToList();

            if (refLangue != null)
            {
                utilisateurs = utilisateurs.Where(s => s.RefLangue == refLangue).ToList();
            }

            if (!string.IsNullOrEmpty(searchString))
            {
                utilisateurs = utilisateurs.Where(s => s.Nom.StartsWith(searchString,StringComparison.InvariantCultureIgnoreCase)).ToList();
            }

            var utilisateurVm = MapperConfig.mapper.Map<List<Utilisateur>, List<UtilisateurViewModel>>(utilisateurs);

            var langues = new SelectList((from l in db.Langues
                                          join t in db.Traductions on l.Nom_T equals t.RefElement
                                          where l.IsActive && t.RefLangue == User.LanguageId
                                          select new { l.ID, Name = t.Traduction1 }), "ID", "Name", refLangue);

            ViewBag.DDLLangues = langues;

            utilisateurVm.ToList()
                .ForEach(u => u.LangueName = langues.Where(l => l.Value == u.RefLangue.ToString()).Select(l => l.Text).FirstOrDefault());

            // Sort by sortOrder parameter if set
            switch (sortOrder)
            {
                case "SortDesc":
                    utilisateurVm = utilisateurVm.OrderByDescending(s => s.Nom).ToList();
                    break;

                case "PaysDesc":
                    utilisateurVm = utilisateurVm.OrderByDescending(s => s.RefLangue).ToList();
                    break;

                case "Pays":
                    utilisateurVm = utilisateurVm.OrderBy(s => s.RefLangue).ToList();
                    break;
            }

            var pageNumber = (page ?? 1);
            return View(utilisateurVm.ToPagedList(pageNumber, pageSize));
        }

        // GET: Utilisateurs/Details/5
        public ActionResult Details(int? id, int? page)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var utilisateur = db.Utilisateurs.Find(id);
            if (utilisateur == null)
            {
                return HttpNotFound();
            }
            var utilisateurVm = MapperConfig.mapper.Map<Utilisateur, UtilisateurViewModel>(utilisateur);
            InitViewBag(utilisateurVm);
            ViewBag.Page = (page ?? 1);

            return View(utilisateurVm);
        }

        // GET: Utilisateurs/Edit/5
        public ActionResult Edit(int? id, int? page)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var utilisateur = db.Utilisateurs.Find(id);
            if (utilisateur == null)
            {
                return HttpNotFound();
            }

            var utilisateurVm = MapperConfig.mapper.Map<Utilisateur, UtilisateurViewModel>(utilisateur);
            InitViewBag(utilisateurVm);
            ViewBag.Page = (page ?? 1);
            return View(utilisateurVm);
        }

        // POST: Utilisateurs/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(UtilisateurViewModel utilisateurVm)
        {
            if (ModelState.IsValid)
            {
                var utilisateur = MapperConfig.mapper.Map<UtilisateurViewModel, Utilisateur>(utilisateurVm);
                db.Entry(utilisateur).State = EntityState.Modified;

                // change PaysId and LanguageId inside actual cookie for impact UI language immediately
                if (User.LanguageId != utilisateur.RefLangue && utilisateur.ID == User.UserId )
                {
                    // convert language to contry ID
                    var actualUserPays = Repositories.ConvertLangueToPays(db, utilisateur?.RefLangue ?? 1);

                    User.PayId = actualUserPays;
                    User.LanguageId = utilisateur?.RefLangue ?? 1;

                    var authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
                    if (authCookie != null)
                    {
                        var authTicket = FormsAuthentication.Decrypt(authCookie.Value);

                        if (authTicket != null)
                        {
                            var userData = JsonConvert.DeserializeObject<CustomPrincipalSerializeModel>(authTicket.UserData);

                            userData.LanguageId = User.LanguageId;
                            userData.PayId = User.PayId;
                            var newUserData = JsonConvert.SerializeObject(userData);

                            var newAuthTicket = new FormsAuthenticationTicket(
                                1,
                                authTicket.Name,
                                DateTime.Now,
                                DateTime.Now.AddMinutes(30),
                                authTicket.IsPersistent,
                                newUserData);

                            var encTicket = FormsAuthentication.Encrypt(newAuthTicket);
                            var faCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encTicket);
                            Response.Cookies.Add(faCookie);
                        }
                    }
                }

                db.SaveChanges();

                if (utilisateurVm.Technicien != null && roles.Contains(utilisateurVm.RefRole))
                {
                    var technicien = MapperConfig.mapper.Map<TechnicienViewModel, Technicien>(utilisateurVm.Technicien);
                    db.Entry(technicien).State = EntityState.Modified;
                    db.SaveChanges();
                }

                // redirection by Role
                if (User.IsAuthorized(new[] { RoleType.Pdg, RoleType.ResponsableZone }))
                {
                    return RedirectToAction("Index");
                }
                return RedirectToAction("Index", "Home");
            }

            InitViewBag(utilisateurVm);
            return View(utilisateurVm);
        }

        // GET: Utilisateurs/Create
        public ActionResult Create()
        {
            var utilisateurVm = new UtilisateurViewModel
            {
                PassWord = "",
                Technicien = new TechnicienViewModel(),
                RefId = 0,
                RefRole = 0
            };
            InitViewBag(utilisateurVm);

            return View(utilisateurVm);
        }

        // POST: Utilisateurs/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(UtilisateurViewModel utilisateurVm)
        {
            if (ModelState.IsValid)
            {
                if (utilisateurVm.Technicien != null && roles.Contains(utilisateurVm.RefRole))
                {
                    var technicien = MapperConfig.mapper.Map<TechnicienViewModel, Technicien>(utilisateurVm.Technicien);
                    db.Techniciens.Add(technicien);
                    db.SaveChanges();
                    utilisateurVm.RefId = technicien.ID;
                }

                var utilisateur = MapperConfig.mapper.Map<UtilisateurViewModel, Utilisateur>(utilisateurVm);
                db.Utilisateurs.Add(utilisateur);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            InitViewBag(utilisateurVm);
            return View(utilisateurVm);
        }

        private void InitViewBag(UtilisateurViewModel utilisateur)
        {
            ViewBag.DLLRefId = new SelectList((from t in db.Techniciens
                                               join u in db.Utilisateurs on t.ID equals u.RefId
                                               where u.RefRole == (int)RoleType.Technicien
                                               orderby u.Nom
                                               select new
                                               {
                                                   t.ID,
                                                   Name = u.Nom + " " + u.Prenom
                                               }).ToList(), "ID", "Name", utilisateur?.RefId ?? 0);

            ViewBag.DLLanguages = new SelectList((from l in db.Langues
                                                  join t in db.Traductions on l.Nom_T equals t.RefElement
                                                  where l.IsActive && t.RefLangue == User.LanguageId
                                                  orderby t.Traduction1
                                                  select new
                                                  {
                                                      l.ID,
                                                      Langue = t.Traduction1
                                                  }).ToList(), "ID", "Langue", utilisateur?.RefLangue ?? 0);

            // only : pdg, responsable, tech
            ViewBag.DDLRefRole = new SelectList(db.RoleUtilisateurs
                .Where(r => new[] { (int)RoleType.Pdg, (int)RoleType.ResponsableZone, (int)RoleType.Technicien }
                .Contains(r.Id))
                .Select(r => r),
                "Id", "Name", utilisateur?.RefRole ?? 0);

            var refrole = new SelectList(
                (from r in db.RoleUtilisateurs.ToList()
                                          join t in db.Traductions on r.Nom_T equals t.RefElement
                                          where t.RefLangue == User.LanguageId
                                          && (new[] { (int)RoleType.Pdg, (int)RoleType.ResponsableZone, (int)RoleType.Technicien }).Contains(r.Id)
                                          select new RoleUtilisateur
                                          {
                                              Id = r.Id,
                                              Name = t.Traduction1,
                                              Nom_T = r.Nom_T
                                          }).ToList(), "Id", "Name", utilisateur?.RefRole ?? 0);

            var rolesTranslated = (from r in db.RoleUtilisateurs.ToList()
                                   join t in db.Traductions on r.Nom_T equals t.RefElement
                                   where t.RefLangue == User.LanguageId
                                   select new RoleUtilisateur
                                   {
                                       Id = r.Id,
                                       Name = t.Traduction1,
                                       Nom_T = r.Nom_T
                                   }).ToList();

            ViewBag.DLLRoleTranslate = new SelectList(rolesTranslated, "Id", "Name");

            //if (utilisateur == null || utilisateur.RefRole != (int)RoleType.Technicien) return;
            // var technicien = new

            var technicienVm = new TechnicienViewModel();

            if (User.IsAuthorized(new[] { RoleType.Pdg, RoleType.ResponsableZone, RoleType.Technicien }))
            {
                var technicien = db.Techniciens.Find(utilisateur?.RefId ?? 0) ?? new Technicien();

                technicienVm = MapperConfig.mapper.Map<Technicien, TechnicienViewModel>(technicien);

                var responsable = (from u in db.Utilisateurs
                                   where u.RefRole <= (int)RoleType.ResponsableZone
                                   && u.IsActive == true
                                   orderby u.Nom
                                   select new
                                   {
                                       Value = u.RefId,
                                       Text = u.Nom + " " + u.Prenom
                                   }).Distinct();

                ViewBag.DLLResponsables = new SelectList(responsable.ToList(), "Value", "Text", technicien?.RefRespZone ?? 0);
                ViewBag.DDLPays = Repositories.GetCombo(db, "cmbPays", User.LanguageId, technicien?.RefComboPays ?? 0).OrderBy(d => d.Text);
            }

            if (utilisateur != null) utilisateur.Technicien = technicienVm;
        }

        // POST : GetUtilisateur, Id : ID utilisateur, origin : origine de la demande (distributeur/client)
        [HttpGet]
        public ActionResult GetUtilisateur(int id, string origin)
        {
            var utilisateur = db.Utilisateurs.Find(id);
            var utilisateurVm = MapperConfig.mapper.Map<Utilisateur, UtilisateurViewModel>(utilisateur);
            utilisateurVm.Origin = origin;
            InitViewBag(utilisateurVm);

            return PartialView("_Edit", utilisateurVm);
        }

        // add utilisateur from distributeur/client
        [HttpGet]
        public ActionResult AddUtilisateur(int refId, string origin)
        {
            var utilisateurVm = new UtilisateurViewModel
            {
                ID = 0,
                Login = "",
                PassWord = "",
                RefLangue = User.LanguageId, /* default language choice*/
                RefId = refId,
                IsActive = true,
                Origin = origin,
                RefRole = (origin == RoleType.Client.ToString()) ? (int)RoleType.Client : (int)RoleType.Distributeur
            };
            InitViewBag(utilisateurVm);
            return PartialView("_Edit", utilisateurVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateUtilisateur(UtilisateurViewModel utilisateurVm)
        {
            if (ModelState.IsValid)
            {
                var utilisateur = MapperConfig.mapper.Map<UtilisateurViewModel, Utilisateur>(utilisateurVm);

                if (utilisateur.ID != 0)
                {
                    // UPDATE
                    db.Entry(utilisateur).State = EntityState.Modified;
                }
                else
                {
                    // ADD NEW Utilisateur
                    db.Utilisateurs.Add(utilisateur);
                }

                db.SaveChanges();

                if (utilisateurVm.Origin == RoleType.Distributeur.ToString())
                {
                    // utilisateur.RefId = distributeur ID
                    return RedirectToAction("Edit", "Distributeurs", new { id = utilisateur.RefId });
                }

                if (utilisateurVm.Origin == RoleType.Client.ToString())
                {
                    // utilisateur.RefId = distributeur ID
                    return RedirectToAction("Edit", "Clients", new { id = utilisateur.RefId });
                }

                // redirection by Role
                if (User.IsAuthorized(new[] { RoleType.Pdg, RoleType.ResponsableZone }))
                {
                    return RedirectToAction("Edit", new { id = utilisateur.ID });
                }
                return RedirectToAction("Edit", new { id = utilisateur.ID });
            }

            InitViewBag(utilisateurVm);
            return PartialView("_Edit", utilisateurVm);
        }

        // Archive Utilisateur from Details view
        [HttpPost]
        public JsonResult Archiver(int? id)
        {
            if (id != null)
            {
                var utilisateur = db.Utilisateurs.Find(id);
                utilisateur.IsActive = false;
                db.Entry(utilisateur).State = EntityState.Modified;
                db.SaveChanges();
                return Json("");
            }
            return Json("Response Error");
        }

        public TechnicienViewModel GetTechnicien(int? id)
        {
            var technicien = db.Techniciens.Find(id);
            var technicienVm = MapperConfig.mapper.Map<Technicien, TechnicienViewModel>(technicien);

            ViewBag.DLLResponsables = new SelectList((from u in db.Utilisateurs
                                                      where u.RefRole == (int)RoleType.ResponsableZone
                                                      orderby u.Nom
                                                      select new
                                                      {
                                                          Value = u.RefId,
                                                          Text = u.Nom + " " + u.Prenom
                                                      }).ToList(), "Value", "Text", technicien?.RefRespZone ?? 0);

            ViewBag.DDLPays = Repositories.GetCombo(db, "cmbPays", User.LanguageId, technicien?.RefComboPays ?? 0).OrderBy(d => d.Text);

            return technicienVm;
        }

        // GET: Utilisateurs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var utilisateur = db.Utilisateurs.Find(id);
            if (utilisateur == null)
            {
                return HttpNotFound();
            }
            return View(utilisateur);
        }

        // POST: Utilisateurs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var utilisateur = db.Utilisateurs.Find(id);
            db.Utilisateurs.Remove(utilisateur);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

/*

        [HttpPost]
        public JsonResult UpdateUtilisateur(int? id)
        {
            if (id != null)
            {
                var distributeur = db.Distributeurs.Find(id);
                distributeur.IsActive = false;
                db.Entry(distributeur).State = EntityState.Modified;
                db.SaveChanges();
                return Json("");
            }
            return Json("Response Error");
        }

        [HttpPost]
        public JsonResult getUtilisateur(int? id)
        {
            if (id != null)
            {
                var utilisateur = db.Utilisateurs.Find(id);
                return Json("");
                // return PartialView( "UserDetails", model );
            }
            return Json("Response Error");
        }

*/