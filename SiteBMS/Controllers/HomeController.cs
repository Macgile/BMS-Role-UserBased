﻿using System;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web.Mvc;
using PagedList;
using SiteBMS.Models;
using SiteBMS.ViewModels;

namespace SiteBMS.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            //string FullName = User.FirstName + " " + User.LastName;
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        // http://www.mikesdotnetting.com/article/268/how-to-send-email-in-asp-net-mvc

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Contact(EmailFormModel model)
        {
            if (ModelState.IsValid)
            {
                // template html body
                var body = "<p>Email From: {0} ({1})</p><p>Message:</p><p>{2}</p>";

                var message = new MailMessage();

                message.To.Add(new MailAddress("name@gmail.com")); //replace with valid value
                message.Subject = "Your email subject";
                message.Body = string.Format(body, model.FromName, model.FromEmail, model.Message);
                message.IsBodyHtml = true;

                using (var smtp = new SmtpClient())
                {
                    await smtp.SendMailAsync(message);
                    return RedirectToAction("Sent");
                }
            }
            return View(model);
        }

        /*
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Contact(EmailFormModel model)
        {
            if (ModelState.IsValid)
            {
                var body = "<p>Email From: {0} ({1})</p><p>Message:</p><p>{2}</p>";
                var message = new MailMessage();
                message.To.Add(new MailAddress("recipient@gmail.com"));  // replace with valid value
                message.From = new MailAddress("sender@outlook.com");  // replace with valid value
                message.Subject = "Your email subject";
                message.Body = string.Format(body, model.FromName, model.FromEmail, model.Message);
                message.IsBodyHtml = true;

                using (var smtp = new SmtpClient())
                {
                    var credential = new NetworkCredential
                    {
                        UserName = "user@outlook.com",  // replace with valid value
                        Password = "password"  // replace with valid value
                    };
                    smtp.Credentials = credential;
                    smtp.Host = "smtp-mail.outlook.com";
                    smtp.Port = 587;
                    smtp.EnableSsl = true;
                    await smtp.SendMailAsync(message);
                    return RedirectToAction("Sent");
                }
            }
            return View(model);
        }
        */

        // result of sending mail
        public ActionResult Sent()
        {
            return View();
        }

        public ViewResult Translate(int? page, string searchString, string currentFilter)
        {
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            var translation = (from t in db.Traductions
                               from it in db.Traductions.Where(it => it.RefElement == t.RefElement && it.RefLangue == 2).DefaultIfEmpty()
                               from es in db.Traductions.Where(es => es.RefElement == t.RefElement && es.RefLangue == 3).DefaultIfEmpty()
                               from de in db.Traductions.Where(de => de.RefElement == t.RefElement && de.RefLangue == 4).DefaultIfEmpty()
                               from gb in db.Traductions.Where(gb => gb.RefElement == t.RefElement && gb.RefLangue == 5).DefaultIfEmpty()
                               from pt in db.Traductions.Where(pt => pt.RefElement == t.RefElement && pt.RefLangue == 6).DefaultIfEmpty()
                               from nl in db.Traductions.Where(nl => nl.RefElement == t.RefElement && nl.RefLangue == 8).DefaultIfEmpty()
                               where t.RefLangue == 1
                               select new
                               {
                                   Cle = t.Cle ?? "",
                                   Id = t.RefElement,
                                   FR = t.Traduction1,
                                   IT = it.Traduction1,
                                   ES = es.Traduction1,
                                   DE = de.Traduction1,
                                   GB = gb.Traduction1,
                                   PT = pt.Traduction1,
                                   NL = nl.Traduction1
                               }).AsEnumerable().Select(x => new TranslateViewModel
                               {
                                   Cle = x.Cle,
                                   Id = x.Id,
                                   FR = x.FR,
                                   IT = x.IT,
                                   ES = x.ES,
                                   DE = x.DE,
                                   GB = x.GB,
                                   PT = x.PT,
                                   NL = x.NL
                               }).Skip(1); // skip first row (id = 0)

            if (!string.IsNullOrEmpty(searchString))
            {
                // search in cle and traduction field
                translation = translation.Where(t => t.Cle.StartsWith(searchString, StringComparison.InvariantCultureIgnoreCase)
           || t.FR.StartsWith(searchString, StringComparison.InvariantCultureIgnoreCase)
           || t.IT.StartsWith(searchString, StringComparison.InvariantCultureIgnoreCase)
           || t.ES.StartsWith(searchString, StringComparison.InvariantCultureIgnoreCase)
           || t.DE.StartsWith(searchString, StringComparison.InvariantCultureIgnoreCase)
          || t.GB.StartsWith(searchString, StringComparison.InvariantCultureIgnoreCase)
          || t.PT.StartsWith(searchString, StringComparison.InvariantCultureIgnoreCase)
          || t.NL.StartsWith(searchString, StringComparison.InvariantCultureIgnoreCase));
            }

            var pageNumber = (page ?? 1);
            return View(translation.ToPagedList(pageNumber, 10));

            // return View(list);
        }

        // POST: Add Distributor
        // http://vault.lozanotek.com/archive/2010/04/16/posting_json_data_to_mvc_controllers.aspx
        [HttpPost]
        public ActionResult SaveTranslate(TranslateViewModel translate)
        {
            // ajouter auto les langues manquante
            if (translate != null && translate.Id > 0)
            {
                var traduction = db.Traductions.Where(t => t.RefElement == translate.Id).ToList();

                foreach (var item in traduction)
                {
                    Debug.WriteLine(item.RefLangue);

                    // save key if not null
                    if (!string.IsNullOrEmpty(translate.Cle))
                        item.Cle = translate.Cle.Trim();

                    //if (item.Culture == null)
                    //    item.Culture = "";

                    switch (item.RefLangue)
                    {
                        case 1: item.Traduction1 = translate.FR?.Trim() ?? ""; break;
                        case 2: item.Traduction1 = translate.IT?.Trim() ?? ""; break;
                        case 3: item.Traduction1 = translate.ES?.Trim() ?? ""; break;
                        case 4: item.Traduction1 = translate.DE?.Trim() ?? ""; break;
                        case 5: item.Traduction1 = translate.GB?.Trim() ?? ""; break;
                        case 6: item.Traduction1 = translate.PT?.Trim() ?? ""; break;
                        case 8: item.Traduction1 = translate.NL?.Trim() ?? ""; break;
                    }
                    db.Entry(item).State = EntityState.Modified;

                    db.SaveChanges();
                }
                return Json(new { id = translate.Id });
            }
            return Json("Response Error");
        }
    }
}