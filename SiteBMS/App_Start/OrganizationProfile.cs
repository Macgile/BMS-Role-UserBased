﻿using AutoMapper;
using SiteBMS.Models;
using SiteBMS.ViewModels;

namespace SiteBMS
{
    public static class MapperConfig
    {
        public static IMapper mapper;

        public static void Configure()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<ClientProfile>();
                cfg.AddProfile<DistributeurProfile>();
                cfg.AddProfile<ParcelleProfile>();
                cfg.AddProfile<ReleveProfile>();
                cfg.AddProfile<CarenceProfile>();
                cfg.AddProfile<UtilisateurProfile>();
                cfg.AddProfile<TechnicienProfile>();
               
            });
            mapper = config.CreateMapper();
        }
    }

    public class ClientProfile : Profile
    {
        public ClientProfile()
        {
            // DESTINATION->SOURCE
            // AutoMapper.Mapper.CreateMap<SourceClass, DestinationClass>();
            // var destinationObject = AutoMapper.Mapper.Map<DestinatationClass>(sourceObject);
            // AutoMapper.Mapper.Map(sourceObject, destinationObject);

            // http://cpratt.co/using-automapper-creating-mappings/
            CreateMap<Client, ClientViewModel>();
                //.ForMember(dest => dest.Nom, opt =>opt.MapFrom(src => src.Nom.ToUpper()));

            //dest = viewModel-->ClientViewModel
            //source = model-->Client

            /* Country Name for List Custumer */
            //.ForMember(dest => dest.PaysName, opt => opt.MapFrom(src => src.Pay.Name))
            //.ForMember(dest => dest.NbrParcelles, opt => opt.MapFrom(src => src.Parcelles.Count(p => new[] { 6, 7 }.Contains(p.RefComboEspece)).ToString()));

            CreateMap<ClientViewModel, Client>()
                .ForMember(dest => dest.DatCreat, opt => opt.MapFrom(src => src.DatCreat))
               // .ForMember(dest => dest.Nom, opt => opt.MapFrom(src => src.Nom.ToUpper()))
                .ForMember(dest => dest.DatModif, opt => opt.MapFrom(src => src.DatModif));
        }
    }

    public class DistributeurProfile : Profile
    {
        public DistributeurProfile()
        {
            // AutoMapper.Mapper.CreateMap<SourceClass, DestinationClass>();
            CreateMap<Distributeur, DistributeurViewModel>();
            //  .ForMember(dest => dest.Pay, opt => opt.MapFrom(src => src.Pay.Name));

            CreateMap<DistributeurViewModel, Distributeur>();
        }
    }

    public class ParcelleProfile : Profile
    {
        public ParcelleProfile()
        {
            // AutoMapper.Mapper.CreateMap<SourceClass, DestinationClass>();
            CreateMap<Parcelle, ParcelleViewModel>();
            CreateMap<ParcelleViewModel, Parcelle>()
                .ForMember(dest => dest.DatCreat, opt => opt.MapFrom(src => src.DatCreat))
                .ForMember(dest => dest.DatModif, opt => opt.MapFrom(src => src.DatModif));
        }
    }

    public class ReleveProfile : Profile
    {
        public ReleveProfile()
        {
            // AutoMapper.Mapper.CreateMap<SourceClass, DestinationClass>();
            CreateMap<Releve, ReleveViewModel>();
            CreateMap<ReleveViewModel, Releve>();
            //.ForMember(dest => dest.DateReleve, opt => opt.MapFrom(src => src.DateReleve));
        }
    }

    public class CarenceProfile : Profile
    {
        public CarenceProfile()
        {
            // AutoMapper.Mapper.CreateMap<SourceClass, DestinationClass>();
            CreateMap<CarenceReleve, CarenceViewModel>();
            CreateMap<CarenceViewModel, CarenceReleve>()
                .ForSourceMember(src => src.CarenceValue, s => s.Ignore())
                .ForSourceMember(src => src.TypeCarence, s => s.Ignore());

            // CreateMap<CarenceViewModel, CarenceReleve>()
            // .ForAllMembers(opt => opt.Ignore());
            // .ForMember(dest => dest.ID, opt => opt.MapFrom(src => src.ID))

            CreateMap<Carence, CarenceReleve>()
                .ForMember(dest => dest.ValeurAnalyseCarence, opt => opt.MapFrom(src => src.ValeursAnalyse))
                .ForMember(dest => dest.RefComboCarence, opt => opt.MapFrom(src => src.TypeChoix))
                .ForMember(dest => dest.RefCarenceNorme, opt => opt.MapFrom(src => src.RefNorme));
        }
    }

    public class UtilisateurProfile : Profile
    {
        public UtilisateurProfile()
        {
            // AutoMapper.Mapper.CreateMap<SourceClass, DestinationClass>();
            CreateMap<Utilisateur, UtilisateurViewModel>();
            CreateMap<UtilisateurViewModel, Utilisateur>();
        }
    }

    public class TechnicienProfile : Profile
    {
        public TechnicienProfile()
        {
            // AutoMapper.Mapper.CreateMap<SourceClass, DestinationClass>();
            CreateMap<Technicien, TechnicienViewModel>();
            CreateMap<TechnicienViewModel, Technicien>();
        }
    }

}