﻿using System.Web.Optimization;

namespace SiteBMS
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/DataTables/jquery.dataTables.min.js",
                        "~/Scripts/DataTables/dataTables.bootstrap.min.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryUI").Include(
            "~/Scripts/jquery-ui-{version}.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*",
                        "~/Scripts/jQueryFixes.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/bootstrap-select.min.js",
                      "~/Scripts/bootstrap-switch.min.js",
                      //"~/Scripts/bootstrap-toggle.min.js",
                      "~/Scripts/custom.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/pulsate").Include(
                       "~/Scripts/jquery.pulsate.js"
                     ));

            bundles.Add(new ScriptBundle("~/bundles/client").Include(
                      "~/Scripts/client.js"
                      ));

            bundles.Add(new ScriptBundle("~/bundles/reportBilan").Include(
                      "~/Scripts/report.bilan.js"
                      ));

            bundles.Add(new ScriptBundle("~/bundles/password").Include(
                      "~/Scripts/password.js"
                      ));

            bundles.Add(new ScriptBundle("~/bundles/matchHeight").Include(
                      "~/Scripts/jquery.matchHeight.js"
                      ));

            bundles.Add(new ScriptBundle("~/bundles/ieSupport").Include(
                      "~/Scripts/html5shiv.js",
                      "~/Scripts/respond.min.js"));

            bundles.Add(new StyleBundle("~/Content/login").Include(
                        "~/Content/css/login.css"
                        ));

            bundles.Add(new StyleBundle("~/Content/site").Include(
                       "~/Content/css/site.css"
                       ));

            //"~/Content/css/green.css",
            bundles.Add(new StyleBundle("~/Content/gentelella").Include(
                       "~/Content/css/custom.css",
                       "~/Content/css/font-awesome.min.css"
                       ));

            bundles.Add(new StyleBundle("~/Content/general").Include(
                        "~/Content/bootstrap.css",
                        "~/Content/bootstrap-select.min.css",
                        "~/Content/bootstrap-switch.min.css",
                        //"~/Content/bootstrap-toggle.min.css",
                        "~/Content/css/PagedList.css",
                        "~/Content/DataTables/css/dataTables.bootstrap.min.css"
                        //"~/Content/DataTables/css/jquery.dataTables.min.css"
                        ));
        }
    }
}