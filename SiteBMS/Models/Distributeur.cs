namespace SiteBMS.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Distributeur")]
    public partial class Distributeur
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Distributeur()
        {
            Clients = new HashSet<Client>();
        }

        public int ID { get; set; }

        [Required]
        [StringLength(50)]
        public string Nom { get; set; }

        [StringLength(50)]
        public string Adr1 { get; set; }

        [StringLength(50)]
        public string Adr2 { get; set; }

        [StringLength(50)]
        public string CodePostal { get; set; }

        [StringLength(50)]
        public string Ville { get; set; }

        public int RefComboPays { get; set; }

        [StringLength(50)]
        public string Tel1 { get; set; }

        [StringLength(50)]
        public string Fax { get; set; }

        [StringLength(50)]
        public string Tel2 { get; set; }

        [StringLength(50)]
        public string Mel { get; set; }

        public bool IsActive { get; set; }

        public int IDUserCreat { get; set; }

        public int IDUserModif { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime IDDatCreat { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime IDDatModif { get; set; }

        public int RefTechBMS { get; set; }


        // public virtual Pay Pay { get; set; }
        [NotMapped]
        public string Pays { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Client> Clients { get; set; }
    }
}
