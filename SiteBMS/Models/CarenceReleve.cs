using System.Linq;

namespace SiteBMS.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CarenceReleve")]
    public partial class CarenceReleve
    {
        public int ID { get; set; }

        public int RefCarenceNorme { get; set; }

        public int RefReleve { get; set; }

        public int? RefComboCarence { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? ValeurAnalyseCarence { get; set; }

        public virtual CarenceNorme CarenceNorme { get; set; }

        public virtual Releve Releve { get; set; }
    }
}
