namespace SiteBMS.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Combo_Element_Numeric
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int RefCombo { get; set; }

        [Key]
        [Column(Order = 1, TypeName = "numeric")]
        public decimal Valeur { get; set; }

        [StringLength(50)]
        public string Libelle { get; set; }

        public virtual Combo Combo { get; set; }
    }
}
