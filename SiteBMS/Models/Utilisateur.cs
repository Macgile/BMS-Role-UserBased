namespace SiteBMS.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Utilisateur")]
    public partial class Utilisateur
    {
        public int ID { get; set; }

        [Required]
        [StringLength(50)]
        public string Nom { get; set; }

        [StringLength(50)]
        public string Prenom { get; set; }

        [Required]
        [StringLength(50)]
        public string Login { get; set; }

        [Required]
        [StringLength(50)]
        public string PassWord { get; set; }

        public int? RefLangue { get; set; }

        [NotMapped]
        public string LangueName { get; set; }

        [Required]
        [StringLength(50)]
        public string Email { get; set; }

        public bool IsActive { get; set; }

        public int RefRole { get; set; }

        public int RefId { get; set; }

        public virtual RoleUtilisateur RoleUtilisateurs { get; set; }
    }
}
