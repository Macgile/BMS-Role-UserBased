namespace SiteBMS.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CarenceNorme")]
    public partial class CarenceNorme
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CarenceNorme()
        {
            CarenceReleves = new HashSet<CarenceReleve>();
        }

        public int ID { get; set; }

        public int RefCarenceElement { get; set; }

        public int RefEspece { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? NormeMax { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? NormeMin { get; set; }

        public int RefType { get; set; }

        public virtual CarenceElement CarenceElement { get; set; }

        public virtual CarenceType CarenceType { get; set; }

        public virtual Espece Espece { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CarenceReleve> CarenceReleves { get; set; }
    }
}
