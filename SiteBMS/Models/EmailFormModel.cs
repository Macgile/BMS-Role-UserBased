﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace SiteBMS.Models
{
    public class EmailFormModel
    {
        [Required, Display(Name = "Votre Nom")]
        public string FromName { get; set; }
        [Required, Display(Name = "Votre email"), EmailAddress]
        public string FromEmail { get; set; }
        [Required]
        public string Message { get; set; }
    }
}