namespace SiteBMS.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Client")]
    public partial class Client
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Client()
        {
            Parcelles = new HashSet<Parcelle>();
            Distributeurs = new HashSet<Distributeur>();
        }

        [NotMapped]
        public string FullName => Utils.UcFirst(Prenom) + " " + Utils.UcFirst(Nom);

        public int ID { get; set; }

        [Required]
        [StringLength(50)]
        public string Nom { get; set; }

        [StringLength(50)]
        public string Prenom { get; set; }

        [StringLength(50)]
        public string Societe { get; set; }

        public int RefTechBMS { get; set; }

        [StringLength(50)]
        public string Adr1 { get; set; }

        [StringLength(50)]
        public string Adr2 { get; set; }

        [StringLength(10)]
        public string CodePostal { get; set; }

        [StringLength(70)]
        public string Ville { get; set; }

        public int RefComboPays { get; set; }

        [StringLength(30)]
        public string Tel1 { get; set; }

        [StringLength(30)]
        public string Tel2 { get; set; }

        [StringLength(30)]
        public string Fax { get; set; }

        [StringLength(50)]
        public string Mel { get; set; }

        public int RefComboTypeCommercialisation { get; set; }

        public int IDUserCreat { get; set; }

        public int IDUserModif { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime DatCreat { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime DatModif { get; set; }

        public bool IsActive { get; set; }

        //public int Pays_ID { get; set; }

        //public virtual Pay Pay { get; set; }

        [NotMapped]
        public string Pays { get; set; }

        public virtual Technicien Technicien { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Parcelle> Parcelles { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Distributeur> Distributeurs { get; set; }
    }
}
