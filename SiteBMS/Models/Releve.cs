namespace SiteBMS.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Releve")]
    public partial class Releve
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Releve()
        {
            Bilans = new HashSet<Bilan>();
            CarenceReleves = new HashSet<CarenceReleve>();
        }

        public int ID { get; set; }

        [Required]
        [StringLength(15)]
        public string Nom { get; set; }

        public int RefParcelle { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime DateReleve { get; set; }

        [Column(TypeName = "numeric")]
        public decimal ObjectifRendement { get; set; }

        [Column(TypeName = "numeric")]
        public decimal ObjectifRendementN_1 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? ObjectifCalibre { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? ObjectifCalibreN_1 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? ObjectifDegre { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? ObjectifDegreN_1 { get; set; }

        public bool? Alternance { get; set; }

        public int? RefComboTypeVin { get; set; }

        public int RefComboGreffe { get; set; }

        public int? RefComboVigueurVisuelle { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MatiereOrganique { get; set; }

        // number with comma or point separator
        [Column(TypeName = "numeric")]
        public decimal? PH { get; set; }

        [Column(TypeName = "numeric")]
        //[RegularExpression(@"^[0-9\.,]{0,15}$", ErrorMessage = "ce champ ne doit contenir que des chiffres")]
        public decimal? CEC { get; set; }

        public bool? Drainage { get; set; }

        public int? FumureN { get; set; }

        public int? FumureP { get; set; }

        public int? FumureK { get; set; }

        public int? RefComboAge { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? NbRang { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? HauteurCanope { get; set; }

        public int? RefComboTypeTaille { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? PluvioReel { get; set; }

        public int? RefComboCoulure { get; set; }

        public int? RefComboChlorose { get; set; }

        public int? RefComboStadeConservation { get; set; }

        public int? RefComboTypeConservation { get; set; }

        public bool? ApportMO { get; set; }

        public int? RefComboMillerandage { get; set; }

        public int? RefComboSanitaire { get; set; }

        public int? RefComboCategorieVin { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Calcaire { get; set; }

        public int? RefComboRafle { get; set; }

        public int? RefComboCarencePrincipale { get; set; }

        public int? RefComboCarenceSecondaire { get; set; }

        public int? RefComboCarenceAutre { get; set; }

        [Column(TypeName = "text")]
        public string Remarque { get; set; }

        [Column(TypeName = "text")]
        public string RemarqueCarence { get; set; }

        [Column(TypeName = "text")]
        public string RemarqueTech { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Pluvio2 { get; set; }

        public int? IDUserCreat { get; set; }

        public int? IDUserModif { get; set; }

        public DateTime? DatCreat { get; set; }

        public DateTime? DatModif { get; set; }

        public bool IsPerso { get; set; }

        public bool IsActive { get; set; }

        public bool IsInitial { get; set; }

        public bool IsBilanCalculated { get; set; }

        public int RefComboTypeUser { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Bilan> Bilans { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CarenceReleve> CarenceReleves { get; set; }

        public virtual Greffe Greffe { get; set; }

        public virtual Parcelle Parcelle { get; set; }
    }
}