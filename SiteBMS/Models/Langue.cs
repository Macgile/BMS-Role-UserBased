namespace SiteBMS.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Langue")]
    public partial class Langue
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Langue()
        {
            Traductions = new HashSet<Traduction>();
        }

        public int ID { get; set; }

        [Required]
        [StringLength(50)]
        public string Nom { get; set; }

        public bool IsActive { get; set; }

        public int? Nom_T { get; set; }

        public int RefElement { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Traduction> Traductions { get; set; }
    }
}
