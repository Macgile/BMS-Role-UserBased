namespace SiteBMS.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Produit")]
    public partial class Produit
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Produit()
        {
            Bilans = new HashSet<Bilan>();
            BilanModeles = new HashSet<BilanModele>();
            ProgrammeDetails = new HashSet<ProgrammeDetail>();
            Quotas = new HashSet<Quota>();
            Especes = new HashSet<Espece>();
        }

        public int ID { get; set; }

        public int Nom_T { get; set; }

        [Required]
        [StringLength(2)]
        public string UnitProd { get; set; }

        public bool Bio { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Pack1 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Pack2 { get; set; }

        public bool IsActive { get; set; }

        [Column(TypeName = "numeric")]
        public decimal Prix { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Bilan> Bilans { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BilanModele> BilanModeles { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProgrammeDetail> ProgrammeDetails { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Quota> Quotas { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Espece> Especes { get; set; }
    }
}
