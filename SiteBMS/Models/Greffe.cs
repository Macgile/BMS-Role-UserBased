namespace SiteBMS.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Greffe")]
    public partial class Greffe
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Greffe()
        {
            Releves = new HashSet<Releve>();
        }

        public int ID { get; set; }

        public int RefEspece { get; set; }

        public int Nom_T { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? VigGref { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? IPC { get; set; }

        public bool IsActive { get; set; }

        public virtual Espece Espece { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Releve> Releves { get; set; }
    }
}
