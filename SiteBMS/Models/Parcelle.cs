namespace SiteBMS.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Parcelle")]
    public partial class Parcelle
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Parcelle()
        {
            Releves = new HashSet<Releve>();
        }

        public int ID { get; set; }

        public int RefClient { get; set; }

        [Required]
        [Display(Name = "Nom de la Parcelle")]
        [StringLength(50)]
        public string Nom { get; set; }

        [Display(Name = "Surface")]
        public float Surface { get; set; }

        [Display(Name = "Esp�ce")]
        public int RefComboEspece { get; set; }

        [Display(Name = "Vari�t�")]
        public int RefVariete { get; set; }

        [Display(Name = "Zone climatique")]
        public int RefComboZoneClimatique { get; set; }

        [Display(Name = "Type irrigation")]
        public int RefComboTypeIrrigation { get; set; }

        [Display(Name = "Mode de production")]
        public int? RefComboModeProduction { get; set; }

        [Display(Name = "Cr�� par")]
        public int IDUserCreat { get; set; }

        [Display(Name = "Modifi� par")]
        public int IDUserModif { get; set; }

        [Display(Name = "Cr�� le")]
        [DisplayFormat(DataFormatString = "{0:dd MMMM yyyy}", ApplyFormatInEditMode = true)]
        [Column(TypeName = "smalldatetime")]
        public DateTime DatCreat { get; set; }

        [Display(Name = "Modifi� le")]
        [DisplayFormat(DataFormatString = "{0:dd MMMM yyyy}", ApplyFormatInEditMode = true)]
        [Column(TypeName = "smalldatetime")]
        public DateTime DatModif { get; set; }

        [Display(Name = "Activ�")]
        public bool IsActive { get; set; }

        public virtual Client Client { get; set; }

        public virtual Variete Variete { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Releve> Releves { get; set; }
    }
}
