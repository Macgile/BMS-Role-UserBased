namespace SiteBMS.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ProgrammeDetail")]
    public partial class ProgrammeDetail
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int RefProgrammeModele { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int RefProgrammeNutrition { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int RefEtape { get; set; }

        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int RefProduit { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Qte { get; set; }

        public virtual Etape Etape { get; set; }

        public virtual Produit Produit { get; set; }

        public virtual ProgrammeModele ProgrammeModele { get; set; }

        public virtual ProgrammeNutrition ProgrammeNutrition { get; set; }
    }
}
