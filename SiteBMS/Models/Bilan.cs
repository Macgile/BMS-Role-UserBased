namespace SiteBMS.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Bilan")]
    public partial class Bilan
    {
        public int ID { get; set; }

        public int RefReleve { get; set; }

        public int RefProgrammeNutrition { get; set; }

        public int RefEtape { get; set; }

        public int RefProduit { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Qte { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? QteInitial { get; set; }

        public int NbApplication { get; set; }

        public bool IsValide { get; set; }

        public bool IsPerso { get; set; }

        [StringLength(3)]
        public string Origine { get; set; }

        public int? PersoBy { get; set; }

        public virtual Etape Etape { get; set; }

        public virtual Produit Produit { get; set; }

        public virtual ProgrammeNutrition ProgrammeNutrition { get; set; }

        public virtual Releve Releve { get; set; }
    }
}
