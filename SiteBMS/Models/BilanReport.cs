﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SiteBMS.Models
{

    public class EtapeBilan
    {
        public int ID { get; set; }
        public byte[] ImgEtap { get; set; }
        public int RefEspece { get; set; }
        public string Nom { get; set; }
        public string Description { get; set; }
    }

    public class ProgramBilan
    {
        public int ID { get; set; }
        public string Nom { get; set; }
        public string Description { get; set; }
    }

    public class BilanValues
    {
        public int ID { get; set; }
        public int RefProgrammeNutrition { get; set; }
        public int RefEtape { get; set; }
        public int RefProduit { get; set; }

        public decimal? Qte { get; set; }
        public decimal? QteInitial { get; set; }

        public bool IsValide { get; set; }
        public bool IsPerso { get; set; }

        public string Releve { get; set; }
        public string Parcelle { get; set; }
        public string Espece { get; set; }
        public string Variete { get; set; }
        public string Produit { get; set; }
        public string UnitProd { get; set; }

        public float Surface { get; set; }
        public int PersoCase { get; set; }
    }

    public class BilanReport
    {
        public ICollection<EtapeBilan> Etape { get; set; }
        public ICollection<ProgramBilan> Programme { get; set; }
        public ICollection<BilanValues> Values { get; set; }
    }
}