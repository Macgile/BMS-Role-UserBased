namespace SiteBMS.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("BilanModele")]
    public partial class BilanModele
    {
        public int ID { get; set; }

        public int RefProgrammeNutrition { get; set; }

        public int RefEtape { get; set; }

        public int RefProduit { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Qte { get; set; }

        public int NbApplication { get; set; }

        public virtual Etape Etape { get; set; }

        public virtual Produit Produit { get; set; }

        public virtual ProgrammeNutrition ProgrammeNutrition { get; set; }
    }
}
