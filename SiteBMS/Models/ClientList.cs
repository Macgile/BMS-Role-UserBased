namespace SiteBMS.Models
{
    internal class ClientList
    {
        public int ID { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public string Societe { get; set; }
        public string CodePostal { get; set; }
        public string Ville { get; set; }
        public int RefComboPays { get; set; }
        //public string Pays { get; set; }
    }
}