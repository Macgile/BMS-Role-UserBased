namespace SiteBMS.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ProgrammeModele")]
    public partial class ProgrammeModele
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ProgrammeModele()
        {
            ProgrammeDetails = new HashSet<ProgrammeDetail>();
            ProgrammeApplicationDirects = new HashSet<ProgrammeApplicationDirect>();
        }

        public int ID { get; set; }

        [Required]
        [StringLength(50)]
        public string Nom { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProgrammeDetail> ProgrammeDetails { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProgrammeApplicationDirect> ProgrammeApplicationDirects { get; set; }
    }
}
