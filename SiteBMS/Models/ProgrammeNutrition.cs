namespace SiteBMS.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ProgrammeNutrition")]
    public partial class ProgrammeNutrition
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ProgrammeNutrition()
        {
            Bilans = new HashSet<Bilan>();
            BilanModeles = new HashSet<BilanModele>();
            ProgrammeDetails = new HashSet<ProgrammeDetail>();
            Quotas = new HashSet<Quota>();
        }

        public int ID { get; set; }

        public int Nom_T { get; set; }

        public int RefEspece { get; set; }

        public int? Description_T { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Bilan> Bilans { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BilanModele> BilanModeles { get; set; }

        public virtual Espece Espece { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProgrammeDetail> ProgrammeDetails { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Quota> Quotas { get; set; }
    }
}
