namespace SiteBMS.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CoefFumure")]
    public partial class CoefFumure
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID { get; set; }

        public int Nom_T { get; set; }

        public decimal Coef { get; set; }

        public int RefEspece { get; set; }

        public virtual Espece Espece { get; set; }
    }
}
