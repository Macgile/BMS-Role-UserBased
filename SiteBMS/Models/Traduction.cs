namespace SiteBMS.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Traduction")]
    public partial class Traduction
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int RefElement { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int RefLangue { get; set; }

        [Column("Traduction")]
       // [Required]
        [StringLength(1000)]
        public string Traduction1 { get; set; }

        [StringLength(50)]
        public string Cle { get; set; }

        [StringLength(10)]
        public string Culture { get; set; }

        public bool IsDefaut { get; set; }

        //[NotMapped]
        public virtual Element Element { get; set; }

       // [NotMapped]
        public virtual Langue Langue { get; set; }
    }
}