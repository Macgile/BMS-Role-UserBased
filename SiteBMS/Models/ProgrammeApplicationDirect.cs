namespace SiteBMS.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ProgrammeApplicationDirect")]
    public partial class ProgrammeApplicationDirect
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int RefVariete { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(50)]
        public string ColReleve { get; set; }

        [Key]
        [Column(Order = 2)]
        public bool Desequilibre { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Valeur { get; set; }

        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int RefProgrammeModele { get; set; }

        public virtual ProgrammeModele ProgrammeModele { get; set; }
    }
}
