namespace SiteBMS.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Variete")]
    public partial class Variete
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Variete()
        {
            Parcelles = new HashSet<Parcelle>();
        }

        public int ID { get; set; }

        public int RefEspece { get; set; }

        public int Nom_T { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? RendtMoy { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? CalibreMoy { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Vigueur { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? Precoce { get; set; }

        public bool Noor { get; set; }

        public bool IsAcive { get; set; }

        public virtual Espece Espece { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Parcelle> Parcelles { get; set; }
    }
}
