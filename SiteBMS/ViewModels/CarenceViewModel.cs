﻿using SiteBMS.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;
using System.Reflection;

namespace SiteBMS.ViewModels
{
    public class Carence
    {
        public decimal ValeursAnalyse { get; set; }
        public int TypeChoix { get; set; }
        public int RefNorme { get; set; }
    }

    public class CarenceValues
    {

        // ref to class origin : store infos care
        public CarenceReleve CarenceReleve { get; set; }

        // Feuilles(1) - Fruits(2) - Rameaux(3)
        public int TypeCarence { get; set; }

        // dropdownlist properties (not in CarenceReleve model )
        public int ddl_1 { get; set; }

        public int ddl_2 { get; set; }
        public int ddl_3 { get; set; }
        public int ddl_4 { get; set; }
        public int ddl_5 { get; set; }
        public int ddl_6 { get; set; }
        public int ddl_7 { get; set; }
        public int ddl_8 { get; set; }
        public int ddl_9 { get; set; }
        public int ddl_10 { get; set; }
        public int ddl_11 { get; set; }
        public int ddl_12 { get; set; }

        // textbox properties
        public string tbx_1 { get; set; }

        public string tbx_2 { get; set; }
        public string tbx_3 { get; set; }
        public string tbx_4 { get; set; }
        public string tbx_5 { get; set; }
        public string tbx_6 { get; set; }
        public string tbx_7 { get; set; }
        public string tbx_8 { get; set; }
        public string tbx_9 { get; set; }
        public string tbx_10 { get; set; }
        public string tbx_11 { get; set; }
        public string tbx_12 { get; set; }

        // set values by propoerty name
        public void PropertySet(object p, string propName, object value)
        {
            try
            {
                Type t = p.GetType();
                PropertyInfo info = t.GetProperty(propName);

                if (info == null)
                    return;
                if (!info.CanWrite)
                    return;

                info.SetValue(p, value, null);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        public List<int> GetList
        {
            get
            {
                var list = new List<int>();

                try
                {
                    Type t = GetType();

                    for (int i = 1; i <= 12; i++)
                    {
                        var p = GetType().GetField("tbx_" + i).GetValue(this).ToString();
                        if (string.IsNullOrEmpty(p))
                            list.Add(i);
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                }

                return list;
            }
        }

        // get List of Values of properties
        // using reflexion
        public List<Carence> GetValues(object p)
        {
            var list = new List<Carence>();

            try
            {
                Type t = p.GetType();

                for (var i = 1; i <= 12; i++)
                {
                    // get property by name in class
                    PropertyInfo tbx = t.GetProperty("tbx_" + i);
                    PropertyInfo ddl = t.GetProperty("ddl_" + i);

                    if (tbx != null)
                    {
                        var val_tbx = tbx.GetValue(p, null);
                        var val_ddl = ddl.GetValue(p, null);

                        var valueTbx = (val_tbx != null) ? Convert.ToDecimal(val_tbx) : 0;
                        var valueDDL = (val_ddl != null) ? Convert.ToInt32(val_ddl) : 4;

                        if (valueTbx > 0)
                        {
                            var carence = new Carence()
                            {
                                ValeursAnalyse = valueTbx,
                                TypeChoix = valueDDL,
                                RefNorme = i
                            };

                            list.Add(carence);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
            return list;
        }
    }

    // viewModel of CarenceReleve model
    public class CarenceViewModel
    {
        public int ID { get; set; }

        // [Display(Name = "Standard", ResourceType = typeof(Resources.Resources))]

        [Display(Name = "Standard", ResourceType = typeof(Resources.Resources))] //[Display(Name = "Norme")]
        public int RefCarenceNorme { get; set; }

        public int RefReleve { get; set; }

       [Display(Name = "Deficiency", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Carence")]
        public int? RefComboCarence { get; set; }

        [Display(Name = "Analyzed", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Analysé")]
        [Column(TypeName = "numeric")]
        public decimal? ValeurAnalyseCarence { get; set; }

        [Display(Name = "Standard", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Norme")]
        public virtual CarenceNorme CarenceNorme { get; set; }

        public virtual Releve Releve { get; set; }

        // Properties  not in model
        public int TypeCarence { get; set; }

        public int EspeceID { get; set; }

        public CarenceValues CarenceValue { get; set; }
    }
}