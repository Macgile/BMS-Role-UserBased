﻿namespace SiteBMS.ViewModels
{
    using System;
    using System.ComponentModel.DataAnnotations;

    [Serializable]
    public class TranslateViewModel
    {
        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        public string Cle { get; set; }

        [Required]
        [StringLength(1000)]
        public string FR { get; set; }

        [StringLength(1000)]
        public string IT { get; set; }

        [StringLength(1000)]
        public string ES { get; set; }

        [StringLength(1000)]
        public string DE { get; set; }

        [StringLength(1000)]
        public string GB { get; set; }

        [StringLength(1000)]
        public string PT { get; set; }

        //[StringLength(1000)]
        //public string AR { get; set; }

        [StringLength(1000)]
        public string NL { get; set; }
    }
}