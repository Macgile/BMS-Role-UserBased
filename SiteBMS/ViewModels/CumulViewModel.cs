﻿using System.ComponentModel.DataAnnotations;

namespace SiteBMS.ViewModels
{
    public class CumulViewModel
    {
        [Display(Name = "Produits")]
        public string Produit { get; set; }

        [DisplayFormat(DataFormatString = "{0:#.##}")]
        [Display(Name = "Quantité")]
        public double? TotQte { get; set; }

        [Display(Name = "Unité(s)")]
        public string UnitProd { get; set; }
    }
}