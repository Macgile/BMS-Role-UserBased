﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;
using SiteBMS.Models;

namespace SiteBMS.ViewModels
{
    public class UtilisateurViewModel
    {
        public int ID { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "Name", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Nom")]
        public string Nom { get; set; }

        [StringLength(50)]
        [Display(Name = "FirstName", ResourceType = typeof(Resources.Resources))] //[Display(Nom = "Prénom")]
        public string Prenom { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "UserName", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Login")]
        public string Login { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "Password", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Mot de passe")]
        public string PassWord { get; set; }

        //[Required(ErrorMessageResourceType = typeof(Resources.Resources),
        //      ErrorMessageResourceName = "EmailRequired")]
        /*
        [Display(Name = "PasswordField", ResourceType = typeof(My.Resources.Resource))]
        [MinLength(6, ErrorMessageResourceName = "PasswordLengthError", ErrorMessageResourceType = typeof(My.Resources.Resource))]
        [Compare("PasswordAgain", ErrorMessageResourceName = "CompareError", ErrorMessageResourceType = typeof(My.Resources.Resource))]
        [Required(AllowEmptyStrings = False, ErrorMessageResourceName = "RequiredPassword", ErrorMessageResourceType = typeof(My.Resources.Resource))]
        */
        //[Compare("PassWord", ErrorMessage = "Le mot de passe ne correspond pas, saissez le à nouveau! ErrorPassword")]

        /*
                [NotMapped]
                [Display(Name = "Confirmer le mot de passe")]
                [Compare("PassWord", ErrorMessage = "Le mot de passe ne correspond pas, saissez le à nouveau!")]
                public string ConfirmPassword { get; set; }

        */

        [NotMapped]
        [Display(Name = "ConfirmPassword", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Confirmer le mot de passe")]
        [Compare("PassWord", ErrorMessageResourceName = "ErrorPassword", ErrorMessageResourceType = typeof(Resources.Resources))]
        public string ConfirmPassword { get; set; }

        [Display(Name = "Language", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Langue")]
        public int? RefLangue { get; set; }

        [NotMapped]
        [Display(Name = "Language", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Langue")]
        public string LangueName { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "Email", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name = "Activated", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Activé")]
        public bool IsActive { get; set; }

        [Display(Name = "Role", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Rôle")]
        public int RefRole { get; set; }

        [Display(Name = "Technician", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Technicien")]
        public int RefId { get; set; }

        [Display(Name = "Role", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Rôle")]
        public virtual RoleUtilisateur RoleUtilisateurs { get; set; }

        [NotMapped]
        public TechnicienViewModel Technicien { get; set; }

        //provide origin of action (from distributeur/client) when update or add utilisateur
        [NotMapped]
        public string Origin { get; set; }

        public string FullName => Utils.UcFirst(Prenom) + " " + Utils.UcFirst(Nom);
    }
}