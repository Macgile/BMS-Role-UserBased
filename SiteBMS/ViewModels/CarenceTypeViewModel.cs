﻿using System.ComponentModel.DataAnnotations;
using SiteBMS.Models;

namespace SiteBMS.ViewModels
{
    public class CarenceTypeViewModel
    {
        [Display(Name = "TypeAnalysis", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Type Analyses")]
        public string Name { get; set; }

        [Display(Name = "NumberAnalyzes", ResourceType = typeof(Resources.Resources))] //  [Display(Name = "Nombre d'analyses")]
        public int? CarenceCount { get; set; }

        public int? RefType { get; set; }

        public int? RefReleve { get; set; }

        public int? RefParcelle { get; set; }

        public Releve Releve { get; set; }

    }
}