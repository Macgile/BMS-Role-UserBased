﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;
using SiteBMS.Models;

namespace SiteBMS.ViewModels
{
    public class TechnicienViewModel
    {
        public int ID { get; set; }

        [Display(Name = "InChargeOf", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Responsable zone")]
        public int RefRespZone { get; set; }

        [Display(Name = "Country", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Pays")]
        public int RefComboPays { get; set; }

        [StringLength(50)]
        [Display(Name = "PhoneNumber", ResourceType = typeof(Resources.Resources))] //[Display(Name = "Téléphone")]
        public string Tel1 { get; set; }

        [StringLength(50)]
        [Display(Name = "NumberGSM", ResourceType = typeof(Resources.Resources))] //[Display(Name = "Portable")]
        public string Tel2 { get; set; }

        [StringLength(50)]
        [Display(Name = "FaxNumber", ResourceType = typeof(Resources.Resources))] //[Display(Name = "Fax")]
        public string Fax { get; set; }

        [Display(Name = "Activated", ResourceType = typeof(Resources.Resources))] //[Display(Name = "Activé")]
        public bool IsActive { get; set; }

    }
}