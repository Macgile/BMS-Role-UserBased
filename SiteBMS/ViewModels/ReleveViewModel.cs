﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SiteBMS.Models;

namespace SiteBMS.ViewModels
{
    public class ReleveViewModel
    {
        public int ID { get; set; }

        // [DisplayFormat(DataFormatString = "{0:0.00}", ApplyFormatInEditMode = true)]

        // nom de la recolte
        [Required]
        [Display(Name = "Name", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Nom du relevé")]
        [StringLength(15)]
        public string Nom { get; set; }

        [Display(Name = "Parcel", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Parcelle")]
        public int RefParcelle { get; set; }

        [Display(Name = "DateOfStatement", ResourceType = typeof(Resources.Resources))] //[Display(Name = "Date du Releve")]
        [Column(TypeName = "smalldatetime")]
        [DisplayFormat(DataFormatString = "{0:dd MMMM yyyy}", ApplyFormatInEditMode = true)]
        public DateTime DateReleve { get; set; }

        [Display(Name = "PerformanceObjective", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Objectif rendement")]
        //[RegularExpression(@"^[0-9.]{0,15}$", ErrorMessage = "ce champ ne doit contenir que des chiffres")]
        [Column(TypeName = "numeric")] //cmbTonnagePomPoir GetComboNumeric
        public decimal ObjectifRendement { get; set; }

        [Display(Name = "PerformanceObjectiveN1", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Objectif rendement N-1")]
       // [RegularExpression(@"^[0-9.]{0,15}$", ErrorMessage = "ce champ ne doit contenir que des chiffres")]
        [Column(TypeName = "numeric")] //cmbTonnagePomPoir GetComboNumeric
        public decimal ObjectifRendementN_1 { get; set; }

        [Display(Name = "Caliber", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Calibre")]
       // [RegularExpression(@"^[0-9.]{0,15}$", ErrorMessage = "ce champ ne doit contenir que des chiffres")]
        [Column(TypeName = "numeric")] // cmbCalibrePomPoir GetComboNumeric
        public decimal? ObjectifCalibre { get; set; }

        [Display(Name = "CaliberN1", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Calibre N-1")]
        //[RegularExpression(@"^[0-9.]{0,15}$", ErrorMessage = "ce champ ne doit contenir que des chiffres")]
        [Column(TypeName = "numeric")]// cmbCalibrePomPoir GetComboNumeric
        public decimal? ObjectifCalibreN_1 { get; set; }

        [Display(Name = "ObjectiveDegree", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Objectif Degré")]
        //[RegularExpression(@"^[0-9.]{0,15}$", ErrorMessage = "ce champ ne doit contenir que des chiffres")]
        [Column(TypeName = "numeric")]
        public decimal? ObjectifDegre { get; set; }

        [Display(Name = "ObjectiveDegree", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Objectif Degré N-1")]
        //[RegularExpression(@"^[0-9.]{0,15}$", ErrorMessage = "ce champ ne doit contenir que des chiffres")]
        [Column(TypeName = "numeric")]
        public decimal? ObjectifDegreN_1 { get; set; }

        [Display(Name = "Alternating", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Alternance")]
        public bool? Alternance { get; set; }

        [Display(Name = "M.O")]
        [Column(TypeName = "numeric")]
        //[RegularExpression(@"^[0-9.]{0,15}$", ErrorMessage = "ce champ ne doit contenir que des chiffres")]
        public decimal? MatiereOrganique { get; set; }



        //[RegularExpression(@"^[0-9.]{0,15}$", ErrorMessage = "ce champ ne doit contenir que des chiffres")]
        //[Range(0, 14.00, ErrorMessage = "Cette valeur ne peux excéder 14")]
        //[Range(typeof(decimal), "0", "14")]



        [Display(Name = "pH H2O")]
        [Column(TypeName = "numeric")]
        [CustomValidation(typeof(ValidationMethods), "ValidateGreaterOrEqualToZero")]
        public decimal? PH { get; set; }

        [Display(Name = "CEC")]
        [Column(TypeName = "numeric")]
        //[RegularExpression(@"^[0-9.]{0,15}$", ErrorMessage = "ce champ ne doit contenir que des chiffres")]

        public decimal? CEC { get; set; }

        [Display(Name = "SubsurfaceDrainage", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Sous sol drainant")]
        public bool? Drainage { get; set; }

        [Display(Name = "Realized", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Realisee N")]
        public int? FumureN { get; set; } // getFumureKPN

        [Display(Name = "P2O5")] // getFumureKPN
        public int? FumureP { get; set; }

        [Display(Name = "K2O")] // getFumureKPN
        public int? FumureK { get; set; }

        [Display(Name = "WidthRow", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Largeur du rang")]
        //[RegularExpression(@"^[0-9.]{0,15}$", ErrorMessage = "ce champ ne doit contenir que des chiffres")]
        [Column(TypeName = "numeric")] // cmbFoliaire
        public decimal? NbRang { get; set; }

        // champ utiliser uniquement pour le displayname
        [NotMapped]
        [Display(Name = "NumberFlowersMeterSquare", ResourceType = typeof(Resources.Resources))]//[Display(Name = "Nombre de fleurs par m2"
        public string NbrFleurs { get; set; }

        [Display(Name = "HeightCanopy", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Hauteur de la canopée")]
        //[RegularExpression(@"^[0-9.]{0,15}$", ErrorMessage = "ce champ ne doit contenir que des chiffres")]
        [Column(TypeName = "numeric")] // cmbFoliaire
        public decimal? HauteurCanope { get; set; }

        // champ utiliser uniquement pour le displayname
        [NotMapped]
        [Display(Name = "NumberFruitPerMeterSquare", ResourceType = typeof(Resources.Resources))]// [Display(Name = "Nombre de fruits par m2"
        public string NbrFruits { get; set; }

        [Display(Name = "RealPluvio", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Pluviométrie réelle")]
        //[RegularExpression(@"^[0-9.]{0,15}$", ErrorMessage = "ce champ ne doit contenir que des chiffres")]
        [Column(TypeName = "numeric")] // cmbPluvio GetComboNumeric
        public decimal? PluvioReel { get; set; }

        [Display(Name = "SummerPluvio", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Pluviométrie estivale")]
        //[RegularExpression(@"^[0-9.]{0,15}$", ErrorMessage = "ce champ ne doit contenir que des chiffres")]
        [Column(TypeName = "numeric")] // cmbPluvio GetComboNumeric
        public decimal? Pluvio2 { get; set; }

        [Display(Name = "BringMO", ResourceType = typeof(Resources.Resources))] // [Display(Name = "BringMO")] // oui/non
        public bool? ApportMO { get; set; }

        [Display(Name = "PercentActiveLimestone", ResourceType = typeof(Resources.Resources))]
       // [RegularExpression(@"^[0-9.]{0,15}$", ErrorMessage = "ce champ ne doit contenir que des chiffres")]
        [Column(TypeName = "numeric")]
        public decimal? Calcaire { get; set; }

        [Display(Name = "Remarks", ResourceType = typeof(Resources.Resources))]
        [Column(TypeName = "text")]
        public string Remarque { get; set; }

        [Display(Name = "NoteDeficiency", ResourceType = typeof(Resources.Resources))]
        [Column(TypeName = "text")]
        public string RemarqueCarence { get; set; }

        [Display(Name = "TechnicalNote", ResourceType = typeof(Resources.Resources))]
        [Column(TypeName = "text")]
        public string RemarqueTech { get; set; }

        [Display(Name = "BalanceSheet", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Bilan")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Bilan> Bilans { get; set; }

        [Display(Name = "Analyzes", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Analyses")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CarenceReleve> CarenceReleves { get; set; }

        [Display(Name = "Graft", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Greffe")]
        public virtual Greffe Greffe { get; set; }

        [Display(Name = "Parcel", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Parcelle")]
        public virtual Parcelle Parcelle { get; set; }

        [Display(Name = "CreatedBy", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Créé par")]
        public int? IDUserCreat { get; set; }

        [Display(Name = "ModifiedBy", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Modifié par")]
        public int? IDUserModif { get; set; }

        [Display(Name = "Created", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Créé")]
        [Column(TypeName = "smalldatetime")]
        [DisplayFormat(DataFormatString = "{0:dd MMMM yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? DatCreat { get; set; }

        [Display(Name = "Modified", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Modifié")]
        [Column(TypeName = "smalldatetime")]
        [DisplayFormat(DataFormatString = "{0:dd MMMM yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? DatModif { get; set; }

        [Display(Name = "Customized", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Personnalisé")]
        public bool IsPerso { get; set; }

        [Display(Name = "Activated", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Activé")]
        public bool IsActive { get; set; }

        [Display(Name = "Initial", ResourceType = typeof(Resources.Resources))] // initiale
        public bool IsInitial { get; set; }

        [Display(Name = "BalanceCalculated", ResourceType = typeof(Resources.Resources))] //  [Display(Name = "Bilan calculé")]
        public bool IsBilanCalculated { get; set; }

        [Display(Name = "Customer", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Client")]
        public string Client => Parcelle.Client.Prenom + " " + Parcelle.Client.Nom;

        // COMBO
        [Display(Name = "TypeOfCut", ResourceType = typeof(Resources.Resources))] // GetComboEspece cmbTypeTaille [Display(Name = "Type de taille")]//
        public int? RefComboTypeTaille { get; set; }

        [Display(Name = "Age", ResourceType = typeof(Resources.Resources))] // cmbAge // GetCombo
        public int? RefComboAge { get; set; }

        [Display(Name = "TypeWine", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Type Vin")]
        public int? RefComboTypeVin { get; set; }

        [Display(Name = "Rootstock", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Porte Greffe")] // GetGreffe
        public int RefComboGreffe { get; set; }

        [Display(Name = "VisualInspectionVigor", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Contrôle visuel de la vigueur")] // cmbVigueurVisuelle // getCombo
        public int? RefComboVigueurVisuelle { get; set; }

        [Display(Name = "Coulure", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Coulure")]// cmbDysfonctionnement GetCombo
        public int? RefComboCoulure { get; set; }

        [Display(Name = "Chlorosis", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Chlorose")] // cmbDysfonctionnement GetCombo
        public int? RefComboChlorose { get; set; }

        [Display(Name = "StagesConservations", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Stades conservations")] // cmbTypeConservation ComboEspece
        public int? RefComboStadeConservation { get; set; }

        [Display(Name = "Conservation", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Conservation")] // cmbStadeConservation Combo
        public int? RefComboTypeConservation { get; set; }

        [Display(Name = "Millerandage", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Millerandage")]
        public int? RefComboMillerandage { get; set; }

        [Display(Name = "Health", ResourceType = typeof(Resources.Resources))]//[Display(Name = "Sanitaire")] //
        public int? RefComboSanitaire { get; set; }

        [Display(Name = "WineCategory", ResourceType = typeof(Resources.Resources))]//[Display(Name = "Categorie de vin")] //
        public int? RefComboCategorieVin { get; set; }

        [Display(Name = "SatementConductedBy", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Relevé effectué par")] // cmbTypeUser // GetCombo
        public int RefComboTypeUser { get; set; }

       [Display(Name = "GrapeStalk", ResourceType = typeof(Resources.Resources))]// [Display(Name = "Rafle")] // 
        public int? RefComboRafle { get; set; }

        [Display(Name = "MainDeficiency", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Carence Principale")]
        public int? RefComboCarencePrincipale { get; set; }

        [Display(Name = "SecondaryDeficiency", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Carence Secondaire")]
        public int? RefComboCarenceSecondaire { get; set; }

        [Display(Name = "DeficiencyOther", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Carence Autre")]
        public int? RefComboCarenceAutre { get; set; }

        [Display(Name = "TypeAnalysis", ResourceType = typeof(Resources.Resources))]//[Display(Name = "Type Analyse")] // 
        public ICollection<CarenceType> CarenceType { get; set; }

        // check if there is cumulate
        [NotMapped]
        public bool hasCumul { get; set; }
    }


    public class ValidationMethods
    {
        public static ValidationResult ValidateGreaterOrEqualToZero(decimal? value, ValidationContext context)
        {
            bool isValid = true;

            if (value > 14)
            {
                isValid = false;
            }

            if (isValid)
            {
                return ValidationResult.Success;
            }
            else
            {
                return new ValidationResult(
                    string.Format("The field {0} must not be greater than 14.", context.MemberName),
                    new List<string>() { context.MemberName });
            }
        }
    }




}