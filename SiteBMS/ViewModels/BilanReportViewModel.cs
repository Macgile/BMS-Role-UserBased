﻿using SiteBMS.Models;
using System.Collections.Generic;
using System.Linq;

namespace SiteBMS.ViewModels
{
    public class BilanReportViewModel
    {
        public ICollection<EtapeBilan> Etape { get; set; }
        public ICollection<ProgramBilan> Programme { get; set; }
        public ICollection<BilanValues> Values { private get; set; }

        public ICollection<BilanValues> GetProduits(int? idProgramme, int idEtape)
        {
            var result = (from v in Values
                          where v.RefProgrammeNutrition == idProgramme && v.RefEtape == idEtape
                          select v).ToList();

            return result;
        }

        //Fonction pour la mise en forme des produits affichés par Bilan
        public static string DisplayProduct(BilanValues item, string template)
        {
            if (string.IsNullOrEmpty(template))
            {
                template = @"<div class='panel panel-default' id='item-{2}'>
                      <div class='panel-heading'><h5>{0}</h5></div>
                      <div class='panel-body'>{1}</div>
                    </div>";
            }

            var sResult = "";

            switch (item.PersoCase)
            {
                case 0:
                    //Bilan non personnalisé : IsPerso = 0
                    sResult += string.Format(template, item.Produit, item.Qte, item.ID, item.RefProduit, item.UnitProd);
                    break;

                case 1:
                    //Bilan personnalisé : IsPerso = 1 - Qté modifiée
                    sResult += string.Format(template, item.Produit, item.Qte , item.ID, item.RefProduit, item.UnitProd);
                    break;

                case 2:
                    //Bilan personnalisé : IsPerso = 1 - Produit supprimé -> valeur Qté NULL
                    sResult += string.Format(template, item.Produit, "", item.ID, item.RefProduit, item.UnitProd);
                    break;

                case 3:
                    //Bilan personnalisé : IsPerso = 1 - Produit ajouté -> valeur QtéInit NULL
                    sResult += string.Format(template, item.Produit, item.Qte, item.ID, item.RefProduit, item.UnitProd);
                    break;

                default:
                    sResult += string.Format(template, "", "", item.ID, item.RefProduit, "");
                    break;
            }

            return sResult;
        }
    }
}