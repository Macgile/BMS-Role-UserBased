﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SiteBMS.Models;

namespace SiteBMS.ViewModels
{
    public class DistributeurViewModel
    {
        public int ID { get; set; }

        [Required]
        [Display(Name = "Nom")]
        [StringLength(50)]
        public string Nom { get; set; }

        [Display(Name = "Adresse")]
        public string Adr1 { get; set; }

        [Display(Name = "Complément adresse")]
        public string Adr2 { get; set; }

        [Display(Name = "Code postal")]
        public string CodePostal { get; set; }

        [Display(Name = "Ville")]
        public string Ville { get; set; }

        [Display(Name = "Téléphone")]
        public string Tel1 { get; set; }

        [Display(Name = "Portable")]
        public string Tel2 { get; set; }

        [Display(Name = "Fax")]
        public string Fax { get; set; }

        [Display(Name = "Email")]
        public string Mel { get; set; }

        [Display(Name = "Activé")]
        public bool IsActive { get; set; }

        [Display(Name = "Créé")]
        [Column(TypeName = "smalldatetime")]
        [DisplayFormat(DataFormatString = "{0:dd MMMM yyyy}")]
        public DateTime IDDatCreat { get; set; }

        [Display(Name = "Modifié")]
        [Column(TypeName = "smalldatetime")]
        [DisplayFormat(DataFormatString = "{0:dd MMMM yyyy}")]
        public DateTime IDDatModif { get; set; }

        //public int Pays_ID { get; set; }

        //[Display(Name = "Pays")]
        //public string Pay { get; set; }
        [Display(Name = "Pays")]
        [NotMapped]
        public string Pays { get; set; }

        [Display(Name = "Pays")]
        public int RefComboPays { get; set; }

        public int IDUserCreat { get; set; }

        public int IDUserModif { get; set; }

        [Display(Name = "Technicien BMS")]
        public int RefTechBMS { get; set; }

        [NotMapped]
        public int Old_Technicien { get; set; }

        // check if there is cumulate
        [NotMapped]
        public bool hasCumul { get; set; }

        // add utilisateur of bilan
        [NotMapped]
        public Utilisateur Utilisateur { get; set; }

        // list of users web bilan for current distributor
        [NotMapped]
        public IEnumerable<Utilisateur> Utilisateurs { get; set; }

        [NotMapped]
        public ICollection<UtilisateurViewModel> UtilisateursVm { get; set; }

        [NotMapped]
        public int UserCount { get; set; }

    }
}