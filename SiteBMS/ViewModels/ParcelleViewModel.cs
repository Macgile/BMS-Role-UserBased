﻿using SiteBMS.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web.Mvc;

namespace SiteBMS.ViewModels
{
    public class ParcelleViewModel
    {
        public int ID { get; set; }

        public int RefClient { get; set; }

        [Required]
        [Display(Name = "NameParcel", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Nom de la Parcelle")]
        [StringLength(50)]
        public string Nom { get; set; }

        [Display(Name = "Surface", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Surface")]
        public float Surface { get; set; }

        [Display(Name = "Variety", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Variété")]
        public int RefVariete { get; set; }

        [Display(Name = "ClimateZone", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Zone climatique")]
        public int RefComboZoneClimatique { get; set; }

        [Display(Name = "TypeIrrigation", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Type irrigation")]
        public int RefComboTypeIrrigation { get; set; }

        [Display(Name = "ProductionMode", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Mode de production")]
        public int? RefComboModeProduction { get; set; }

        [Display(Name = "CreatedBy", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Créé par")]
        public int IDUserCreat { get; set; }

        [Display(Name = "ModifiedBy", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Modifié par")]
        public int IDUserModif { get; set; }

        [Display(Name = "CreatedThe", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Créé le")]
        [DisplayFormat(DataFormatString = "{0:dd MMMM yyyy}", ApplyFormatInEditMode = true)]
        [Column(TypeName = "smalldatetime")]
        public DateTime DatCreat { get; set; }

        [Display(Name = "Modified", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Modifié le")]
        [DisplayFormat(DataFormatString = "{0:dd MMMM yyyy}", ApplyFormatInEditMode = true)]
        [Column(TypeName = "smalldatetime")]
        public DateTime DatModif { get; set; }

        [Display(Name = "Activated", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Activé")]
        public bool IsActive { get; set; }

        [Display(Name = "Customer", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Client")]
        public virtual Client Client { get; set; }

        [Display(Name = "Variety", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Variété")]
        public virtual Variete Variete { get; set; }

        [Display(Name = "StatementS", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Relevés")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Releve> Releves { get; set; }

        //public int RelevesCount1 => (Releves.Count > 0) ? Releves.Count / 2 : 0;

        public int RelevesCount
        {
            get
            {
                return Releves.Where(r => r.IsInitial == false).Select(r => r.ID).Count();
            }
        }

        [Display(Name = "Specie", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Espèce")]
        public int RefComboEspece { get; set; }

        [Column(TypeName = "Espèces")]
        [Display(Name = "SpecieS", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Espèces")]
        public virtual IEnumerable<SelectListItem> Especes { get; set; }

        public string NomEspece { get; set; }

        // check if there is cumulate
        [NotMapped]
        public bool hasCumul { get; set; }

    }
}