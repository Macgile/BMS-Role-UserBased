﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using SiteBMS.Models;

namespace SiteBMS.ViewModels
{
    public class ClientViewModel
    {
        //public int Page { get; set; }

        protected readonly int[] especes = { 11, 12, 13 }; // aucun, mais, cereal

        [Key]
        public int ID { get; set; }

        public string FullName => Utils.UcFirst(Prenom) + " " + Utils.UcFirst(Nom);

        /*
        [Display(Name = "Age", ResourceType = typeof(Resources.Resources))]
        [Required(ErrorMessageResourceType = typeof(Resources.Resources),
                  ErrorMessageResourceName = "AgeRequired")]
        [Range(0, 130, ErrorMessageResourceType = typeof(Resources.Resources),
                       ErrorMessageResourceName = "AgeRange")]
        */

        [Required]
        [Display(Name = "Name", ResourceType = typeof(Resources.Resources))]//[Display(Nom = "Prénom")]
        public string Nom { get; set; }

        [Required]
        [Display(Name = "FirstName", ResourceType = typeof(Resources.Resources))]//[Display(Name = "Prénom", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Prénom")]
        public string Prenom { get; set; }

        [Display(Name = "Society", ResourceType = typeof(Resources.Resources))] //[Display(Name = "Société", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Société")]
        public string Societe { get; set; }

        [Required]
        [Display(Name = "Address", ResourceType = typeof(Resources.Resources))]//[Display(Name = "Adresse", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Adresse")]
        public string Adr1 { get; set; }

        [Display(Name = "AdditionalAddress", ResourceType = typeof(Resources.Resources))]// [Display(Name = "Complément adresse", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Complément adresse")] //
        public string Adr2 { get; set; }

        [Required]
        [Display(Name = "ZipCode", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Code postal", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Code postal")]
        [RegularExpression(@"^[0-9-a-zA-Z]{0,15}$", ErrorMessage = "ce champ ne doit contenir que des chiffres")]
        public string CodePostal { get; set; }

        [Required]
        [Display(Name = "City", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Ville")]
        public string Ville { get; set; }

        [Display(Name = "PhoneNumber", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Téléphone")]
        [RegularExpression(@"^[0-9\s+]{0,20}$", ErrorMessage = "ce champ ne doit contenir que des chiffres")]
        public string Tel1 { get; set; }

        [Display(Name = "NumberGSM", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Portable")]
        [RegularExpression(@"^[0-9\s+]{0,15}$", ErrorMessage = "ce champ ne doit contenir que des chiffres")]
        public string Tel2 { get; set; }

        [Display(Name = "FaxNumber", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Fax")]
        [RegularExpression(@"^[0-9\s+]{0,20}$", ErrorMessage = "ce champ ne doit contenir que des chiffres")]
        public string Fax { get; set; }

        [Required]
        [Display(Name = "Email", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Email")]
        [RegularExpression(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$", ErrorMessage = "Email n'est pas au bon format")]
        public string Mel { get; set; }

        [Display(Name = "Activate", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Activé")]
        public bool IsActive { get; set; }

        [Display(Name = "Created", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Créé")]
        [Column(TypeName = "smalldatetime")]
        [DisplayFormat(DataFormatString = "{0:dd MMMM yyyy}", ApplyFormatInEditMode = true)]
        public DateTime DatCreat { get; set; }

        [Display(Name = "Modified", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Modifié")]
        [Column(TypeName = "smalldatetime")]
        [DisplayFormat(DataFormatString = "{0:dd MMMM yyyy}", ApplyFormatInEditMode = true)]
        public DateTime DatModif { get; set; }

        [Display(Name = "Country", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Pays")]
        [NotMapped]
        public string Pays { get; set; }

        [Display(Name = "Country", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Pays")]
        public int RefComboPays { get; set; }

        // liste des distributeurs pour choix ajout distributeur
        public ICollection<Distributeur> ListDistributeurs { get; set; }

        // for selected Parcelle
        public int SelectedParcelle { get; set; }

        [Display(Name = "ParcelS", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Parcelles")]
        public ICollection<Parcelle> Parcelles { get; set; }

        [Display(Name = "ParcelS", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Parcelles")]
        [NotMapped]
        public int NbrParcelles
        {
            get
            {
                // TODO : Array Espece
                return Parcelles?.Where(p => !especes.Contains(p.RefComboEspece)).Select(r => r).Count() ?? 0;
                //return Parcelles?.Count() ?? 0;
            }
        }

        [Display(Name = "DistributorS", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Distributeurs")]
        [NotMapped]
        public int Distributeur_ID { get; set; }

        // distributeur lié au client de la table Distributeur_Client
        [Display(Name = "DistributorS", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Distributeurs")]
        public ICollection<Distributeur> Distributeurs { get; set; }

        // Mandatory fields
        [Required]
        [Display(Name = "MethodMarketing", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Commercialisation")]
        public int RefComboTypeCommercialisation { get; set; }

        public int IDUserCreat { get; set; }

        public int IDUserModif { get; set; }

        [Display(Name = "BMSTechnician", ResourceType = typeof(Resources.Resources))] // [Display(Name = "Techniciens BMS")]
        public int RefTechBMS { get; set; }

        // check if there is cumulate
        [NotMapped]
        public bool hasCumul { get; set; }

        [NotMapped]
        public ICollection<UtilisateurViewModel> UtilisateursVm { get; set; }

        [NotMapped]
        public int UserCount { get; set; }
    }
}