﻿var pattern = /\((kg|l)\)/i;

function ChangeProduit(obj) {
    // replace unite for input quantite
    var val   = $("#" + obj.id + " option:selected").text().replace(/\s*/gi, ""); // replace all spaces
    var unite = pattern.exec(val) || [""]; // retrieve unity (kg/L)
    $(".modal-body #unite").text(unite[0].replace("(", "").replace(")", ""));
}

$(document).ready(function () {
    
    // height size
    var boxes     = $(".row-panel");
    var maxHeight = Math.max.apply(Math, boxes.map(function () {
        return $(this).height();
    }).get());

    boxes.height(maxHeight);

    $("#dropdownUnity li a").click(function () {
        var btn = $("#btn-unity");
        btn.html($(this).text() + ' <span class="caret"></span>');
        btn.val($(this).data('value'));
    });

    // customize modal, check if quantity is not null and disable submit button 
    $(document).on("keyup", "#PersoModal #quantite", function () {
        // var value = $(this).val();
        if ($(this).val().length > 0) {
            $("#PersoModal #submit-perso").attr("disabled", false);
        } else {
            $("#PersoModal #submit-perso").attr("disabled", true);
        }
    });

    // add modal, check if qte is set else disable submit button
    $(document).on("keyup", "#add-modal #add-quantity", function () {

        var list = $("#add-modal #add-listProduits").val();

        if ($(this).val().length > 0 && list.length > 0) {
            $("#add-modal #submit-add").attr("disabled", false);
        } else {
            $("#add-modal #submit-add").attr("disabled", true);
        }
    });

    // add modal, check if qte is set else disable submit button
    $(document).on("change", "#add-modal #add-listProduits", function () {

        var qte = $("#add-modal #add-quantity").val();

        if ($(this).val().length > 0 && qte.length > 0) {
            $("#add-modal #submit-add").attr("disabled", false);
        } else {
            $("#add-modal #submit-add").attr("disabled", true);
        }
    });
    
    // submit-add add-quantity
    $(document).on("change", "#PersoModal #listProduits", function () {
        

        var value = $(this).val();
        var qte = $("#PersoModal #quantite").val();

        console.log("change listProduits : " + value + " " + qte);

        if (value.length > 0 && qte.length > 0) {
            $("#PersoModal #submit-perso").attr("disabled", false);
        }
    });

    /*
     * MODAL SUBMIT ACTION
     */

    // submit customize product
    $(document).on("click", "#submit-perso", function () {
        var idProduct = $("#listProduits").val();
        var id        = $("#form-isPerso #item-id").val();
        var quantite  = $("#form-isPerso #quantite").val();

        $.ajax({
            url: "/BilanReport/UpdateProduct",
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset =utf-8",
            data: JSON.stringify({ id: id, idProduct: idProduct, quantite: quantite }),
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                if (data > 0) {
                    window.location.href = window.location.href;
                    // set new values to item HTML
                    $("#e-" + id).data("product", idProduct);
                    $("#d-" + id).data("product", idProduct);
                    var obj     = $("#listProduits option:selected").text();
                    var product = obj.replace(/\(\s?[Lkg]*\s?\)/gi, ""); // replace unity/parentheses
                    var unity   = $("#unite").text();

                    $("#product-" + id).text(product);
                    $("#quantite-" + id).text(quantite + unity);
                }
            },
            error: function (xhr) {
                // unchanged
                console.log("error:" + xhr.responseText);
            }
        });


        $("#PersoModal").modal("hide");
    });

    // submit add product
    $(document).on("click", "#submit-add", function () {

        var idProduct = $("#form-add #add-listProduits").val();
        var quantite = $("#form-add #add-quantity").val().replace(",", ".");


        if (idProduct.length < 1 || quantite.length < 1) {
            alert("Veuillez renseigner les champs");
            return;
        }


        var releveId  = $("#form-add #releve-id").val();
        var program   = $("#form-add #program-id").val();
        var etape     = $("#form-add #etape-id").val();

        var jsonData = JSON.stringify({
            idProduct: idProduct, quantite: quantite,
            idReleve: releveId, idProgram: program, idEtape: etape
        });

        $.ajax({ 
            url: "/BilanReport/AddProduct",
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset =utf-8",
            data: jsonData,
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                 if (data > 0) {
                     window.location.href = window.location.href;
                }
            },
            error: function (xhr) {
                // unchanged
                console.log("error:" + xhr.responseText);
            }
        });

        $("#add-modal").modal("hide");
    });

    // submit delete product
    $(document).on("click", "#btn-delete", function () {
        // console.log("delete");
        var id = $("#form-delete #delete-id").val();

        $.ajax({
            url: "/BilanReport/DeleteProduct",
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset =utf-8",
            data: JSON.stringify({ id: id }),
            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                // console.log(data);
                if (data > 0) {
                    window.location.href = window.location.href;
                }
            },
            error: function (xhr) {
                // unchanged
                console.log("error:" + xhr.responseText);
            }
        });
        $("#delete-modal").modal("hide");
    });

    /*
     * SHOW MODAL ACTION
     */
    // show modal dialog customize product
    $(document).on("click", ".edit-link", function () {
        // get ids
        var id = $(this).data("id");
        var idProduit = $(this).data("product");

        var quantity = $("#quantite-" + id).text();  // retrieve quantity
        var unity = quantity.replace(/[0-9\.]*/, "");// replace digit and dot decimal
        quantity = quantity.replace(/(kg|L)/gi, ""); // remove unity and parenthesis

        $(".modal-body #unite").text(unity);
        $(".modal-body #item-id").val(id);
        $(".modal-body #quantite").val(quantity);
        $("#listProduits option[value =\"" + idProduit + "\"]").prop("selected", true);
    });

    // show modal dialog add product
    $(document).on("click", ".add-link", function () {
        // get ids
        var id = $(this).data("id");
        var releve = $(this).data("releve");
        var program = $(this).data("program");
        var etape = $(this).data("etape");

        $("#add-modal-body #item-id").val(id);
        $("#add-modal-body #releve-id").val(releve);
        $("#add-modal-body #program-id").val(program);
        $("#add-modal-body #etape-id").val(etape);
    });

    // show modal dialog delete product
    $(document).on("click", ".delete-link", function () {
        var id = $(this).data("id");
        var product = $("#product-" + id).text();
        $(".modal-body #delete-id").val(id);
        $(".modal-body #libelle-produit").text(product);
    });

    // clear data on Hide Modal dialog
    $('#add-modal').on('hidden.bs.modal', function (e) {

        $(this).find("#add-quantity").val(null);
        $(this).find("#add-listProduits").val(null);
        $(this).find("#submit-add").attr("disabled", true);
        $(this).removeData('bs.modal');
    });

    $('#PersoModal').on('hidden.bs.modal', function (e) {
        $(this).find("#quantite").val(null);
        $(this).removeData('bs.modal');
    });
    
});

document.getElementById("btnPrint").onclick = function () {
    window.print();
}
