﻿

function DisplayPassword(origin, srcId, destId) {

    if (origin.attr("disabled") === "disabled") return;

    var src      = $("#" + srcId);
    var dest     = $("#" + destId);
    var position = src.position();
    dest.text(src.val());

    dest.css({
        "background": src.css("background"),
        "top"       : position.top,
        "left"      : position.left,
        "width"     : src.innerWidth() + 2,
        "display"   : "block"
    });
}

function HiddePassword(dest) {
    $("#" + dest).css({"display": "none"});
}