﻿$(document).ready(function () {

    $(".open-modal").click(function () {
        $("#distributeurModal").modal('show');
    });
    $(".submit-modal").click(function () {

        var selectedId = $("#DistributeurSelected").val();
        if (selectedId === 0 || selectedId === "")
            alert("Veuillez choisir un distributeur.");
        else {
            var id = $("#ID").val();
            var name = $("#DistributeurSelected option:selected").text();

            $.ajax({
                url: "/Clients/AddDistributor",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({ id: id, selectedId: selectedId, name: name }), // user: { id: id, selectedId: selectedId }
                async: true,
                processData: false,
                cache: false,
                success: function (data) {
                    $("#DistributeursList").append($("<option>", {
                        value: selectedId,
                        text: name
                    }));

                    // refresh distributeur dropdownlist with selectpicker class
                    $(".distributeur-selectpicker").selectpicker("refresh");
                    $(".distributeur-selectpicker").selectpicker("val", selectedId);

                    // https://kilianvalkhof.com/jquerypulsate/
                    // pulse effect

                    // if selectpicker is used get it
                    var dropdownList = $("button[data-id='DistributeursList']");
                    
                    // else if not select DistributeursList
                    if (dropdownList.length === 0) {
                        dropdownList = $("#DistributeursList");
                    }
                    // pulsate effect
                    dropdownList.pulsate({ color: "#09f", reach: 10, repeat: 3 });

                    $("#DistributeurSelected option:eq(0)").prop("selected", true);
                    
                },
                error: function (xhr) {
                    console.log("error:" + xhr);
                }
            });
            $("#distributeurModal").modal('hide');
        }
    });
});
