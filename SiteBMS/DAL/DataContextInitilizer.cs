﻿using SiteBMS.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;

namespace SiteBMS.DAL
{
    public class DataContextInitializer : CreateDatabaseIfNotExists<DataContext>
    {
        protected override void Seed(DataContext context)
        {
            /*
            Language lang1 = new Language { Code = "fr-FR" };
            Language lang2 = new Language { Code = "en-EN" };

            context.Languages.Add(lang1);
            context.Languages.Add(lang2);
            context.SaveChanges();

            var langId1 = lang1.LanguageID;
            var langId2 = lang2.LanguageID;

            Role role1 = new Role { RoleName = "Admin" };
            Role role2 = new Role { RoleName = "User" };

            User user1 = new User
            {
                Username = "admin",
                Email = "admin@ymail.com",
                FirstName = "Admin",
                Password = "123456",
                IsActive = true,
                CreateDate = DateTime.UtcNow,
                LanguageID = langId1,
                Roles = new List<Role>()
            };

            User user2 = new User
            {
                Username = "user1",
                Email = "user1@ymail.com",
                FirstName = "User1",
                Password = "123456",
                IsActive = true,
                CreateDate = DateTime.UtcNow,
                LanguageID = langId2,
                Roles = new List<Role>()
            };

            user1.Roles.Add(role1);
            user2.Roles.Add(role2);

            context.Users.Add(user1);
            context.Users.Add(user2);

            context.SaveChanges();
            */
        }
    }
}