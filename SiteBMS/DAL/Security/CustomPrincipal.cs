﻿using System.Linq;

using System.Security.Principal;

namespace SiteBMS.DAL.Security
{
    public class CustomPrincipal : IPrincipal
    {
        private readonly DataContext db = new DataContext();
        public IIdentity Identity { get; private set; }

        public bool IsRole(RoleType role)
        {
            return EnumRole == role;
        }
   
        /// <summary>
        /// Check if the Role Name is in Role array string
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        public bool IsInRole(string role)
        {
            return Roles.Any(role.Contains);
        }


        /// <summary>
        /// check if User.EnumRole is in RoleType Array
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        public bool IsAuthorized(RoleType[] role)
        {
            var result = role.Contains(EnumRole);
            return result;
        }

        /// <summary>
        /// Check if Roletype role is current role of user
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        public bool IsAuthorized(RoleType role)
        {
            return EnumRole == role;
        }

        public string FullName => Utils.UcFirst(FirstName) + " " + Utils.UcFirst(LastName);

        public CustomPrincipal(string username)
        {
            Identity = new GenericIdentity(username);
        }

        public CustomPrincipal()
        {

        }


        public int UserId { get; set; }
        public int LanguageId { get; set; }
        public int PayId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Roles { private get; set; }
        public int RefRole { get; set; }
        public int RefId { get; set; }
        public string RoleName { get; set; }
        public int RoleId { get; set; }
        public RoleType EnumRole { get; set; }
        public int IdTech { get; set; }
        public int IdDistributeur { get; set; }
        public int IdClient { get; set; }

        //        Case BMSUser.RoleType.Technicien
        //            Me.IdTech = mUser.RefId


        //        Case BMSUser.RoleType.Distributeur
        //            Me.IdDistributeur = mUser.RefId


        //        Case BMSUser.RoleType.Client
        //            Me.IdClient = mUser.RefId



    }

    public class CustomPrincipalSerializeModel
    {
        public int UserId { get; set; }
        public int LanguageId { get; set; }
        public int PayId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Roles { get; set; }
        public int RefRole { get; set; }
        public int RefId { get; set; }
        public string RoleName { get; set; }
        public int RoleId { get; set; }
    }
}