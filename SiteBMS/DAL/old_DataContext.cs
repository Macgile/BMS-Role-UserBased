namespace SiteBMS.DAL
{
    using SiteBMS.Models;
    using System;
    using System.Data.Entity;
    using System.IO;

    public partial class DataContext : DbContext
    {
        //internal TextWriter Log;

        public DataContext()
            : base(((Environment.MachineName == "GILLES-PC") ? "Gilles" : "Phareway"))
        {
        }

        public virtual DbSet<Bilan> Bilans { get; set; }
        public virtual DbSet<BilanModele> BilanModeles { get; set; }
        public virtual DbSet<CarenceElement> CarenceElements { get; set; }
        public virtual DbSet<CarenceNorme> CarenceNormes { get; set; }
        public virtual DbSet<CarenceReleve> CarenceReleves { get; set; }
        public virtual DbSet<CarenceType> CarenceTypes { get; set; }
        public virtual DbSet<Client> Clients { get; set; }
        public virtual DbSet<CoefFumure> CoefFumures { get; set; }
        public virtual DbSet<Combo> Comboes { get; set; }
        public virtual DbSet<Combo_Element> Combo_Element { get; set; }
        public virtual DbSet<Distributeur> Distributeurs { get; set; }
        public virtual DbSet<Element> Elements { get; set; }
        public virtual DbSet<Espece> Especes { get; set; }
        public virtual DbSet<Etape> Etapes { get; set; }
        public virtual DbSet<Greffe> Greffes { get; set; }
        public virtual DbSet<Language> Languages { get; set; }
        public virtual DbSet<Langue> Langues { get; set; }
        public virtual DbSet<Parcelle> Parcelles { get; set; }
        public virtual DbSet<Pay> Pays { get; set; }
        public virtual DbSet<Produit> Produits { get; set; }
        public virtual DbSet<ProgrammeDetail> ProgrammeDetails { get; set; }
        public virtual DbSet<ProgrammeModele> ProgrammeModeles { get; set; }
        public virtual DbSet<ProgrammeNutrition> ProgrammeNutritions { get; set; }
        public virtual DbSet<Quota> Quotas { get; set; }
        public virtual DbSet<Releve> Releves { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<Technicien> Techniciens { get; set; }
        public virtual DbSet<Traduction> Traductions { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Utilisateur> Utilisateurs { get; set; }
        public virtual DbSet<Variete> Varietes { get; set; }
        public virtual DbSet<Combo_Element_Numeric> Combo_Element_Numeric { get; set; }
        public virtual DbSet<ProgrammeApplicationDirect> ProgrammeApplicationDirects { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Bilan>()
                .Property(e => e.Origine)
                .IsFixedLength();

            modelBuilder.Entity<CarenceElement>()
                .HasMany(e => e.CarenceNormes)
                .WithRequired(e => e.CarenceElement)
                .HasForeignKey(e => e.RefCarenceElement)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CarenceNorme>()
                .HasMany(e => e.CarenceReleves)
                .WithRequired(e => e.CarenceNorme)
                .HasForeignKey(e => e.RefCarenceNorme)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CarenceType>()
                .HasMany(e => e.CarenceNormes)
                .WithRequired(e => e.CarenceType)
                .HasForeignKey(e => e.RefType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Client>()
                .HasMany(e => e.Parcelles)
                .WithRequired(e => e.Client)
                .HasForeignKey(e => e.RefClient)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Client>()
                .HasMany(e => e.Distributeurs)
                .WithMany(e => e.Clients)
                .Map(m => m.ToTable("Distributeur_Client").MapLeftKey("RefClient").MapRightKey("RefDistributeur"));

            modelBuilder.Entity<Combo>()
                .HasMany(e => e.Combo_Element_Numeric)
                .WithRequired(e => e.Combo)
                .HasForeignKey(e => e.RefCombo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Combo>()
                .HasMany(e => e.Combo_Element)
                .WithRequired(e => e.Combo)
                .HasForeignKey(e => e.IDRefCombo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Element>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<Element>()
                .HasMany(e => e.Combo_Element)
                .WithRequired(e => e.Element)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Element>()
                .HasMany(e => e.Traductions)
                .WithRequired(e => e.Element)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Espece>()
                .HasMany(e => e.CarenceNormes)
                .WithRequired(e => e.Espece)
                .HasForeignKey(e => e.RefEspece)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Espece>()
                .HasMany(e => e.CoefFumures)
                .WithRequired(e => e.Espece)
                .HasForeignKey(e => e.RefEspece)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Espece>()
                .HasMany(e => e.Etapes)
                .WithRequired(e => e.Espece)
                .HasForeignKey(e => e.RefEspece)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Espece>()
                .HasMany(e => e.Greffes)
                .WithRequired(e => e.Espece)
                .HasForeignKey(e => e.RefEspece)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Espece>()
                .HasMany(e => e.ProgrammeNutritions)
                .WithRequired(e => e.Espece)
                .HasForeignKey(e => e.RefEspece)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Espece>()
                .HasMany(e => e.Varietes)
                .WithRequired(e => e.Espece)
                .HasForeignKey(e => e.RefEspece)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Espece>()
                .HasMany(e => e.Produits)
                .WithMany(e => e.Especes)
                .Map(m => m.ToTable("ProduitInterdit").MapLeftKey("RefEspece").MapRightKey("RefProduit"));

            modelBuilder.Entity<Etape>()
                .HasMany(e => e.Bilans)
                .WithRequired(e => e.Etape)
                .HasForeignKey(e => e.RefEtape)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Etape>()
                .HasMany(e => e.BilanModeles)
                .WithRequired(e => e.Etape)
                .HasForeignKey(e => e.RefEtape)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Etape>()
                .HasMany(e => e.ProgrammeDetails)
                .WithRequired(e => e.Etape)
                .HasForeignKey(e => e.RefEtape)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Etape>()
                .HasMany(e => e.Quotas)
                .WithRequired(e => e.Etape)
                .HasForeignKey(e => e.RefEtape)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Greffe>()
                .HasMany(e => e.Releves)
                .WithRequired(e => e.Greffe)
                .HasForeignKey(e => e.RefComboGreffe)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Language>()
                .Property(e => e.Code)
                .IsFixedLength();

            modelBuilder.Entity<Langue>()
                .HasMany(e => e.Traductions)
                .WithRequired(e => e.Langue)
                .HasForeignKey(e => e.RefLangue)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Parcelle>()
                .HasMany(e => e.Releves)
                .WithRequired(e => e.Parcelle)
                .HasForeignKey(e => e.RefParcelle);

            modelBuilder.Entity<Pay>()
                .Property(e => e.Name)
                .IsFixedLength();

            modelBuilder.Entity<Pay>()
                .Property(e => e.Flag)
                .IsFixedLength();

            modelBuilder.Entity<Pay>()
                .HasMany(e => e.Clients)
                .WithRequired(e => e.Pay)
                .HasForeignKey(e => e.Pays_ID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Pay>()
                .HasMany(e => e.Distributeurs)
                .WithRequired(e => e.Pay)
                .HasForeignKey(e => e.Pays_ID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Produit>()
                .Property(e => e.UnitProd)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Produit>()
                .HasMany(e => e.Bilans)
                .WithRequired(e => e.Produit)
                .HasForeignKey(e => e.RefProduit)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Produit>()
                .HasMany(e => e.BilanModeles)
                .WithRequired(e => e.Produit)
                .HasForeignKey(e => e.RefProduit)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Produit>()
                .HasMany(e => e.ProgrammeDetails)
                .WithRequired(e => e.Produit)
                .HasForeignKey(e => e.RefProduit)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Produit>()
                .HasMany(e => e.Quotas)
                .WithRequired(e => e.Produit)
                .HasForeignKey(e => e.RefProduit)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ProgrammeModele>()
                .Property(e => e.Nom)
                .IsUnicode(false);

            modelBuilder.Entity<ProgrammeModele>()
                .HasMany(e => e.ProgrammeDetails)
                .WithRequired(e => e.ProgrammeModele)
                .HasForeignKey(e => e.RefProgrammeModele)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ProgrammeModele>()
                .HasMany(e => e.ProgrammeApplicationDirects)
                .WithRequired(e => e.ProgrammeModele)
                .HasForeignKey(e => e.RefProgrammeModele)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ProgrammeNutrition>()
                .HasMany(e => e.Bilans)
                .WithRequired(e => e.ProgrammeNutrition)
                .HasForeignKey(e => e.RefProgrammeNutrition)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ProgrammeNutrition>()
                .HasMany(e => e.BilanModeles)
                .WithRequired(e => e.ProgrammeNutrition)
                .HasForeignKey(e => e.RefProgrammeNutrition)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ProgrammeNutrition>()
                .HasMany(e => e.ProgrammeDetails)
                .WithRequired(e => e.ProgrammeNutrition)
                .HasForeignKey(e => e.RefProgrammeNutrition)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ProgrammeNutrition>()
                .HasMany(e => e.Quotas)
                .WithRequired(e => e.ProgrammeNutrition)
                .HasForeignKey(e => e.RefProgrammeNutrition)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Releve>()
                .Property(e => e.PluvioReel)
                .HasPrecision(9, 2);

            modelBuilder.Entity<Releve>()
                .Property(e => e.Calcaire)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Releve>()
                .Property(e => e.Remarque)
                .IsUnicode(false);

            modelBuilder.Entity<Releve>()
                .Property(e => e.RemarqueCarence)
                .IsUnicode(false);

            modelBuilder.Entity<Releve>()
                .Property(e => e.RemarqueTech)
                .IsUnicode(false);

            modelBuilder.Entity<Releve>()
                .HasMany(e => e.Bilans)
                .WithRequired(e => e.Releve)
                .HasForeignKey(e => e.RefReleve)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Releve>()
                .HasMany(e => e.CarenceReleves)
                .WithRequired(e => e.Releve)
                .HasForeignKey(e => e.RefReleve)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Role>()
                .HasMany(e => e.Users)
                .WithMany(e => e.Roles)
                .Map(m => m.ToTable("UserRole").MapLeftKey("RoleId").MapRightKey("UserId"));

            modelBuilder.Entity<Technicien>()
                .HasMany(e => e.Clients)
                .WithRequired(e => e.Technicien)
                .HasForeignKey(e => e.RefTechBMS)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Variete>()
                .HasMany(e => e.Parcelles)
                .WithRequired(e => e.Variete)
                .HasForeignKey(e => e.RefVariete)
                .WillCascadeOnDelete(false);
        }
    }
}