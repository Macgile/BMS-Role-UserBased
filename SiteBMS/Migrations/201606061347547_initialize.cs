namespace SiteBMS.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initialize : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BilanModele",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        RefProgrammeNutrition = c.Int(nullable: false),
                        RefEtape = c.Int(nullable: false),
                        RefProduit = c.Int(nullable: false),
                        Qte = c.Decimal(precision: 18, scale: 2, storeType: "numeric"),
                        NbApplication = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Etape", t => t.RefEtape)
                .ForeignKey("dbo.Produit", t => t.RefProduit)
                .ForeignKey("dbo.ProgrammeNutrition", t => t.RefProgrammeNutrition)
                .Index(t => t.RefProgrammeNutrition)
                .Index(t => t.RefEtape)
                .Index(t => t.RefProduit);
            
            CreateTable(
                "dbo.Etape",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        RefEspece = c.Int(nullable: false),
                        Nom_T = c.Int(nullable: false),
                        ImgEtap = c.Binary(storeType: "image"),
                        Description_T = c.Int(),
                        Ordre = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Espece", t => t.RefEspece)
                .Index(t => t.RefEspece);
            
            CreateTable(
                "dbo.Bilan",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        RefReleve = c.Int(nullable: false),
                        RefProgrammeNutrition = c.Int(nullable: false),
                        RefEtape = c.Int(nullable: false),
                        RefProduit = c.Int(nullable: false),
                        Qte = c.Decimal(precision: 18, scale: 2, storeType: "numeric"),
                        QteInitial = c.Decimal(precision: 18, scale: 2, storeType: "numeric"),
                        NbApplication = c.Int(nullable: false),
                        IsValide = c.Boolean(nullable: false),
                        IsPerso = c.Boolean(nullable: false),
                        Origine = c.String(maxLength: 3, fixedLength: true),
                        PersoBy = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Produit", t => t.RefProduit)
                .ForeignKey("dbo.Releve", t => t.RefReleve)
                .ForeignKey("dbo.ProgrammeNutrition", t => t.RefProgrammeNutrition)
                .ForeignKey("dbo.Etape", t => t.RefEtape)
                .Index(t => t.RefReleve)
                .Index(t => t.RefProgrammeNutrition)
                .Index(t => t.RefEtape)
                .Index(t => t.RefProduit);
            
            CreateTable(
                "dbo.Produit",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Nom_T = c.Int(nullable: false),
                        UnitProd = c.String(nullable: false, maxLength: 2, fixedLength: true, unicode: false),
                        Bio = c.Boolean(nullable: false),
                        Pack1 = c.Decimal(precision: 18, scale: 2, storeType: "numeric"),
                        Pack2 = c.Decimal(precision: 18, scale: 2, storeType: "numeric"),
                        IsActive = c.Boolean(nullable: false),
                        Prix = c.Decimal(nullable: false, precision: 18, scale: 2, storeType: "numeric"),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Espece",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Nom_T = c.Int(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.CarenceNorme",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        RefCarenceElement = c.Int(nullable: false),
                        RefEspece = c.Int(nullable: false),
                        NormeMax = c.Decimal(precision: 18, scale: 2, storeType: "numeric"),
                        NormeMin = c.Decimal(precision: 18, scale: 2, storeType: "numeric"),
                        RefType = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.CarenceElement", t => t.RefCarenceElement)
                .ForeignKey("dbo.CarenceType", t => t.RefType)
                .ForeignKey("dbo.Espece", t => t.RefEspece)
                .Index(t => t.RefCarenceElement)
                .Index(t => t.RefEspece)
                .Index(t => t.RefType);
            
            CreateTable(
                "dbo.CarenceElement",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Nom_T = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.CarenceReleve",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        RefCarenceNorme = c.Int(nullable: false),
                        RefReleve = c.Int(nullable: false),
                        RefComboCarence = c.Int(),
                        ValeurAnalyseCarence = c.Decimal(precision: 18, scale: 2, storeType: "numeric"),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Releve", t => t.RefReleve)
                .ForeignKey("dbo.CarenceNorme", t => t.RefCarenceNorme)
                .Index(t => t.RefCarenceNorme)
                .Index(t => t.RefReleve);
            
            CreateTable(
                "dbo.Releve",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Nom = c.String(nullable: false, maxLength: 15),
                        RefParcelle = c.Int(nullable: false),
                        DateReleve = c.DateTime(nullable: false, storeType: "smalldatetime"),
                        ObjectifRendement = c.Decimal(nullable: false, precision: 18, scale: 2, storeType: "numeric"),
                        ObjectifRendementN_1 = c.Decimal(nullable: false, precision: 18, scale: 2, storeType: "numeric"),
                        ObjectifCalibre = c.Decimal(precision: 18, scale: 2, storeType: "numeric"),
                        ObjectifCalibreN_1 = c.Decimal(precision: 18, scale: 2, storeType: "numeric"),
                        ObjectifDegre = c.Decimal(precision: 18, scale: 2, storeType: "numeric"),
                        ObjectifDegreN_1 = c.Decimal(precision: 18, scale: 2, storeType: "numeric"),
                        Alternance = c.Boolean(),
                        RefComboTypeVin = c.Int(),
                        RefComboGreffe = c.Int(nullable: false),
                        RefComboVigueurVisuelle = c.Int(),
                        MatiereOrganique = c.Decimal(precision: 18, scale: 2, storeType: "numeric"),
                        PH = c.Decimal(precision: 18, scale: 2, storeType: "numeric"),
                        CEC = c.Decimal(precision: 18, scale: 2, storeType: "numeric"),
                        Drainage = c.Boolean(),
                        FumureN = c.Int(),
                        FumureP = c.Int(),
                        FumureK = c.Int(),
                        RefComboAge = c.Int(),
                        NbRang = c.Decimal(precision: 18, scale: 2, storeType: "numeric"),
                        HauteurCanope = c.Decimal(precision: 18, scale: 2, storeType: "numeric"),
                        RefComboTypeTaille = c.Int(),
                        PluvioReel = c.Decimal(precision: 9, scale: 2, storeType: "numeric"),
                        RefComboCoulure = c.Int(),
                        RefComboChlorose = c.Int(),
                        RefComboStadeConservation = c.Int(),
                        RefComboTypeConservation = c.Int(),
                        ApportMO = c.Boolean(),
                        RefComboMillerandage = c.Int(),
                        RefComboSanitaire = c.Int(),
                        RefComboCategorieVin = c.Int(),
                        Calcaire = c.Decimal(precision: 10, scale: 2, storeType: "numeric"),
                        RefComboRafle = c.Int(),
                        RefComboCarencePrincipale = c.Int(),
                        RefComboCarenceSecondaire = c.Int(),
                        RefComboCarenceAutre = c.Int(),
                        Remarque = c.String(unicode: false, storeType: "text"),
                        RemarqueCarence = c.String(unicode: false, storeType: "text"),
                        RemarqueTech = c.String(unicode: false, storeType: "text"),
                        Pluvio2 = c.Decimal(precision: 18, scale: 2, storeType: "numeric"),
                        IDUserCreat = c.Int(),
                        IDUserModif = c.Int(),
                        DatCreat = c.DateTime(),
                        DatModif = c.DateTime(),
                        IsPerso = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        IsInitial = c.Boolean(nullable: false),
                        IsBilanCalculated = c.Boolean(nullable: false),
                        RefComboTypeUser = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Greffe", t => t.RefComboGreffe)
                .ForeignKey("dbo.Parcelle", t => t.RefParcelle, cascadeDelete: true)
                .Index(t => t.RefParcelle)
                .Index(t => t.RefComboGreffe);
            
            CreateTable(
                "dbo.Greffe",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        RefEspece = c.Int(nullable: false),
                        Nom_T = c.Int(nullable: false),
                        VigGref = c.Decimal(precision: 18, scale: 2, storeType: "numeric"),
                        IPC = c.Decimal(precision: 18, scale: 2, storeType: "numeric"),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Espece", t => t.RefEspece)
                .Index(t => t.RefEspece);
            
            CreateTable(
                "dbo.Parcelle",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        RefClient = c.Int(nullable: false),
                        Nom = c.String(nullable: false, maxLength: 50),
                        Surface = c.Single(nullable: false),
                        RefComboEspece = c.Int(nullable: false),
                        RefVariete = c.Int(nullable: false),
                        RefComboZoneClimatique = c.Int(nullable: false),
                        RefComboTypeIrrigation = c.Int(nullable: false),
                        RefComboModeProduction = c.Int(),
                        IDUserCreat = c.Int(nullable: false),
                        IDUserModif = c.Int(nullable: false),
                        DatCreat = c.DateTime(nullable: false, storeType: "smalldatetime"),
                        DatModif = c.DateTime(nullable: false, storeType: "smalldatetime"),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Client", t => t.RefClient)
                .ForeignKey("dbo.Variete", t => t.RefVariete)
                .Index(t => t.RefClient)
                .Index(t => t.RefVariete);
            
            CreateTable(
                "dbo.Client",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Nom = c.String(nullable: false, maxLength: 50),
                        Prenom = c.String(maxLength: 50),
                        Societe = c.String(maxLength: 50),
                        RefTechBMS = c.Int(nullable: false),
                        Adr1 = c.String(maxLength: 50),
                        Adr2 = c.String(maxLength: 50),
                        CodePostal = c.String(maxLength: 10),
                        Ville = c.String(maxLength: 70),
                        RefComboPays = c.Int(nullable: false),
                        Tel1 = c.String(maxLength: 30),
                        Tel2 = c.String(maxLength: 30),
                        Fax = c.String(maxLength: 30),
                        Mel = c.String(maxLength: 50),
                        RefComboTypeCommercialisation = c.Int(nullable: false),
                        IDUserCreat = c.Int(nullable: false),
                        IDUserModif = c.Int(nullable: false),
                        DatCreat = c.DateTime(nullable: false, storeType: "smalldatetime"),
                        DatModif = c.DateTime(nullable: false, storeType: "smalldatetime"),
                        IsActive = c.Boolean(nullable: false),
                        Pays_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Pays", t => t.Pays_ID)
                .ForeignKey("dbo.Technicien", t => t.RefTechBMS)
                .Index(t => t.RefTechBMS)
                .Index(t => t.Pays_ID);
            
            CreateTable(
                "dbo.Distributeur",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Nom = c.String(nullable: false, maxLength: 50),
                        Adr1 = c.String(maxLength: 50),
                        Adr2 = c.String(maxLength: 50),
                        CodePostal = c.String(maxLength: 50),
                        Ville = c.String(maxLength: 50),
                        RefComboPays = c.Int(nullable: false),
                        Tel1 = c.String(maxLength: 50),
                        Fax = c.String(maxLength: 50),
                        Tel2 = c.String(maxLength: 50),
                        Mel = c.String(maxLength: 50),
                        IsActive = c.Boolean(nullable: false),
                        IDUserCreat = c.Int(nullable: false),
                        IDUserModif = c.Int(nullable: false),
                        IDDatCreat = c.DateTime(nullable: false, storeType: "smalldatetime"),
                        IDDatModif = c.DateTime(nullable: false, storeType: "smalldatetime"),
                        RefTechBMS = c.Int(nullable: false),
                        Pays_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Pays", t => t.Pays_ID)
                .Index(t => t.Pays_ID);
            
            CreateTable(
                "dbo.Pays",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 10, fixedLength: true),
                        Flag = c.String(maxLength: 10, fixedLength: true),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Technicien",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        RefRespZone = c.Int(nullable: false),
                        RefComboPays = c.Int(nullable: false),
                        Tel1 = c.String(maxLength: 50),
                        Tel2 = c.String(maxLength: 50),
                        Fax = c.String(maxLength: 50),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Variete",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        RefEspece = c.Int(nullable: false),
                        Nom_T = c.Int(nullable: false),
                        RendtMoy = c.Decimal(precision: 18, scale: 2, storeType: "numeric"),
                        CalibreMoy = c.Decimal(precision: 18, scale: 2, storeType: "numeric"),
                        Vigueur = c.Decimal(precision: 18, scale: 2, storeType: "numeric"),
                        Precoce = c.Decimal(precision: 18, scale: 2, storeType: "numeric"),
                        Noor = c.Boolean(nullable: false),
                        IsAcive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Espece", t => t.RefEspece)
                .Index(t => t.RefEspece);
            
            CreateTable(
                "dbo.CarenceType",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Nom_T = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.CoefFumure",
                c => new
                    {
                        ID = c.Int(nullable: false),
                        Nom_T = c.Int(nullable: false),
                        Coef = c.Decimal(nullable: false, precision: 18, scale: 2),
                        RefEspece = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Espece", t => t.RefEspece)
                .Index(t => t.RefEspece);
            
            CreateTable(
                "dbo.ProgrammeNutrition",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Nom_T = c.Int(nullable: false),
                        RefEspece = c.Int(nullable: false),
                        Description_T = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Espece", t => t.RefEspece)
                .Index(t => t.RefEspece);
            
            CreateTable(
                "dbo.ProgrammeDetail",
                c => new
                    {
                        RefProgrammeModele = c.Int(nullable: false),
                        RefProgrammeNutrition = c.Int(nullable: false),
                        RefEtape = c.Int(nullable: false),
                        RefProduit = c.Int(nullable: false),
                        Qte = c.Decimal(precision: 18, scale: 2, storeType: "numeric"),
                    })
                .PrimaryKey(t => new { t.RefProgrammeModele, t.RefProgrammeNutrition, t.RefEtape, t.RefProduit })
                .ForeignKey("dbo.ProgrammeModele", t => t.RefProgrammeModele)
                .ForeignKey("dbo.ProgrammeNutrition", t => t.RefProgrammeNutrition)
                .ForeignKey("dbo.Produit", t => t.RefProduit)
                .ForeignKey("dbo.Etape", t => t.RefEtape)
                .Index(t => t.RefProgrammeModele)
                .Index(t => t.RefProgrammeNutrition)
                .Index(t => t.RefEtape)
                .Index(t => t.RefProduit);
            
            CreateTable(
                "dbo.ProgrammeModele",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Nom = c.String(nullable: false, maxLength: 50, unicode: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.ProgrammeApplicationDirect",
                c => new
                    {
                        RefVariete = c.Int(nullable: false),
                        ColReleve = c.String(nullable: false, maxLength: 50),
                        Desequilibre = c.Boolean(nullable: false),
                        RefProgrammeModele = c.Int(nullable: false),
                        Valeur = c.Decimal(precision: 18, scale: 2, storeType: "numeric"),
                    })
                .PrimaryKey(t => new { t.RefVariete, t.ColReleve, t.Desequilibre, t.RefProgrammeModele })
                .ForeignKey("dbo.ProgrammeModele", t => t.RefProgrammeModele)
                .Index(t => t.RefProgrammeModele);
            
            CreateTable(
                "dbo.Quota",
                c => new
                    {
                        RefProgrammeNutrition = c.Int(nullable: false),
                        RefEtape = c.Int(nullable: false),
                        RefProduit = c.Int(nullable: false),
                        Quota = c.Decimal(nullable: false, precision: 18, scale: 2, storeType: "numeric"),
                    })
                .PrimaryKey(t => new { t.RefProgrammeNutrition, t.RefEtape, t.RefProduit })
                .ForeignKey("dbo.ProgrammeNutrition", t => t.RefProgrammeNutrition)
                .ForeignKey("dbo.Produit", t => t.RefProduit)
                .ForeignKey("dbo.Etape", t => t.RefEtape)
                .Index(t => t.RefProgrammeNutrition)
                .Index(t => t.RefEtape)
                .Index(t => t.RefProduit);
            
            CreateTable(
                "dbo.Combo_Element",
                c => new
                    {
                        IDRefCombo = c.Int(nullable: false),
                        RefElement = c.Int(nullable: false),
                        RefEspece = c.Int(nullable: false),
                        Value = c.String(maxLength: 50),
                        Ordre = c.Int(),
                    })
                .PrimaryKey(t => new { t.IDRefCombo, t.RefElement, t.RefEspece })
                .ForeignKey("dbo.Combo", t => t.IDRefCombo)
                .ForeignKey("dbo.Element", t => t.RefElement)
                .Index(t => t.IDRefCombo)
                .Index(t => t.RefElement);
            
            CreateTable(
                "dbo.Combo",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        NomCombo = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Combo_Element_Numeric",
                c => new
                    {
                        RefCombo = c.Int(nullable: false),
                        Valeur = c.Decimal(nullable: false, precision: 18, scale: 2, storeType: "numeric"),
                        Libelle = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => new { t.RefCombo, t.Valeur })
                .ForeignKey("dbo.Combo", t => t.RefCombo)
                .Index(t => t.RefCombo);
            
            CreateTable(
                "dbo.Element",
                c => new
                    {
                        RefElement = c.Int(nullable: false),
                        Description = c.String(maxLength: 50, unicode: false),
                    })
                .PrimaryKey(t => t.RefElement);
            
            CreateTable(
                "dbo.Traduction",
                c => new
                    {
                        RefElement = c.Int(nullable: false),
                        RefLangue = c.Int(nullable: false),
                        Traduction = c.String(nullable: false, maxLength: 1000),
                        IsDefaut = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => new { t.RefElement, t.RefLangue })
                .ForeignKey("dbo.Langue", t => t.RefLangue)
                .ForeignKey("dbo.Element", t => t.RefElement)
                .Index(t => t.RefElement)
                .Index(t => t.RefLangue);
            
            CreateTable(
                "dbo.Langue",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Nom = c.String(nullable: false, maxLength: 50),
                        IsActive = c.Boolean(nullable: false),
                        Nom_T = c.Int(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Languages",
                c => new
                    {
                        LanguageID = c.Int(nullable: false, identity: true),
                        Code = c.String(nullable: false, maxLength: 10, fixedLength: true),
                    })
                .PrimaryKey(t => t.LanguageID);
            
            CreateTable(
                "dbo.User",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        Username = c.String(nullable: false),
                        Email = c.String(nullable: false),
                        Password = c.String(nullable: false),
                        FirstName = c.String(),
                        LastName = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        LanguageID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.UserId)
                .ForeignKey("dbo.Languages", t => t.LanguageID, cascadeDelete: true)
                .Index(t => t.LanguageID);
            
            CreateTable(
                "dbo.Role",
                c => new
                    {
                        RoleId = c.Int(nullable: false, identity: true),
                        RoleName = c.String(nullable: false),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.RoleId);
            
            CreateTable(
                "dbo.Utilisateur",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Nom = c.String(nullable: false, maxLength: 50),
                        Prenom = c.String(maxLength: 50),
                        Login = c.String(nullable: false, maxLength: 50),
                        PassWord = c.String(nullable: false, maxLength: 50),
                        RefLangue = c.Int(),
                        Email = c.String(nullable: false, maxLength: 50),
                        IsActive = c.Boolean(nullable: false),
                        RefRole = c.Int(nullable: false),
                        RefId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Distributeur_Client",
                c => new
                    {
                        RefClient = c.Int(nullable: false),
                        RefDistributeur = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.RefClient, t.RefDistributeur })
                .ForeignKey("dbo.Client", t => t.RefClient, cascadeDelete: true)
                .ForeignKey("dbo.Distributeur", t => t.RefDistributeur, cascadeDelete: true)
                .Index(t => t.RefClient)
                .Index(t => t.RefDistributeur);
            
            CreateTable(
                "dbo.ProduitInterdit",
                c => new
                    {
                        RefEspece = c.Int(nullable: false),
                        RefProduit = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.RefEspece, t.RefProduit })
                .ForeignKey("dbo.Espece", t => t.RefEspece, cascadeDelete: true)
                .ForeignKey("dbo.Produit", t => t.RefProduit, cascadeDelete: true)
                .Index(t => t.RefEspece)
                .Index(t => t.RefProduit);
            
            CreateTable(
                "dbo.UserRole",
                c => new
                    {
                        RoleId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.RoleId, t.UserId })
                .ForeignKey("dbo.Role", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.User", t => t.UserId, cascadeDelete: true)
                .Index(t => t.RoleId)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserRole", "UserId", "dbo.User");
            DropForeignKey("dbo.UserRole", "RoleId", "dbo.Role");
            DropForeignKey("dbo.User", "LanguageID", "dbo.Languages");
            DropForeignKey("dbo.Traduction", "RefElement", "dbo.Element");
            DropForeignKey("dbo.Traduction", "RefLangue", "dbo.Langue");
            DropForeignKey("dbo.Combo_Element", "RefElement", "dbo.Element");
            DropForeignKey("dbo.Combo_Element_Numeric", "RefCombo", "dbo.Combo");
            DropForeignKey("dbo.Combo_Element", "IDRefCombo", "dbo.Combo");
            DropForeignKey("dbo.Quota", "RefEtape", "dbo.Etape");
            DropForeignKey("dbo.ProgrammeDetail", "RefEtape", "dbo.Etape");
            DropForeignKey("dbo.Bilan", "RefEtape", "dbo.Etape");
            DropForeignKey("dbo.Quota", "RefProduit", "dbo.Produit");
            DropForeignKey("dbo.ProgrammeDetail", "RefProduit", "dbo.Produit");
            DropForeignKey("dbo.Variete", "RefEspece", "dbo.Espece");
            DropForeignKey("dbo.ProgrammeNutrition", "RefEspece", "dbo.Espece");
            DropForeignKey("dbo.Quota", "RefProgrammeNutrition", "dbo.ProgrammeNutrition");
            DropForeignKey("dbo.ProgrammeDetail", "RefProgrammeNutrition", "dbo.ProgrammeNutrition");
            DropForeignKey("dbo.ProgrammeDetail", "RefProgrammeModele", "dbo.ProgrammeModele");
            DropForeignKey("dbo.ProgrammeApplicationDirect", "RefProgrammeModele", "dbo.ProgrammeModele");
            DropForeignKey("dbo.Bilan", "RefProgrammeNutrition", "dbo.ProgrammeNutrition");
            DropForeignKey("dbo.BilanModele", "RefProgrammeNutrition", "dbo.ProgrammeNutrition");
            DropForeignKey("dbo.ProduitInterdit", "RefProduit", "dbo.Produit");
            DropForeignKey("dbo.ProduitInterdit", "RefEspece", "dbo.Espece");
            DropForeignKey("dbo.Greffe", "RefEspece", "dbo.Espece");
            DropForeignKey("dbo.Etape", "RefEspece", "dbo.Espece");
            DropForeignKey("dbo.CoefFumure", "RefEspece", "dbo.Espece");
            DropForeignKey("dbo.CarenceNorme", "RefEspece", "dbo.Espece");
            DropForeignKey("dbo.CarenceNorme", "RefType", "dbo.CarenceType");
            DropForeignKey("dbo.CarenceReleve", "RefCarenceNorme", "dbo.CarenceNorme");
            DropForeignKey("dbo.Parcelle", "RefVariete", "dbo.Variete");
            DropForeignKey("dbo.Releve", "RefParcelle", "dbo.Parcelle");
            DropForeignKey("dbo.Client", "RefTechBMS", "dbo.Technicien");
            DropForeignKey("dbo.Parcelle", "RefClient", "dbo.Client");
            DropForeignKey("dbo.Distributeur_Client", "RefDistributeur", "dbo.Distributeur");
            DropForeignKey("dbo.Distributeur_Client", "RefClient", "dbo.Client");
            DropForeignKey("dbo.Distributeur", "Pays_ID", "dbo.Pays");
            DropForeignKey("dbo.Client", "Pays_ID", "dbo.Pays");
            DropForeignKey("dbo.Releve", "RefComboGreffe", "dbo.Greffe");
            DropForeignKey("dbo.CarenceReleve", "RefReleve", "dbo.Releve");
            DropForeignKey("dbo.Bilan", "RefReleve", "dbo.Releve");
            DropForeignKey("dbo.CarenceNorme", "RefCarenceElement", "dbo.CarenceElement");
            DropForeignKey("dbo.Bilan", "RefProduit", "dbo.Produit");
            DropForeignKey("dbo.BilanModele", "RefProduit", "dbo.Produit");
            DropForeignKey("dbo.BilanModele", "RefEtape", "dbo.Etape");
            DropIndex("dbo.UserRole", new[] { "UserId" });
            DropIndex("dbo.UserRole", new[] { "RoleId" });
            DropIndex("dbo.ProduitInterdit", new[] { "RefProduit" });
            DropIndex("dbo.ProduitInterdit", new[] { "RefEspece" });
            DropIndex("dbo.Distributeur_Client", new[] { "RefDistributeur" });
            DropIndex("dbo.Distributeur_Client", new[] { "RefClient" });
            DropIndex("dbo.User", new[] { "LanguageID" });
            DropIndex("dbo.Traduction", new[] { "RefLangue" });
            DropIndex("dbo.Traduction", new[] { "RefElement" });
            DropIndex("dbo.Combo_Element_Numeric", new[] { "RefCombo" });
            DropIndex("dbo.Combo_Element", new[] { "RefElement" });
            DropIndex("dbo.Combo_Element", new[] { "IDRefCombo" });
            DropIndex("dbo.Quota", new[] { "RefProduit" });
            DropIndex("dbo.Quota", new[] { "RefEtape" });
            DropIndex("dbo.Quota", new[] { "RefProgrammeNutrition" });
            DropIndex("dbo.ProgrammeApplicationDirect", new[] { "RefProgrammeModele" });
            DropIndex("dbo.ProgrammeDetail", new[] { "RefProduit" });
            DropIndex("dbo.ProgrammeDetail", new[] { "RefEtape" });
            DropIndex("dbo.ProgrammeDetail", new[] { "RefProgrammeNutrition" });
            DropIndex("dbo.ProgrammeDetail", new[] { "RefProgrammeModele" });
            DropIndex("dbo.ProgrammeNutrition", new[] { "RefEspece" });
            DropIndex("dbo.CoefFumure", new[] { "RefEspece" });
            DropIndex("dbo.Variete", new[] { "RefEspece" });
            DropIndex("dbo.Distributeur", new[] { "Pays_ID" });
            DropIndex("dbo.Client", new[] { "Pays_ID" });
            DropIndex("dbo.Client", new[] { "RefTechBMS" });
            DropIndex("dbo.Parcelle", new[] { "RefVariete" });
            DropIndex("dbo.Parcelle", new[] { "RefClient" });
            DropIndex("dbo.Greffe", new[] { "RefEspece" });
            DropIndex("dbo.Releve", new[] { "RefComboGreffe" });
            DropIndex("dbo.Releve", new[] { "RefParcelle" });
            DropIndex("dbo.CarenceReleve", new[] { "RefReleve" });
            DropIndex("dbo.CarenceReleve", new[] { "RefCarenceNorme" });
            DropIndex("dbo.CarenceNorme", new[] { "RefType" });
            DropIndex("dbo.CarenceNorme", new[] { "RefEspece" });
            DropIndex("dbo.CarenceNorme", new[] { "RefCarenceElement" });
            DropIndex("dbo.Bilan", new[] { "RefProduit" });
            DropIndex("dbo.Bilan", new[] { "RefEtape" });
            DropIndex("dbo.Bilan", new[] { "RefProgrammeNutrition" });
            DropIndex("dbo.Bilan", new[] { "RefReleve" });
            DropIndex("dbo.Etape", new[] { "RefEspece" });
            DropIndex("dbo.BilanModele", new[] { "RefProduit" });
            DropIndex("dbo.BilanModele", new[] { "RefEtape" });
            DropIndex("dbo.BilanModele", new[] { "RefProgrammeNutrition" });
            DropTable("dbo.UserRole");
            DropTable("dbo.ProduitInterdit");
            DropTable("dbo.Distributeur_Client");
            DropTable("dbo.Utilisateur");
            DropTable("dbo.Role");
            DropTable("dbo.User");
            DropTable("dbo.Languages");
            DropTable("dbo.Langue");
            DropTable("dbo.Traduction");
            DropTable("dbo.Element");
            DropTable("dbo.Combo_Element_Numeric");
            DropTable("dbo.Combo");
            DropTable("dbo.Combo_Element");
            DropTable("dbo.Quota");
            DropTable("dbo.ProgrammeApplicationDirect");
            DropTable("dbo.ProgrammeModele");
            DropTable("dbo.ProgrammeDetail");
            DropTable("dbo.ProgrammeNutrition");
            DropTable("dbo.CoefFumure");
            DropTable("dbo.CarenceType");
            DropTable("dbo.Variete");
            DropTable("dbo.Technicien");
            DropTable("dbo.Pays");
            DropTable("dbo.Distributeur");
            DropTable("dbo.Client");
            DropTable("dbo.Parcelle");
            DropTable("dbo.Greffe");
            DropTable("dbo.Releve");
            DropTable("dbo.CarenceReleve");
            DropTable("dbo.CarenceElement");
            DropTable("dbo.CarenceNorme");
            DropTable("dbo.Espece");
            DropTable("dbo.Produit");
            DropTable("dbo.Bilan");
            DropTable("dbo.Etape");
            DropTable("dbo.BilanModele");
        }
    }
}
