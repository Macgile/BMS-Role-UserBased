﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using Resources.Abstract;
using Resources.Entities;

namespace Resources.Concrete
{
    public class DbResourceProvider : BaseResourceProvider
    {
        // Database connection string
        private static string connectionString = null;
        private int default_language = 1; // fr

        public DbResourceProvider()
        {
            var conName = ((Environment.MachineName == "GILLES-PC") ? "Gilles" : "Phareway");
            connectionString = ConfigurationManager.ConnectionStrings[conName].ConnectionString;
        }

        public DbResourceProvider(string connection)
        {
            connectionString = connection;
        }

        protected override IList<ResourceEntry> ReadResources()
        {
            var resources = new List<ResourceEntry>();
            //var default_culture = 1 // FR;

            // Old Query
            //const string sql = "select Culture, Name, Value from dbo.Resources;";

            // New Query, DISTINCT for eliminate duplicate entry
            const string sql = "SELECT DISTINCT T.Cle AS Name,"
                    + " CASE T.RefLangue "
                    + " WHEN 1 THEN 'fr'"
                    + " WHEN 2 THEN 'it'"
                    + " WHEN 3 THEN 'es'"
                    + " WHEN 4 THEN 'de'"
                    + " WHEN 5 THEN 'en'"
                    + " WHEN 6 THEN 'pt'"
                    + " WHEN 8 THEN 'nl'"
                    + " END AS Culture,"
                    + " CASE WHEN (LEN(T.Traduction) <> 0) THEN T.Traduction"
                    + " ELSE (SELECT Traduction FROM Traduction WHERE RefElement = T.RefElement AND RefLangue = @default_language)"
                    + " END AS Value "
                    + " FROM Traduction T WHERE Cle IS NOT NULL";

            using (var con = new SqlConnection(connectionString))
            {
                var cmd = new SqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@default_language", default_language);

                con.Open();

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        resources.Add(new ResourceEntry
                        {
                            Name = reader["Name"].ToString(),
                            Value = reader["Value"].ToString(),
                            Culture = reader["Culture"].ToString()
                        });
                    }

                    if (!reader.HasRows) throw new Exception("No resources were found");
                }
            }

            return resources;
        }

        protected override ResourceEntry ReadResource(string name, string culture)
        {
            ResourceEntry resource = null;
            var refLangue = 0;

            switch (culture)
            {
                case "fr": refLangue = 1; break;
                case "it": refLangue = 2; break;
                case "es": refLangue = 3; break;
                case "de": refLangue = 4; break;
                case "en": refLangue = 5; break;
                case "pt": refLangue = 6; break;
                case "nl": refLangue = 8; break;
            }

            //const string sql = "select Culture, Name, Value from dbo.Resources where culture = @culture and name = @name;";

            // New Query, DISTINCT for eliminate duplicate entry
            const string sql = "SELECT DISTINCT T.Cle AS Name,"
                    + " CASE T.RefLangue "
                    + " WHEN 1 THEN 'fr'"
                    + " WHEN 2 THEN 'it'"
                    + " WHEN 3 THEN 'es'"
                    + " WHEN 4 THEN 'de'"
                    + " WHEN 5 THEN 'en'"
                    + " WHEN 6 THEN 'pt'"
                    + " WHEN 8 THEN 'nl'"
                    + " END AS Culture,"
                    + " CASE WHEN (LEN(T.Traduction) <> 0) THEN T.Traduction"
                    + " ELSE (SELECT Traduction FROM Traduction WHERE RefElement = T.RefElement AND RefLangue = @default_language)"
                    + " END AS Value"
                    + " FROM Traduction T WHERE AND T.Cle = @name AND T.RefLangue = @refLangue";

            using (var con = new SqlConnection(connectionString))
            {
                var cmd = new SqlCommand(sql, con);

                cmd.Parameters.AddWithValue("@culture", culture);
                cmd.Parameters.AddWithValue("@refLangue", refLangue);
                cmd.Parameters.AddWithValue("@default_language", default_language);

                con.Open();

                using (var reader = cmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        resource = new ResourceEntry
                        {
                            Name = reader["Name"].ToString(),
                            Value = reader["Value"].ToString(),
                            Culture = reader["Culture"].ToString()
                        };
                    }

                    if (!reader.HasRows) throw new Exception(string.Format("Resource {0} for culture {1} was not found", name, culture));
                }
            }

            return resource;
        }
    }
}