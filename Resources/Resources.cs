using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Resources.Abstract;
using Resources.Concrete;
    
namespace Resources {
        public class Resources {
            private static IResourceProvider resourceProvider = new DbResourceProvider();

                
        /// <summary>Compte</summary>
        public static string Account {
               get {
                   return (string) resourceProvider.GetResource("Account", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Activer</summary>
        public static string Activate {
               get {
                   return (string) resourceProvider.GetResource("Activate", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Activé</summary>
        public static string Activated {
               get {
                   return (string) resourceProvider.GetResource("Activated", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Pourcentage calcaire actif</summary>
        public static string ActiveLimestonePercent {
               get {
                   return (string) resourceProvider.GetResource("ActiveLimestonePercent", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Ajouter</summary>
        public static string Add {
               get {
                   return (string) resourceProvider.GetResource("Add", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Ajouter des carences</summary>
        public static string AddDeficiencies {
               get {
                   return (string) resourceProvider.GetResource("AddDeficiencies", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Complément d'adresse</summary>
        public static string AdditionalAddress {
               get {
                   return (string) resourceProvider.GetResource("AdditionalAddress", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Ajouter un nouvel utilisateur</summary>
        public static string AddNewUser {
               get {
                   return (string) resourceProvider.GetResource("AddNewUser", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Adresse</summary>
        public static string Address {
               get {
                   return (string) resourceProvider.GetResource("Address", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Adulte</summary>
        public static string Adult {
               get {
                   return (string) resourceProvider.GetResource("Adult", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Age</summary>
        public static string Age {
               get {
                   return (string) resourceProvider.GetResource("Age", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Tous</summary>
        public static string All {
               get {
                   return (string) resourceProvider.GetResource("All", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Alternance</summary>
        public static string Alternating {
               get {
                   return (string) resourceProvider.GetResource("Alternating", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Analyses : valeurs exprimées en P.P.M de matière séche</summary>
        public static string AnalysesExpValuePPM {
               get {
                   return (string) resourceProvider.GetResource("AnalysesExpValuePPM", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Analysé</summary>
        public static string Analyzed {
               get {
                   return (string) resourceProvider.GetResource("Analyzed", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Valeurs analysées</summary>
        public static string AnalyzedValues {
               get {
                   return (string) resourceProvider.GetResource("AnalyzedValues", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Analyses</summary>
        public static string Analyzes {
               get {
                   return (string) resourceProvider.GetResource("Analyzes", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Appellation</summary>
        public static string Appellation {
               get {
                   return (string) resourceProvider.GetResource("Appellation", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Archiver</summary>
        public static string Archive {
               get {
                   return (string) resourceProvider.GetResource("Archive", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Archivé</summary>
        public static string Archived {
               get {
                   return (string) resourceProvider.GetResource("Archived", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Surface parcelle</summary>
        public static string AreaParcel {
               get {
                   return (string) resourceProvider.GetResource("AreaParcel", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Moyen</summary>
        public static string Average {
               get {
                   return (string) resourceProvider.GetResource("Average", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Moyenne</summary>
        public static string AverageF {
               get {
                   return (string) resourceProvider.GetResource("AverageF", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Pluviomètrie moyenne</summary>
        public static string AveragePluvio {
               get {
                   return (string) resourceProvider.GetResource("AveragePluvio", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Tonnage moyen</summary>
        public static string AverageTonnage {
               get {
                   return (string) resourceProvider.GetResource("AverageTonnage", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Rendement moyen</summary>
        public static string AverageYield {
               get {
                   return (string) resourceProvider.GetResource("AverageYield", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Bactériose</summary>
        public static string Bacteriosis {
               get {
                   return (string) resourceProvider.GetResource("Bacteriosis", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Bilan calculé</summary>
        public static string BalanceCalculated {
               get {
                   return (string) resourceProvider.GetResource("BalanceCalculated", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Equilibré</summary>
        public static string Balanced {
               get {
                   return (string) resourceProvider.GetResource("Balanced", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Equilibre</summary>
        public static string BalancedF {
               get {
                   return (string) resourceProvider.GetResource("BalancedF", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Bilan</summary>
        public static string BalanceSheet {
               get {
                   return (string) resourceProvider.GetResource("BalanceSheet", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Programme de base</summary>
        public static string BasicProgram {
               get {
                   return (string) resourceProvider.GetResource("BasicProgram", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Précédent</summary>
        public static string Before {
               get {
                   return (string) resourceProvider.GetResource("Before", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Avant récolte</summary>
        public static string BeforeCrop {
               get {
                   return (string) resourceProvider.GetResource("BeforeCrop", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Tardive</summary>
        public static string Belated {
               get {
                   return (string) resourceProvider.GetResource("Belated", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Belgique</summary>
        public static string Belgium {
               get {
                   return (string) resourceProvider.GetResource("Belgium", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Bio</summary>
        public static string Bio {
               get {
                   return (string) resourceProvider.GetResource("Bio", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Bitter pit</summary>
        public static string BitterPit {
               get {
                   return (string) resourceProvider.GetResource("BitterPit", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Dessèchement de rafle</summary>
        public static string BlackStem {
               get {
                   return (string) resourceProvider.GetResource("BlackStem", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Impression noir & blanc</summary>
        public static string BlackWhitePrinting {
               get {
                   return (string) resourceProvider.GetResource("BlackWhitePrinting", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Floraison / nouaison</summary>
        public static string BloomingNouaison {
               get {
                   return (string) resourceProvider.GetResource("BloomingNouaison", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Programme de nutrition foliaire garanti selon le relevé effectué par le technicien BMS MN :</summary>
        public static string BmsGarantedFoliarNutritionProgram {
               get {
                   return (string) resourceProvider.GetResource("BmsGarantedFoliarNutritionProgram", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Technicien</summary>
        public static string BMSTechnician {
               get {
                   return (string) resourceProvider.GetResource("BMSTechnician", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Bore</summary>
        public static string Boron {
               get {
                   return (string) resourceProvider.GetResource("Boron", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Apport MO</summary>
        public static string BringMO {
               get {
                   return (string) resourceProvider.GetResource("BringMO", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Calcium</summary>
        public static string Calcium {
               get {
                   return (string) resourceProvider.GetResource("Calcium", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Calibre</summary>
        public static string Caliber {
               get {
                   return (string) resourceProvider.GetResource("Caliber", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Calibre N-1</summary>
        public static string CaliberN1 {
               get {
                   return (string) resourceProvider.GetResource("CaliberN1", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Annuler</summary>
        public static string Cancel {
               get {
                   return (string) resourceProvider.GetResource("Cancel", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>CEC</summary>
        public static string CEC {
               get {
                   return (string) resourceProvider.GetResource("CEC", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>CEC doit être numérique !</summary>
        public static string CECMustNumeric {
               get {
                   return (string) resourceProvider.GetResource("CECMustNumeric", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>PDG</summary>
        public static string CEO {
               get {
                   return (string) resourceProvider.GetResource("CEO", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Chlorose</summary>
        public static string Chlorosis {
               get {
                   return (string) resourceProvider.GetResource("Chlorosis", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Ville</summary>
        public static string City {
               get {
                   return (string) resourceProvider.GetResource("City", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Effacer personnalisation</summary>
        public static string ClearCustomization {
               get {
                   return (string) resourceProvider.GetResource("ClearCustomization", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Cliquer sur le bouton Ok pour continuer</summary>
        public static string ClickOk {
               get {
                   return (string) resourceProvider.GetResource("ClickOk", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Zone climatique</summary>
        public static string ClimateZone {
               get {
                   return (string) resourceProvider.GetResource("ClimateZone", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Fermer</summary>
        public static string Close {
               get {
                   return (string) resourceProvider.GetResource("Close", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Collaborateur</summary>
        public static string Collaborator {
               get {
                   return (string) resourceProvider.GetResource("Collaborator", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Impression couleur</summary>
        public static string ColorPrinting {
               get {
                   return (string) resourceProvider.GetResource("ColorPrinting", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Confirmer le mot de passe</summary>
        public static string ConfirmPassword {
               get {
                   return (string) resourceProvider.GetResource("ConfirmPassword", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Connection</summary>
        public static string Connection {
               get {
                   return (string) resourceProvider.GetResource("Connection", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Informations de connexion</summary>
        public static string ConnectionInformation {
               get {
                   return (string) resourceProvider.GetResource("ConnectionInformation", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Conservation</summary>
        public static string Conservation {
               get {
                   return (string) resourceProvider.GetResource("Conservation", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Contact</summary>
        public static string Contact {
               get {
                   return (string) resourceProvider.GetResource("Contact", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Continentale</summary>
        public static string Continental {
               get {
                   return (string) resourceProvider.GetResource("Continental", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Conventionel</summary>
        public static string Conventional {
               get {
                   return (string) resourceProvider.GetResource("Conventional", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Coordonnées Distributeur</summary>
        public static string CoordinatesDistributor {
               get {
                   return (string) resourceProvider.GetResource("CoordinatesDistributor", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Cuivre</summary>
        public static string Copper {
               get {
                   return (string) resourceProvider.GetResource("Copper", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Durcissement du noyau</summary>
        public static string CoreHardening {
               get {
                   return (string) resourceProvider.GetResource("CoreHardening", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Liège</summary>
        public static string Cork {
               get {
                   return (string) resourceProvider.GetResource("Cork", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Coulure</summary>
        public static string Coulure {
               get {
                   return (string) resourceProvider.GetResource("Coulure", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Pays</summary>
        public static string Country {
               get {
                   return (string) resourceProvider.GetResource("Country", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Créé</summary>
        public static string Created {
               get {
                   return (string) resourceProvider.GetResource("Created", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Créé par</summary>
        public static string CreatedBy {
               get {
                   return (string) resourceProvider.GetResource("CreatedBy", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Créé le</summary>
        public static string CreatedThe {
               get {
                   return (string) resourceProvider.GetResource("CreatedThe", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Créer un utilisateur</summary>
        public static string CreateUser {
               get {
                   return (string) resourceProvider.GetResource("CreateUser", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Récolte</summary>
        public static string Crop {
               get {
                   return (string) resourceProvider.GetResource("Crop", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Récolte N-1</summary>
        public static string CropN1 {
               get {
                   return (string) resourceProvider.GetResource("CropN1", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Cumul</summary>
        public static string Cumulative {
               get {
                   return (string) resourceProvider.GetResource("Cumulative", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Cumul(s)</summary>
        public static string CumulativeP {
               get {
                   return (string) resourceProvider.GetResource("CumulativeP", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Client</summary>
        public static string Customer {
               get {
                   return (string) resourceProvider.GetResource("Customer", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Prénom du client</summary>
        public static string CustomerFirstname {
               get {
                   return (string) resourceProvider.GetResource("CustomerFirstname", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Nom du client</summary>
        public static string CustomerName {
               get {
                   return (string) resourceProvider.GetResource("CustomerName", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Clients</summary>
        public static string Customers {
               get {
                   return (string) resourceProvider.GetResource("Customers", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Liste des clients</summary>
        public static string CustomersList {
               get {
                   return (string) resourceProvider.GetResource("CustomersList", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Client(s)</summary>
        public static string CustomerSP {
               get {
                   return (string) resourceProvider.GetResource("CustomerSP", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Programme nutrition foliaire personnalisé</summary>
        public static string CustomFoliarNutritionProgram {
               get {
                   return (string) resourceProvider.GetResource("CustomFoliarNutritionProgram", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Personnalisation</summary>
        public static string Customization {
               get {
                   return (string) resourceProvider.GetResource("Customization", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Personnalisé</summary>
        public static string Customized {
               get {
                   return (string) resourceProvider.GetResource("Customized", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Date</summary>
        public static string Date {
               get {
                   return (string) resourceProvider.GetResource("Date", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Date du relevé</summary>
        public static string DateOfStatement {
               get {
                   return (string) resourceProvider.GetResource("DateOfStatement", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>AOC</summary>
        public static string DCN {
               get {
                   return (string) resourceProvider.GetResource("DCN", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Désactivé</summary>
        public static string Deactivated {
               get {
                   return (string) resourceProvider.GetResource("Deactivated", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Pourriture</summary>
        public static string Decay {
               get {
                   return (string) resourceProvider.GetResource("Decay", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Carence</summary>
        public static string Deficiency {
               get {
                   return (string) resourceProvider.GetResource("Deficiency", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Carence en Mg</summary>
        public static string DeficiencyInMg {
               get {
                   return (string) resourceProvider.GetResource("DeficiencyInMg", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Carence en Mn</summary>
        public static string DeficiencyInMn {
               get {
                   return (string) resourceProvider.GetResource("DeficiencyInMn", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Carence Autre</summary>
        public static string DeficiencyOther {
               get {
                   return (string) resourceProvider.GetResource("DeficiencyOther", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Carences</summary>
        public static string DeficiencyS {
               get {
                   return (string) resourceProvider.GetResource("DeficiencyS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Degré</summary>
        public static string Degree {
               get {
                   return (string) resourceProvider.GetResource("Degree", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Effacer</summary>
        public static string Delete {
               get {
                   return (string) resourceProvider.GetResource("Delete", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Détail</summary>
        public static string Detail {
               get {
                   return (string) resourceProvider.GetResource("Detail", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Détail de la parcelle</summary>
        public static string DetailParcel {
               get {
                   return (string) resourceProvider.GetResource("DetailParcel", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Différenciation</summary>
        public static string Differentiation {
               get {
                   return (string) resourceProvider.GetResource("Differentiation", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Déconnecter</summary>
        public static string Disconect {
               get {
                   return (string) resourceProvider.GetResource("Disconect", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Distillation</summary>
        public static string Distillation {
               get {
                   return (string) resourceProvider.GetResource("Distillation", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Distributeur</summary>
        public static string DistributionTechnician {
               get {
                   return (string) resourceProvider.GetResource("DistributionTechnician", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Distributeur</summary>
        public static string Distributor {
               get {
                   return (string) resourceProvider.GetResource("Distributor", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Nom du distributeur</summary>
        public static string DistributorName {
               get {
                   return (string) resourceProvider.GetResource("DistributorName", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Distributeurs</summary>
        public static string DistributorS {
               get {
                   return (string) resourceProvider.GetResource("DistributorS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Liste des distributeurs</summary>
        public static string DistributorsList {
               get {
                   return (string) resourceProvider.GetResource("DistributorsList", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Distributeur(s)</summary>
        public static string DistributorSP {
               get {
                   return (string) resourceProvider.GetResource("DistributorSP", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Voulez vous archiver</summary>
        public static string DoyouWantArchive {
               get {
                   return (string) resourceProvider.GetResource("DoyouWantArchive", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Voulez-vous archiver ce client</summary>
        public static string DoyouWantArchiveCustomer {
               get {
                   return (string) resourceProvider.GetResource("DoyouWantArchiveCustomer", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Goutte à goutte</summary>
        public static string Drip {
               get {
                   return (string) resourceProvider.GetResource("Drip", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Tonnage sec</summary>
        public static string DryTonnage {
               get {
                   return (string) resourceProvider.GetResource("DryTonnage", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Précoce</summary>
        public static string Early {
               get {
                   return (string) resourceProvider.GetResource("Early", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Précoce de Croncels</summary>
        public static string EarlyCroncels {
               get {
                   return (string) resourceProvider.GetResource("EarlyCroncels", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Précoce de Hale</summary>
        public static string EarlyHale {
               get {
                   return (string) resourceProvider.GetResource("EarlyHale", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Précoce de Malingre</summary>
        public static string EarlyMalingre {
               get {
                   return (string) resourceProvider.GetResource("EarlyMalingre", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Modifier</summary>
        public static string Edit {
               get {
                   return (string) resourceProvider.GetResource("Edit", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Modifiable</summary>
        public static string Editable {
               get {
                   return (string) resourceProvider.GetResource("Editable", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Modifier la parcelle</summary>
        public static string EditParcel {
               get {
                   return (string) resourceProvider.GetResource("EditParcel", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Eléments pris en compte dans le programme de nutrition foliaire.</summary>
        public static string ElementsTakenFoliarNutritionProg {
               get {
                   return (string) resourceProvider.GetResource("ElementsTakenFoliarNutritionProg", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Email</summary>
        public static string Email {
               get {
                   return (string) resourceProvider.GetResource("Email", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Adresse email</summary>
        public static string EmailAdress {
               get {
                   return (string) resourceProvider.GetResource("EmailAdress", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Anglais</summary>
        public static string English {
               get {
                   return (string) resourceProvider.GetResource("English", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Equatoriale</summary>
        public static string Equatorial {
               get {
                   return (string) resourceProvider.GetResource("Equatorial", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Le mot de passe ne correspond pas, saissez le à nouveau!</summary>
        public static string ErrorPassword {
               get {
                   return (string) resourceProvider.GetResource("ErrorPassword", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Estivale</summary>
        public static string Estival {
               get {
                   return (string) resourceProvider.GetResource("Estival", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Exceptionnel</summary>
        public static string Exceptional {
               get {
                   return (string) resourceProvider.GetResource("Exceptional", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Exceptionnelle</summary>
        public static string ExceptionalS {
               get {
                   return (string) resourceProvider.GetResource("ExceptionalS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Informations Exploitation</summary>
        public static string ExploitationInfos {
               get {
                   return (string) resourceProvider.GetResource("ExploitationInfos", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Exporter client(s)</summary>
        public static string ExportCustomerSP {
               get {
                   return (string) resourceProvider.GetResource("ExportCustomerSP", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Basculement</summary>
        public static string Failover {
               get {
                   return (string) resourceProvider.GetResource("Failover", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Chute de feuilles</summary>
        public static string FallLeaves {
               get {
                   return (string) resourceProvider.GetResource("FallLeaves", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Fax</summary>
        public static string FaxNumber {
               get {
                   return (string) resourceProvider.GetResource("FaxNumber", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Fermeté</summary>
        public static string Firmness {
               get {
                   return (string) resourceProvider.GetResource("Firmness", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Prénom</summary>
        public static string FirstName {
               get {
                   return (string) resourceProvider.GetResource("FirstName", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Calcul foliaire</summary>
        public static string FoliarComputation {
               get {
                   return (string) resourceProvider.GetResource("FoliarComputation", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Programme nutrition foliaire</summary>
        public static string FoliarNutritionProgram {
               get {
                   return (string) resourceProvider.GetResource("FoliarNutritionProgram", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>France</summary>
        public static string France {
               get {
                   return (string) resourceProvider.GetResource("France", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Français</summary>
        public static string French {
               get {
                   return (string) resourceProvider.GetResource("French", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Frigo</summary>
        public static string Fridge {
               get {
                   return (string) resourceProvider.GetResource("Fridge", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Taux de nouaison</summary>
        public static string FruitingRate {
               get {
                   return (string) resourceProvider.GetResource("FruitingRate", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Grossissement du fruit</summary>
        public static string FruitMagnification {
               get {
                   return (string) resourceProvider.GetResource("FruitMagnification", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Allemand</summary>
        public static string German {
               get {
                   return (string) resourceProvider.GetResource("German", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Allemagne</summary>
        public static string Germany {
               get {
                   return (string) resourceProvider.GetResource("Germany", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Objectif</summary>
        public static string Goal {
               get {
                   return (string) resourceProvider.GetResource("Goal", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Gobelet</summary>
        public static string Goblet {
               get {
                   return (string) resourceProvider.GetResource("Goblet", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Golden</summary>
        public static string Golden {
               get {
                   return (string) resourceProvider.GetResource("Golden", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Greffe</summary>
        public static string Graft {
               get {
                   return (string) resourceProvider.GetResource("Graft", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Grand crus</summary>
        public static string GrandCrus {
               get {
                   return (string) resourceProvider.GetResource("GrandCrus", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Rafle</summary>
        public static string GrapeStalk {
               get {
                   return (string) resourceProvider.GetResource("GrapeStalk", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Grande Bretagne</summary>
        public static string GreatBritain {
               get {
                   return (string) resourceProvider.GetResource("GreatBritain", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Supérieur à 10%</summary>
        public static string GreaterThanTenPercent {
               get {
                   return (string) resourceProvider.GetResource("GreaterThanTenPercent", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Sanitaire</summary>
        public static string Health {
               get {
                   return (string) resourceProvider.GetResource("Health", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Hauteur de la canopée</summary>
        public static string HeightCanopy {
               get {
                   return (string) resourceProvider.GetResource("HeightCanopy", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Bonjour</summary>
        public static string Hello {
               get {
                   return (string) resourceProvider.GetResource("Hello", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Elevé</summary>
        public static string High {
               get {
                   return (string) resourceProvider.GetResource("High", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Elevée</summary>
        public static string HighF {
               get {
                   return (string) resourceProvider.GetResource("HighF", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Menu général</summary>
        public static string Home {
               get {
                   return (string) resourceProvider.GetResource("Home", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Idéale</summary>
        public static string Ideal {
               get {
                   return (string) resourceProvider.GetResource("Ideal", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Calibre idéal</summary>
        public static string IdealCaliber {
               get {
                   return (string) resourceProvider.GetResource("IdealCaliber", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Tonnage idéal</summary>
        public static string IdealTonnage {
               get {
                   return (string) resourceProvider.GetResource("IdealTonnage", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Renforcement immunitaire</summary>
        public static string ImmuneEnhancement {
               get {
                   return (string) resourceProvider.GetResource("ImmuneEnhancement", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Responsable</summary>
        public static string InChargeOf {
               get {
                   return (string) resourceProvider.GetResource("InChargeOf", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Individuelle</summary>
        public static string Individual {
               get {
                   return (string) resourceProvider.GetResource("Individual", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Information des problèmes nutritionnels</summary>
        public static string InfoNutriProblem {
               get {
                   return (string) resourceProvider.GetResource("InfoNutriProblem", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Informations Technicien</summary>
        public static string InfoTechnician {
               get {
                   return (string) resourceProvider.GetResource("InfoTechnician", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Initiale</summary>
        public static string Initial {
               get {
                   return (string) resourceProvider.GetResource("Initial", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Intégré</summary>
        public static string Integrated {
               get {
                   return (string) resourceProvider.GetResource("Integrated", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Adresse Email invalide</summary>
        public static string InvalidEmail {
               get {
                   return (string) resourceProvider.GetResource("InvalidEmail", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Utilisateur ou mot de passe incorrect !</summary>
        public static string InvalidUserPassword {
               get {
                   return (string) resourceProvider.GetResource("InvalidUserPassword", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Fer</summary>
        public static string Iron {
               get {
                   return (string) resourceProvider.GetResource("Iron", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Irrigation</summary>
        public static string Irrigation {
               get {
                   return (string) resourceProvider.GetResource("Irrigation", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Italien</summary>
        public static string Italian {
               get {
                   return (string) resourceProvider.GetResource("Italian", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Italie</summary>
        public static string Italy {
               get {
                   return (string) resourceProvider.GetResource("Italy", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Juvénile</summary>
        public static string Juvenile {
               get {
                   return (string) resourceProvider.GetResource("Juvenile", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Langue</summary>
        public static string Language {
               get {
                   return (string) resourceProvider.GetResource("Language", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Mise en page</summary>
        public static string Layout {
               get {
                   return (string) resourceProvider.GetResource("Layout", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Feuilles</summary>
        public static string Leaf {
               get {
                   return (string) resourceProvider.GetResource("Leaf", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Inférieur à 10 %</summary>
        public static string LessThan10Percent {
               get {
                   return (string) resourceProvider.GetResource("LessThan10Percent", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Liste des parcelles d'un client</summary>
        public static string ListParcelsACustomer {
               get {
                   return (string) resourceProvider.GetResource("ListParcelsACustomer", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Liste des parcelles pour le client :</summary>
        public static string ListParcelsCustomer {
               get {
                   return (string) resourceProvider.GetResource("ListParcelsCustomer", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Liste des relevés d'un client</summary>
        public static string ListStatementsCustomer {
               get {
                   return (string) resourceProvider.GetResource("ListStatementsCustomer", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Liste des relevés pour la parcelle :</summary>
        public static string ListStatementsParcel {
               get {
                   return (string) resourceProvider.GetResource("ListStatementsParcel", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Se connecter</summary>
        public static string Login {
               get {
                   return (string) resourceProvider.GetResource("Login", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Longue</summary>
        public static string Long {
               get {
                   return (string) resourceProvider.GetResource("Long", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Faible</summary>
        public static string Low {
               get {
                   return (string) resourceProvider.GetResource("Low", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Magnésie</summary>
        public static string Magnesia {
               get {
                   return (string) resourceProvider.GetResource("Magnesia", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Carence Principale</summary>
        public static string MainDeficiency {
               get {
                   return (string) resourceProvider.GetResource("MainDeficiency", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>* Champs obligatoires</summary>
        public static string MandatoryField {
               get {
                   return (string) resourceProvider.GetResource("MandatoryField", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Manganèse</summary>
        public static string Manganese {
               get {
                   return (string) resourceProvider.GetResource("Manganese", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Fumure</summary>
        public static string Manure {
               get {
                   return (string) resourceProvider.GetResource("Manure", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Fumure réalisée</summary>
        public static string ManureRealised {
               get {
                   return (string) resourceProvider.GetResource("ManureRealised", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Norme maximale</summary>
        public static string MaxStandard {
               get {
                   return (string) resourceProvider.GetResource("MaxStandard", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Mécanisée</summary>
        public static string Mechanized {
               get {
                   return (string) resourceProvider.GetResource("Mechanized", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Méditerranéen</summary>
        public static string Mediterranean {
               get {
                   return (string) resourceProvider.GetResource("Mediterranean", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Mode de commercialisation</summary>
        public static string MethodMarketing {
               get {
                   return (string) resourceProvider.GetResource("MethodMarketing", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>MICROLAN Zn</summary>
        public static string MicrolanZn {
               get {
                   return (string) resourceProvider.GetResource("MicrolanZn", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Moyen Orient</summary>
        public static string MiddleEast {
               get {
                   return (string) resourceProvider.GetResource("MiddleEast", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Millerandage</summary>
        public static string Millerandage {
               get {
                   return (string) resourceProvider.GetResource("Millerandage", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Norme minimale</summary>
        public static string MinStandard {
               get {
                   return (string) resourceProvider.GetResource("MinStandard", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>M.O.</summary>
        public static string MO {
               get {
                   return (string) resourceProvider.GetResource("MO", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Modesto</summary>
        public static string Modesto {
               get {
                   return (string) resourceProvider.GetResource("Modesto", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Modifié le</summary>
        public static string Modified {
               get {
                   return (string) resourceProvider.GetResource("Modified", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Modifié par</summary>
        public static string ModifiedBy {
               get {
                   return (string) resourceProvider.GetResource("ModifiedBy", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Mon compte</summary>
        public static string MyAccount {
               get {
                   return (string) resourceProvider.GetResource("MyAccount", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Nom</summary>
        public static string Name {
               get {
                   return (string) resourceProvider.GetResource("Name", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Nom de la parcelle</summary>
        public static string NameParcel {
               get {
                   return (string) resourceProvider.GetResource("NameParcel", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Le nom est requis</summary>
        public static string NameRequired {
               get {
                   return (string) resourceProvider.GetResource("NameRequired", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Nouveau compte</summary>
        public static string NewAccount {
               get {
                   return (string) resourceProvider.GetResource("NewAccount", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Nouveau client</summary>
        public static string NewCustomer {
               get {
                   return (string) resourceProvider.GetResource("NewCustomer", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Nouveau distributeur</summary>
        public static string NewDistributor {
               get {
                   return (string) resourceProvider.GetResource("NewDistributor", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Nouvelle parcelle</summary>
        public static string NewParcel {
               get {
                   return (string) resourceProvider.GetResource("NewParcel", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Nouveau relevé</summary>
        public static string NewStatement {
               get {
                   return (string) resourceProvider.GetResource("NewStatement", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Suivant</summary>
        public static string Next {
               get {
                   return (string) resourceProvider.GetResource("Next", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Azote</summary>
        public static string Nitrogen {
               get {
                   return (string) resourceProvider.GetResource("Nitrogen", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Non</summary>
        public static string No {
               get {
                   return (string) resourceProvider.GetResource("No", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Aucun cumul</summary>
        public static string NoCumulAvailable {
               get {
                   return (string) resourceProvider.GetResource("NoCumulAvailable", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Programme de nutrition foliaire informatique non garanti</summary>
        public static string NoGarantedFoliarNutritionProgram {
               get {
                   return (string) resourceProvider.GetResource("NoGarantedFoliarNutritionProgram", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Aucun</summary>
        public static string None {
               get {
                   return (string) resourceProvider.GetResource("None", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Aucune</summary>
        public static string NoneF {
               get {
                   return (string) resourceProvider.GetResource("NoneF", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Nordique</summary>
        public static string Nordic {
               get {
                   return (string) resourceProvider.GetResource("Nordic", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Normal</summary>
        public static string Normal {
               get {
                   return (string) resourceProvider.GetResource("Normal", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Normale</summary>
        public static string NormalF {
               get {
                   return (string) resourceProvider.GetResource("NormalF", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Remarque sur la carence</summary>
        public static string NoteDeficiency {
               get {
                   return (string) resourceProvider.GetResource("NoteDeficiency", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Nombre d'analyses</summary>
        public static string NumberAnalyzes {
               get {
                   return (string) resourceProvider.GetResource("NumberAnalyzes", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Nombre de fleurs par mètre linéaire</summary>
        public static string NumberFlowersMeterLinear {
               get {
                   return (string) resourceProvider.GetResource("NumberFlowersMeterLinear", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Nombre de fleurs par m2</summary>
        public static string NumberFlowersMeterSquare {
               get {
                   return (string) resourceProvider.GetResource("NumberFlowersMeterSquare", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Nombre de fruits par m2</summary>
        public static string NumberFruitPerMeterSquare {
               get {
                   return (string) resourceProvider.GetResource("NumberFruitPerMeterSquare", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Nombre de fruits par mètre linéaire</summary>
        public static string NumberFruitsMeterLinear {
               get {
                   return (string) resourceProvider.GetResource("NumberFruitsMeterLinear", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Portable</summary>
        public static string NumberGSM {
               get {
                   return (string) resourceProvider.GetResource("NumberGSM", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Nombre d'utilisateurs</summary>
        public static string NumberOfUsers {
               get {
                   return (string) resourceProvider.GetResource("NumberOfUsers", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Objectif degré</summary>
        public static string ObjectiveDegree {
               get {
                   return (string) resourceProvider.GetResource("ObjectiveDegree", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Objectif Tonnage</summary>
        public static string ObjectiveTonnage {
               get {
                   return (string) resourceProvider.GetResource("ObjectiveTonnage", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Occasionnel</summary>
        public static string Occasional {
               get {
                   return (string) resourceProvider.GetResource("Occasional", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Occasionnelle</summary>
        public static string OccasionalF {
               get {
                   return (string) resourceProvider.GetResource("OccasionalF", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Océanique</summary>
        public static string Oceanic {
               get {
                   return (string) resourceProvider.GetResource("Oceanic", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Sur frondaison</summary>
        public static string OnFoliage {
               get {
                   return (string) resourceProvider.GetResource("OnFoliage", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Optimal</summary>
        public static string Optimal {
               get {
                   return (string) resourceProvider.GetResource("Optimal", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Rendement idéal</summary>
        public static string OptimalYield {
               get {
                   return (string) resourceProvider.GetResource("OptimalYield", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Option(s)</summary>
        public static string OptionP {
               get {
                   return (string) resourceProvider.GetResource("OptionP", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Orientale</summary>
        public static string Oriental {
               get {
                   return (string) resourceProvider.GetResource("Oriental", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Pacifique</summary>
        public static string Pacific {
               get {
                   return (string) resourceProvider.GetResource("Pacific", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Page</summary>
        public static string Page {
               get {
                   return (string) resourceProvider.GetResource("Page", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Parcelle</summary>
        public static string Parcel {
               get {
                   return (string) resourceProvider.GetResource("Parcel", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Parcelle(s)</summary>
        public static string ParcelFS {
               get {
                   return (string) resourceProvider.GetResource("ParcelFS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Parcelles</summary>
        public static string ParcelS {
               get {
                   return (string) resourceProvider.GetResource("ParcelS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Mot de passe</summary>
        public static string Password {
               get {
                   return (string) resourceProvider.GetResource("Password", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Confirmation mot de passe</summary>
        public static string PasswordConfirm {
               get {
                   return (string) resourceProvider.GetResource("PasswordConfirm", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>% Calcaire actif</summary>
        public static string PercentActiveLimestone {
               get {
                   return (string) resourceProvider.GetResource("PercentActiveLimestone", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Objectif de rendement</summary>
        public static string PerformanceObjective {
               get {
                   return (string) resourceProvider.GetResource("PerformanceObjective", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Objectif de rendement N-1</summary>
        public static string PerformanceObjectiveN1 {
               get {
                   return (string) resourceProvider.GetResource("PerformanceObjectiveN1", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Pergola</summary>
        public static string Pergola {
               get {
                   return (string) resourceProvider.GetResource("Pergola", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Informations personnelles</summary>
        public static string PersonalInfos {
               get {
                   return (string) resourceProvider.GetResource("PersonalInfos", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Stades phénologiques</summary>
        public static string PhenologicalStages {
               get {
                   return (string) resourceProvider.GetResource("PhenologicalStages", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>N° de téléphone</summary>
        public static string PhoneNumber {
               get {
                   return (string) resourceProvider.GetResource("PhoneNumber", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Phosphore</summary>
        public static string Phosphor {
               get {
                   return (string) resourceProvider.GetResource("Phosphor", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Situation phytosanitaire</summary>
        public static string PhytosanitarySituation {
               get {
                   return (string) resourceProvider.GetResource("PhytosanitarySituation", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Pluviometrie avant fleur</summary>
        public static string PluvioBeforeFlower {
               get {
                   return (string) resourceProvider.GetResource("PluvioBeforeFlower", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Portugais</summary>
        public static string Portuguese {
               get {
                   return (string) resourceProvider.GetResource("Portuguese", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Post récolte</summary>
        public static string PostHarvest {
               get {
                   return (string) resourceProvider.GetResource("PostHarvest", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Potassium</summary>
        public static string Potassium {
               get {
                   return (string) resourceProvider.GetResource("Potassium", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Pré-récolte</summary>
        public static string PreCrop {
               get {
                   return (string) resourceProvider.GetResource("PreCrop", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Présence</summary>
        public static string Presence {
               get {
                   return (string) resourceProvider.GetResource("Presence", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Président</summary>
        public static string President {
               get {
                   return (string) resourceProvider.GetResource("President", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Précédent</summary>
        public static string Previous {
               get {
                   return (string) resourceProvider.GetResource("Previous", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Imprimer</summary>
        public static string Print {
               get {
                   return (string) resourceProvider.GetResource("Print", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Client</summary>
        public static string Producer {
               get {
                   return (string) resourceProvider.GetResource("Producer", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Produit</summary>
        public static string Product {
               get {
                   return (string) resourceProvider.GetResource("Product", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Mode de production</summary>
        public static string ProductionMode {
               get {
                   return (string) resourceProvider.GetResource("ProductionMode", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Produits</summary>
        public static string Products {
               get {
                   return (string) resourceProvider.GetResource("Products", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Quantité</summary>
        public static string Quantity {
               get {
                   return (string) resourceProvider.GetResource("Quantity", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Quitter</summary>
        public static string Quit {
               get {
                   return (string) resourceProvider.GetResource("Quit", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Réalisée</summary>
        public static string Realized {
               get {
                   return (string) resourceProvider.GetResource("Realized", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Pluviométrie réelle</summary>
        public static string RealPluvio {
               get {
                   return (string) resourceProvider.GetResource("RealPluvio", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Pluviométrie réelle (90 jours avant F2)</summary>
        public static string RealRainfallBefore {
               get {
                   return (string) resourceProvider.GetResource("RealRainfallBefore", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Recommandations</summary>
        public static string Recommandations {
               get {
                   return (string) resourceProvider.GetResource("Recommandations", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Préconisation</summary>
        public static string Recommendation {
               get {
                   return (string) resourceProvider.GetResource("Recommendation", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Rendement conseillé</summary>
        public static string RecommendedYield {
               get {
                   return (string) resourceProvider.GetResource("RecommendedYield", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Restaurer</summary>
        public static string Recover {
               get {
                   return (string) resourceProvider.GetResource("Recover", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Récurrent</summary>
        public static string Recurrent {
               get {
                   return (string) resourceProvider.GetResource("Recurrent", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Récurrente</summary>
        public static string RecurrentF {
               get {
                   return (string) resourceProvider.GetResource("RecurrentF", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Label rouge</summary>
        public static string RedLabel {
               get {
                   return (string) resourceProvider.GetResource("RedLabel", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Code région</summary>
        public static string RegionCode {
               get {
                   return (string) resourceProvider.GetResource("RegionCode", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Enregistrement effectué</summary>
        public static string RegistrationComplete {
               get {
                   return (string) resourceProvider.GetResource("RegistrationComplete", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Remarques</summary>
        public static string Remarks {
               get {
                   return (string) resourceProvider.GetResource("Remarks", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Remarques technicien</summary>
        public static string RemarksTechnicianBMS {
               get {
                   return (string) resourceProvider.GetResource("RemarksTechnicianBMS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Supprimer</summary>
        public static string Remove {
               get {
                   return (string) resourceProvider.GetResource("Remove", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Rapport</summary>
        public static string Report {
               get {
                   return (string) resourceProvider.GetResource("Report", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Rapports analysés</summary>
        public static string ReportsAnalyzed {
               get {
                   return (string) resourceProvider.GetResource("ReportsAnalyzed", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Mise en réserve</summary>
        public static string Reserve {
               get {
                   return (string) resourceProvider.GetResource("Reserve", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Retour</summary>
        public static string Return {
               get {
                   return (string) resourceProvider.GetResource("Return", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Rôle</summary>
        public static string Role {
               get {
                   return (string) resourceProvider.GetResource("Role", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Porte greffe</summary>
        public static string Rootstock {
               get {
                   return (string) resourceProvider.GetResource("Rootstock", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Relevé effectué par</summary>
        public static string SatementConductedBy {
               get {
                   return (string) resourceProvider.GetResource("SatementConductedBy", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Enregister</summary>
        public static string Save {
               get {
                   return (string) resourceProvider.GetResource("Save", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Enregistrer les modifications</summary>
        public static string SaveChanges {
               get {
                   return (string) resourceProvider.GetResource("SaveChanges", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Enregistrer le nouveau client</summary>
        public static string SaveNewCustomer {
               get {
                   return (string) resourceProvider.GetResource("SaveNewCustomer", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Enregistrer le nouveau distributeur</summary>
        public static string SaveNewDistributor {
               get {
                   return (string) resourceProvider.GetResource("SaveNewDistributor", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Enregistrer le nouveau relevé</summary>
        public static string SaveNewStatement {
               get {
                   return (string) resourceProvider.GetResource("SaveNewStatement", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Enregistrer l'utilisateur</summary>
        public static string SaveNewUser {
               get {
                   return (string) resourceProvider.GetResource("SaveNewUser", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Enregistrer la parcelle</summary>
        public static string SaveParcel {
               get {
                   return (string) resourceProvider.GetResource("SaveParcel", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Echaudure</summary>
        public static string Screws {
               get {
                   return (string) resourceProvider.GetResource("Screws", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Chercher</summary>
        public static string Search {
               get {
                   return (string) resourceProvider.GetResource("Search", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Carence Secondaire</summary>
        public static string SecondaryDeficiency {
               get {
                   return (string) resourceProvider.GetResource("SecondaryDeficiency", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Sélectionner</summary>
        public static string Select {
               get {
                   return (string) resourceProvider.GetResource("Select", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Sélectionner une année de récolte</summary>
        public static string SelectCropYear {
               get {
                   return (string) resourceProvider.GetResource("SelectCropYear", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Sélectionner un client</summary>
        public static string SelectCustomer {
               get {
                   return (string) resourceProvider.GetResource("SelectCustomer", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Sélectionner un distributeur</summary>
        public static string SelectDistributor {
               get {
                   return (string) resourceProvider.GetResource("SelectDistributor", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Sélectionner un produit dans la liste et indiquez la quantité désirée.</summary>
        public static string SelectProductQuantity {
               get {
                   return (string) resourceProvider.GetResource("SelectProductQuantity", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Sélectionner le type</summary>
        public static string SelectType {
               get {
                   return (string) resourceProvider.GetResource("SelectType", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Veuillez choisir un type d'analyse dans la liste</summary>
        public static string SelectTypeAnalysis {
               get {
                   return (string) resourceProvider.GetResource("SelectTypeAnalysis", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Sélectionner le type de carence</summary>
        public static string SelectTypeDeficiency {
               get {
                   return (string) resourceProvider.GetResource("SelectTypeDeficiency", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Sénescent</summary>
        public static string Senescent {
               get {
                   return (string) resourceProvider.GetResource("Senescent", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Courte</summary>
        public static string Short {
               get {
                   return (string) resourceProvider.GetResource("Short", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Société</summary>
        public static string Society {
               get {
                   return (string) resourceProvider.GetResource("Society", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Sodium</summary>
        public static string Sodium {
               get {
                   return (string) resourceProvider.GetResource("Sodium", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Méridionale</summary>
        public static string Southern {
               get {
                   return (string) resourceProvider.GetResource("Southern", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Espagne</summary>
        public static string Spain {
               get {
                   return (string) resourceProvider.GetResource("Spain", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Espagnol</summary>
        public static string Spanish {
               get {
                   return (string) resourceProvider.GetResource("Spanish", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Espèce</summary>
        public static string Specie {
               get {
                   return (string) resourceProvider.GetResource("Specie", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Espèces</summary>
        public static string SpecieS {
               get {
                   return (string) resourceProvider.GetResource("SpecieS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Cahier des charges</summary>
        public static string Specifications {
               get {
                   return (string) resourceProvider.GetResource("Specifications", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Stades conservations</summary>
        public static string StagesConservations {
               get {
                   return (string) resourceProvider.GetResource("StagesConservations", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Norme</summary>
        public static string Standard {
               get {
                   return (string) resourceProvider.GetResource("Standard", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Relevé</summary>
        public static string Statement {
               get {
                   return (string) resourceProvider.GetResource("Statement", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Relevé(s)</summary>
        public static string StatementP {
               get {
                   return (string) resourceProvider.GetResource("StatementP", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Relevés</summary>
        public static string StatementS {
               get {
                   return (string) resourceProvider.GetResource("StatementS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Relevé vigne</summary>
        public static string StatementVine {
               get {
                   return (string) resourceProvider.GetResource("StatementVine", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Submersion</summary>
        public static string Submersion {
               get {
                   return (string) resourceProvider.GetResource("Submersion", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Sous sol drainant</summary>
        public static string SubsurfaceDrainage {
               get {
                   return (string) resourceProvider.GetResource("SubsurfaceDrainage", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Soufre</summary>
        public static string Sulfur {
               get {
                   return (string) resourceProvider.GetResource("Sulfur", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Pluviométrie estivale</summary>
        public static string SummerPluvio {
               get {
                   return (string) resourceProvider.GetResource("SummerPluvio", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Surface</summary>
        public static string Surface {
               get {
                   return (string) resourceProvider.GetResource("Surface", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Surface foliaire</summary>
        public static string SurfaceFoliar {
               get {
                   return (string) resourceProvider.GetResource("SurfaceFoliar", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>La surface est requise</summary>
        public static string SurfaceRequired {
               get {
                   return (string) resourceProvider.GetResource("SurfaceRequired", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Symptôme</summary>
        public static string Symptom {
               get {
                   return (string) resourceProvider.GetResource("Symptom", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Remarque technique</summary>
        public static string TechnicalNote {
               get {
                   return (string) resourceProvider.GetResource("TechnicalNote", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Technicien</summary>
        public static string Technician {
               get {
                   return (string) resourceProvider.GetResource("Technician", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Tonnage</summary>
        public static string Tonnage {
               get {
                   return (string) resourceProvider.GetResource("Tonnage", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>tonnes/ha</summary>
        public static string TonsHa {
               get {
                   return (string) resourceProvider.GetResource("TonsHa", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Outils</summary>
        public static string Tools {
               get {
                   return (string) resourceProvider.GetResource("Tools", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Conduite</summary>
        public static string Training {
               get {
                   return (string) resourceProvider.GetResource("Training", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Traduction</summary>
        public static string TranslateTools {
               get {
                   return (string) resourceProvider.GetResource("TranslateTools", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Tropicale</summary>
        public static string Tropical {
               get {
                   return (string) resourceProvider.GetResource("Tropical", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Type d'analyse</summary>
        public static string TypeAnalysis {
               get {
                   return (string) resourceProvider.GetResource("TypeAnalysis", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Type irrigation</summary>
        public static string TypeIrrigation {
               get {
                   return (string) resourceProvider.GetResource("TypeIrrigation", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Type de taille</summary>
        public static string TypeOfCut {
               get {
                   return (string) resourceProvider.GetResource("TypeOfCut", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Type de sol</summary>
        public static string TypeSoil {
               get {
                   return (string) resourceProvider.GetResource("TypeSoil", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Type de vin</summary>
        public static string TypeWine {
               get {
                   return (string) resourceProvider.GetResource("TypeWine", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Déséquilibré</summary>
        public static string Unbalanced {
               get {
                   return (string) resourceProvider.GetResource("Unbalanced", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Non taille</summary>
        public static string Uncut {
               get {
                   return (string) resourceProvider.GetResource("Uncut", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Sous frondaison</summary>
        public static string UnderFoliage {
               get {
                   return (string) resourceProvider.GetResource("UnderFoliage", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Unité(s)</summary>
        public static string UnitSP {
               get {
                   return (string) resourceProvider.GetResource("UnitSP", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Inconnu</summary>
        public static string Unknown {
               get {
                   return (string) resourceProvider.GetResource("Unknown", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Inconnue</summary>
        public static string UnknownF {
               get {
                   return (string) resourceProvider.GetResource("UnknownF", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Mettre à jour</summary>
        public static string Update {
               get {
                   return (string) resourceProvider.GetResource("Update", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Utilisateur</summary>
        public static string User {
               get {
                   return (string) resourceProvider.GetResource("User", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Utilisateurs du "Bilan Web" associé à ce client</summary>
        public static string UserBalanceCustomer {
               get {
                   return (string) resourceProvider.GetResource("UserBalanceCustomer", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Utilisateur(s) du "Bilan Web" associé à ce distributeur</summary>
        public static string UserBalanceCustomerP {
               get {
                   return (string) resourceProvider.GetResource("UserBalanceCustomerP", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Nom d'utilisateur</summary>
        public static string UserName {
               get {
                   return (string) resourceProvider.GetResource("UserName", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Impossible ! Cet nom d'utilisateur existe déjà</summary>
        public static string UsernameAlreadyExists {
               get {
                   return (string) resourceProvider.GetResource("UsernameAlreadyExists", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Utilisateurs</summary>
        public static string UserS {
               get {
                   return (string) resourceProvider.GetResource("UserS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Liste des utilisateurs</summary>
        public static string UsersList {
               get {
                   return (string) resourceProvider.GetResource("UsersList", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Variété</summary>
        public static string Variety {
               get {
                   return (string) resourceProvider.GetResource("Variety", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Carence visuelle</summary>
        public static string VisualDeficiency {
               get {
                   return (string) resourceProvider.GetResource("VisualDeficiency", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Contrôle visuel de la vigueur</summary>
        public static string VisualInspectionVigor {
               get {
                   return (string) resourceProvider.GetResource("VisualInspectionVigor", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Attention</summary>
        public static string Warning {
               get {
                   return (string) resourceProvider.GetResource("Warning", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Attention, cette action supprime toutes associations entre ce client et son distributeur.</summary>
        public static string WarningDelAssoCustomer {
               get {
                   return (string) resourceProvider.GetResource("WarningDelAssoCustomer", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Bienvenue</summary>
        public static string Welcome {
               get {
                   return (string) resourceProvider.GetResource("Welcome", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Bienvenue sur le bilan Web de BMS Micro-Nutrients</summary>
        public static string WelcomeBMS {
               get {
                   return (string) resourceProvider.GetResource("WelcomeBMS", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Largeur du rang</summary>
        public static string WidthRow {
               get {
                   return (string) resourceProvider.GetResource("WidthRow", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Catégorie de vin</summary>
        public static string WineCategory {
               get {
                   return (string) resourceProvider.GetResource("WineCategory", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Protection hivernale</summary>
        public static string WinterProtection {
               get {
                   return (string) resourceProvider.GetResource("WinterProtection", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Sans Distributeur</summary>
        public static string WithoutDistributor {
               get {
                   return (string) resourceProvider.GetResource("WithoutDistributor", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Informations de connection erronées</summary>
        public static string WrongConnectionInfos {
               get {
                   return (string) resourceProvider.GetResource("WrongConnectionInfos", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Oui</summary>
        public static string Yes {
               get {
                   return (string) resourceProvider.GetResource("Yes", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Rendement</summary>
        public static string Yield {
               get {
                   return (string) resourceProvider.GetResource("Yield", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Zinc</summary>
        public static string Zinc {
               get {
                   return (string) resourceProvider.GetResource("Zinc", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>C.Postal</summary>
        public static string Zip {
               get {
                   return (string) resourceProvider.GetResource("Zip", CultureInfo.CurrentUICulture.Name);
               }
            }
            
        /// <summary>Code postal</summary>
        public static string ZipCode {
               get {
                   return (string) resourceProvider.GetResource("ZipCode", CultureInfo.CurrentUICulture.Name);
               }
            }

        }        
}
