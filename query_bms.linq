﻿(from P in db.Parcelle
join R in (
	(from Releve in db.Releve
	group Releve by new {
	  Releve.RefParcelle
	} into g
	select new {
	  g.Key.RefParcelle,
	  DateReleve = g.Max(p => p.DateReleve)
	})) on new { ID = P.ID } equals new { ID =R.RefParcelle }
where
  P.Client.ID == 110
select new {
  P.Client.Nom,
  ID_PARCEL = P.ID,
  Column1 = P.Nom,
  R.RefParcelle,
  R.DateReleve
}).Distinct()